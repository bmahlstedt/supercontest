"""You can also run ``commit_scores(verify=False)`` if they're
still posted for the desired week.
"""
from supercontest.dbsession import queries
from supercontest.models import db

# THIS SCRIPT IS OLD AND DOESN'T WORK. SHOULD BE MOVED TO ADMIN VIEW.

SEASON = 2024
WEEK = 2
SCORES = {"EAGLES": 21, "FALCONS": 22}

games = queries.get_games_in_week(SEASON, WEEK)

for game in games:
    game.status_id = 9  # 9 is final, 10 is final OT
    for contestant in game.contestants:
        score = SCORES[contestant.team.name.upper()]
        contestant.score = score
        contestant.get_opponent().opponent_score = score
db.session.commit()
