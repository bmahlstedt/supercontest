from supercontest.models import db
from supercontest.dbsession import queries


SEASON = 2023
WEEK = 10
TEAM = "CHARGERS"  # could be either team
LINE = 4.5

contestant = queries.get_contestant_from_team(season=SEASON, week=WEEK, team=TEAM)
contestant.game.line = LINE
db.session.commit()
