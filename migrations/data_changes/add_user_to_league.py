from supercontest.dbsession import queries, commits

admin_emails = queries.get_admin_emails()
for admin_email in admin_emails:
    commits.add_user_to_current_paid_league(user_email=admin_email)
