"""adding 2022-2023 season

Revision ID: f5a5e0d42653
Revises: 8e05905f1f92
Create Date: 2022-09-01 17:28:41.923257

"""
from typing import Any
from alembic import op

op: Any  # Alembic has unknowns in its own types and I don't want to stub it.

# revision identifiers, used by Alembic.
revision = 'f5a5e0d42653'
down_revision = '8e05905f1f92'
branch_labels = None
depends_on = None


def upgrade():
    ########################## THIS IS THE SECTION TO MODIFY ##################
    year = 2022  # the year in which it starts (not the next year when the season ends)
    first_wed_MM_DD = "09-07"  # this is the wednesday (1 day) before TNF week 1
    dst_shift_week = 9  # the week DURING which daylight savings time shifts (btw wed -> tues)
    ################################ END MODIFY SECTION #######################

    # Secondary configurations. You probably don't have to change these.
    weeks = 18  # number of weeks in the season
    season_start_MM_DD = "09-01"  # september 1st, arbitrary, just enough before season starts
    season_end_MM_DD = "02-01"  # february 1st, arbitrary, just enough after season ends
    season_start = f"'{year}-{season_start_MM_DD}'"
    season_end = f"'{year + 1}-{season_end_MM_DD}'"
    first_wed_date = f"{year}-{first_wed_MM_DD}"
    first_wed_time = "17:00:00"  # 5pm PDT
    delta_to_utc = "7 hours"  # difference from PDT to UTC
    first_wed_datetime = f"TIMESTAMP '{first_wed_date} {first_wed_time}'"  # for pg
    utc_interval = f"INTERVAL '{delta_to_utc}'"  # for pg
    week_length_interval = "INTERVAL '1 week'"  # for pg
    dst_interval = "INTERVAL '1 hour'"  # for pg

    # Add season and get its ID.
    op.execute(
        "INSERT INTO seasons (season, season_start, season_end) "
        f"VALUES ({year}, {season_start}, {season_end})"
    )
    season_id = op.execute("SELECT last_value FROM seasons_id_seq").fetchall()[0]
    # Add weeks.
    for week in range(1, weeks + 1):
        num_weeks_interval = f"{week_length_interval} * {week - 1}"
        week_start = " + ".join([first_wed_datetime, utc_interval, num_weeks_interval])
        week_end = " + ".join([week_start, week_length_interval])
        op.execute(
            "INSERT INTO weeks (season_id, week, week_start, week_end) "
            f"VALUES ({season_id}, {week}, {week_start}, {week_end})"
        )
    # Handle daylight savings time.
    op.execute(
        f"UPDATE weeks SET week_end = week_end + {dst_interval}"
        f"WHERE season_id = {season_id} AND week >= {dst_shift_week}"
    )
    op.execute(
        f"UPDATE weeks SET week_start = week_start + {dst_interval}"
        f"WHERE season_id = {season_id} AND week >= {dst_shift_week + 1}"
    )


def downgrade():
    pass
