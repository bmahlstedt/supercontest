"""adding coverage table

Revision ID: 978e373c9572
Revises: d7d1ff396e12
Create Date: 2023-03-04 21:29:19.663644

"""
from typing import Any
from alembic import op
import sqlalchemy as sa

op: Any

# revision identifiers, used by Alembic.
revision = '978e373c9572'
down_revision = 'd7d1ff396e12'
branch_labels = None
depends_on = None


def upgrade():
    coverage_table = op.create_table(
        'coverage',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('points', sa.Float(), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )
    coverage_rows = [
        {"name": "noncover", "points": 0.0},
        {"name": "push", "points": 0.5},
        {"name": "cover", "points": 1.0},
    ]
    op.bulk_insert(table=coverage_table, rows=coverage_rows)


def downgrade():
    op.drop_table('coverage')
