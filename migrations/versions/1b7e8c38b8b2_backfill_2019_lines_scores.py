"""backfill-2019-lines-scores

Revision ID: 1b7e8c38b8b2
Revises: cca63b25a176
Create Date: 2023-01-16 11:26:23.949166

"""

from supercontest.dbsession import queries, commits

# revision identifiers, used by Alembic.
revision = '1b7e8c38b8b2'
down_revision = 'cca63b25a176'
branch_labels = None
depends_on = None


def upgrade():
    season = 2019

    week = 11
    week_id = queries.get_week_id(season=season, week=week)
    lines = [
        ['BROWNS*', 'STEELERS', 'THURSDAY, NOVEMBER 14, 2019 5:20 PM', '3'],
        ['RAVENS*', 'TEXANS', 'SUNDAY, NOVEMBER 17, 2019 10:00 AM', '4'],
        ['PANTHERS*', 'FALCONS', 'SUNDAY, NOVEMBER 17, 2019 10:00 AM', '3.5'],
        ['COWBOYS', 'LIONS*', 'SUNDAY, NOVEMBER 17, 2019 10:00 AM', '7.5'],
        ['COLTS*', 'JAGUARS', 'SUNDAY, NOVEMBER 17, 2019 10:00 AM', '2.5'],
        ['BILLS', 'DOLPHINS*', 'SUNDAY, NOVEMBER 17, 2019 10:00 AM', '7'],
        ['VIKINGS*', 'BRONCOS', 'SUNDAY, NOVEMBER 17, 2019 10:00 AM', '10'],
        ['SAINTS', 'BUCCANEERS*', 'SUNDAY, NOVEMBER 17, 2019 10:00 AM', '5'],
        ['COMMANDERS*', 'JETS', 'SUNDAY, NOVEMBER 17, 2019 10:00 AM', '1'],
        ['49ERS*', 'CARDINALS', 'SUNDAY, NOVEMBER 17, 2019 1:04 PM', '10'],
        ['RAIDERS*', 'BENGALS', 'SUNDAY, NOVEMBER 17, 2019 1:25 PM', '13'],
        ['PATRIOTS', 'EAGLES*', 'SUNDAY, NOVEMBER 17, 2019 1:25 PM', '4.5'],
        ['RAMS*', 'BEARS', 'SUNDAY, NOVEMBER 17, 2019 5:20 PM', '5'],
        ['CHIEFS', 'CHARGERS', 'MONDAY, NOVEMBER 18, 2019 5:15 PM', '5.5'],
    ]
    commits.write_lines(week_id=week_id, lines=lines)
    scores = [
        Score(line_id=queries.get_line_id(season=season, week=week, team='BROWNS*'), favored_team_score=21, underdog_team_score=7, status='F', coverer=determine_coverer(line=3, favored_team='BROWNS*'.replace('*', ''), underdog_team='STEELERS'.replace('*', ''), favored_team_score=21, underdog_team_score=7)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='RAVENS*'), favored_team_score=41, underdog_team_score=7, status='F', coverer=determine_coverer(line=4, favored_team='RAVENS*'.replace('*', ''), underdog_team='TEXANS'.replace('*', ''), favored_team_score=41, underdog_team_score=7)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='PANTHERS*'), favored_team_score=3, underdog_team_score=29, status='F', coverer=determine_coverer(line=3.5, favored_team='PANTHERS*'.replace('*', ''), underdog_team='FALCONS'.replace('*', ''), favored_team_score=3, underdog_team_score=29)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='COWBOYS'), favored_team_score=35, underdog_team_score=27, status='F', coverer=determine_coverer(line=7.5, favored_team='COWBOYS'.replace('*', ''), underdog_team='LIONS*'.replace('*', ''), favored_team_score=35, underdog_team_score=27)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='COLTS*'), favored_team_score=33, underdog_team_score=13, status='F', coverer=determine_coverer(line=2.5, favored_team='COLTS*'.replace('*', ''), underdog_team='JAGUARS'.replace('*', ''), favored_team_score=33, underdog_team_score=13)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='BILLS'), favored_team_score=37, underdog_team_score=20, status='F', coverer=determine_coverer(line=7, favored_team='BILLS'.replace('*', ''), underdog_team='DOLPHINS*'.replace('*', ''), favored_team_score=37, underdog_team_score=20)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='VIKINGS*'), favored_team_score=27, underdog_team_score=23, status='F', coverer=determine_coverer(line=10, favored_team='VIKINGS*'.replace('*', ''), underdog_team='BRONCOS'.replace('*', ''), favored_team_score=27, underdog_team_score=23)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='SAINTS'), favored_team_score=34, underdog_team_score=17, status='F', coverer=determine_coverer(line=5, favored_team='SAINTS'.replace('*', ''), underdog_team='BUCCANEERS*'.replace('*', ''), favored_team_score=34, underdog_team_score=17)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='COMMANDERS*'), favored_team_score=17, underdog_team_score=34, status='F', coverer=determine_coverer(line=1, favored_team='COMMANDERS*'.replace('*', ''), underdog_team='JETS'.replace('*', ''), favored_team_score=17, underdog_team_score=34)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='49ERS*'), favored_team_score=36, underdog_team_score=26, status='F', coverer=determine_coverer(line=10, favored_team='49ERS*'.replace('*', ''), underdog_team='CARDINALS'.replace('*', ''), favored_team_score=36, underdog_team_score=26)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='RAIDERS*'), favored_team_score=17, underdog_team_score=10, status='F', coverer=determine_coverer(line=13, favored_team='RAIDERS*'.replace('*', ''), underdog_team='BENGALS'.replace('*', ''), favored_team_score=17, underdog_team_score=10)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='PATRIOTS'), favored_team_score=17, underdog_team_score=10, status='F', coverer=determine_coverer(line=4.5, favored_team='PATRIOTS'.replace('*', ''), underdog_team='EAGLES*'.replace('*', ''), favored_team_score=17, underdog_team_score=10)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='RAMS*'), favored_team_score=17, underdog_team_score=7, status='F', coverer=determine_coverer(line=5, favored_team='RAMS*'.replace('*', ''), underdog_team='BEARS'.replace('*', ''), favored_team_score=17, underdog_team_score=7)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='CHIEFS'), favored_team_score=24, underdog_team_score=17, status='F', coverer=determine_coverer(line=5.5, favored_team='CHIEFS'.replace('*', ''), underdog_team='CHARGERS'.replace('*', ''), favored_team_score=24, underdog_team_score=17)),
    ]
    db.session.add_all(scores)
    db.session.commit()

    week = 12
    week_id = queries.get_week_id(season=season, week=week)
    lines = [
        ['TEXANS*', 'COLTS', 'THURSDAY, NOVEMBER 21, 2019 5:20 PM', '3.5'],
        ['FALCONS*', 'BUCCANEERS', 'SUNDAY, NOVEMBER 24, 2019 10:00 AM', '3.5'],
        ['BILLS*', 'BRONCOS', 'SUNDAY, NOVEMBER 24, 2019 10:00 AM', '3.5'],
        ['BEARS*', 'GIANTS', 'SUNDAY, NOVEMBER 24, 2019 10:00 AM', '6'],
        ['STEELERS', 'BENGALS*', 'SUNDAY, NOVEMBER 24, 2019 10:00 AM', '5.5'],
        ['BROWNS*', 'DOLPHINS', 'SUNDAY, NOVEMBER 24, 2019 10:00 AM', '11'],
        ['SAINTS*', 'PANTHERS', 'SUNDAY, NOVEMBER 24, 2019 10:00 AM', '10'],
        ['RAIDERS', 'JETS*', 'SUNDAY, NOVEMBER 24, 2019 10:00 AM', '3.5'],
        ['SEAHAWKS', 'EAGLES*', 'SUNDAY, NOVEMBER 24, 2019 10:00 AM', '1'],
        ['LIONS', 'COMMANDERS*', 'SUNDAY, NOVEMBER 24, 2019 10:00 AM', '4'],
        ['TITANS*', 'JAGUARS', 'SUNDAY, NOVEMBER 24, 2019 1:04 PM', '4.5'],
        ['PATRIOTS*', 'COWBOYS', 'SUNDAY, NOVEMBER 24, 2019 1:25 PM', '5.5'],
        ['49ERS*', 'PACKERS', 'SUNDAY, NOVEMBER 24, 2019 5:20 PM', '3'],
        ['RAVENS', 'RAMS*', 'MONDAY, NOVEMBER 25, 2019 5:15 PM', '3.5'],
    ]
    commits.write_lines(week_id=week_id, lines=lines)
    scores = [
        Score(line_id=queries.get_line_id(season=season, week=week, team='TEXANS*'), favored_team_score=20, underdog_team_score=17, status='F', coverer=determine_coverer(line=3.5, favored_team='TEXANS*'.replace('*', ''), underdog_team='COLTS'.replace('*', ''), favored_team_score=20, underdog_team_score=17)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='FALCONS*'), favored_team_score=22, underdog_team_score=35, status='F', coverer=determine_coverer(line=3.5, favored_team='FALCONS*'.replace('*', ''), underdog_team='BUCCANEERS'.replace('*', ''), favored_team_score=22, underdog_team_score=35)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='BILLS*'), favored_team_score=20, underdog_team_score=3, status='F', coverer=determine_coverer(line=3.5, favored_team='BILLS*'.replace('*', ''), underdog_team='BRONCOS'.replace('*', ''), favored_team_score=20, underdog_team_score=3)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='BEARS*'), favored_team_score=19, underdog_team_score=14, status='F', coverer=determine_coverer(line=6, favored_team='BEARS*'.replace('*', ''), underdog_team='GIANTS'.replace('*', ''), favored_team_score=19, underdog_team_score=14)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='STEELERS'), favored_team_score=16, underdog_team_score=10, status='F', coverer=determine_coverer(line=5.5, favored_team='STEELERS'.replace('*', ''), underdog_team='BENGALS*'.replace('*', ''), favored_team_score=16, underdog_team_score=10)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='BROWNS*'), favored_team_score=41, underdog_team_score=24, status='F', coverer=determine_coverer(line=11, favored_team='BROWNS*'.replace('*', ''), underdog_team='DOLPHINS'.replace('*', ''), favored_team_score=41, underdog_team_score=24)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='SAINTS*'), favored_team_score=34, underdog_team_score=31, status='F', coverer=determine_coverer(line=10, favored_team='SAINTS*'.replace('*', ''), underdog_team='PANTHERS'.replace('*', ''), favored_team_score=34, underdog_team_score=31)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='RAIDERS'), favored_team_score=3, underdog_team_score=34, status='F', coverer=determine_coverer(line=3.5, favored_team='RAIDERS'.replace('*', ''), underdog_team='JETS*'.replace('*', ''), favored_team_score=3, underdog_team_score=34)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='SEAHAWKS'), favored_team_score=17, underdog_team_score=9, status='F', coverer=determine_coverer(line=1, favored_team='SEAHAWKS'.replace('*', ''), underdog_team='EAGLES*'.replace('*', ''), favored_team_score=17, underdog_team_score=9)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='LIONS'), favored_team_score=16, underdog_team_score=19, status='F', coverer=determine_coverer(line=4, favored_team='LIONS'.replace('*', ''), underdog_team='COMMANDERS*'.replace('*', ''), favored_team_score=16, underdog_team_score=19)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='TITANS*'), favored_team_score=42, underdog_team_score=20, status='F', coverer=determine_coverer(line=4.5, favored_team='TITANS*'.replace('*', ''), underdog_team='JAGUARS'.replace('*', ''), favored_team_score=42, underdog_team_score=20)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='PATRIOTS*'), favored_team_score=13, underdog_team_score=9, status='F', coverer=determine_coverer(line=5.5, favored_team='PATRIOTS*'.replace('*', ''), underdog_team='COWBOYS'.replace('*', ''), favored_team_score=13, underdog_team_score=9)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='49ERS*'), favored_team_score=37, underdog_team_score=8, status='F', coverer=determine_coverer(line=3, favored_team='49ERS*'.replace('*', ''), underdog_team='PACKERS'.replace('*', ''), favored_team_score=37, underdog_team_score=8)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='RAVENS'), favored_team_score=45, underdog_team_score=6, status='F', coverer=determine_coverer(line=3.5, favored_team='RAVENS'.replace('*', ''), underdog_team='RAMS*'.replace('*', ''), favored_team_score=45, underdog_team_score=6)),
    ]
    db.session.add_all(scores)
    db.session.commit()

    week = 13
    week_id = queries.get_week_id(season=season, week=week)
    lines = [
        ['BEARS', 'LIONS*', 'THURSDAY, NOVEMBER 28, 2019 9:30 PM', '5.5'],
        ['COWBOYS*', 'BILLS', 'THURSDAY, NOVEMBER 28, 2019 1:30 PM', '6.5'],
        ['SAINTS', 'FALCONS*', 'THURSDAY, NOVEMBER 28, 2019 5:20 PM', '7'],
        ['RAVENS*', '49ERS', 'SUNDAY, DECEMBER 1, 2019 10:00 AM', '5.5'],
        ['PANTHERS*', 'COMMANDERS', 'SUNDAY, DECEMBER 1, 2019 10:00 AM', '10.5'],
        ['JETS', 'BENGALS*', 'SUNDAY, DECEMBER 1, 2019 10:00 AM', '2.5'],
        ['TITANS', 'COLTS*', 'SUNDAY, DECEMBER 1, 2019 10:00 AM', '1'],
        ['BUCCANEERS', 'JAGUARS*', 'SUNDAY, DECEMBER 1, 2019 10:00 AM', '3'],
        ['EAGLES', 'DOLPHINS*', 'SUNDAY, DECEMBER 1, 2019 10:00 AM', '10.5'],
        ['PACKERS', 'GIANTS*', 'SUNDAY, DECEMBER 1, 2019 10:00 AM', '6.5'],
        ['BROWNS', 'STEELERS*', 'SUNDAY, DECEMBER 1, 2019 10:00 AM', '1'],
        ['RAMS', 'CARDINALS*', 'SUNDAY, DECEMBER 1, 2019 1:04 PM', '2.5'],
        ['CHARGERS', 'BRONCOS*', 'SUNDAY, DECEMBER 1, 2019 1:25 PM', '4.5'],
        ['CHIEFS*', 'RAIDERS', 'SUNDAY, DECEMBER 1, 2019 1:25 PM', '11.5'],
        ['PATRIOTS', 'TEXANS*', 'SUNDAY, DECEMBER 1, 2019 5:20 PM', '3.5'],
        ['SEAHAWKS*', 'VIKINGS', 'MONDAY, DECEMBER 2, 2019 5:15 PM', '2.5'],
    ]
    commits.write_lines(week_id=week_id, lines=lines)
    scores = [
        Score(line_id=queries.get_line_id(season=season, week=week, team='BEARS'), favored_team_score=24, underdog_team_score=20, status='F', coverer=determine_coverer(line=5.5, favored_team='BEARS'.replace('*', ''), underdog_team='LIONS*'.replace('*', ''), favored_team_score=24, underdog_team_score=20)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='COWBOYS*'), favored_team_score=15, underdog_team_score=26, status='F', coverer=determine_coverer(line=6.5, favored_team='COWBOYS*'.replace('*', ''), underdog_team='BILLS'.replace('*', ''), favored_team_score=15, underdog_team_score=26)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='SAINTS'), favored_team_score=26, underdog_team_score=18, status='F', coverer=determine_coverer(line=7, favored_team='SAINTS'.replace('*', ''), underdog_team='FALCONS*'.replace('*', ''), favored_team_score=26, underdog_team_score=18)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='RAVENS*'), favored_team_score=20, underdog_team_score=17, status='F', coverer=determine_coverer(line=5.5, favored_team='RAVENS*'.replace('*', ''), underdog_team='49ERS'.replace('*', ''), favored_team_score=20, underdog_team_score=17)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='PANTHERS*'), favored_team_score=21, underdog_team_score=29, status='F', coverer=determine_coverer(line=10.5, favored_team='PANTHERS*'.replace('*', ''), underdog_team='COMMANDERS'.replace('*', ''), favored_team_score=21, underdog_team_score=29)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='JETS'), favored_team_score=6, underdog_team_score=22, status='F', coverer=determine_coverer(line=2.5, favored_team='JETS'.replace('*', ''), underdog_team='BENGALS*'.replace('*', ''), favored_team_score=6, underdog_team_score=22)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='TITANS'), favored_team_score=31, underdog_team_score=17, status='F', coverer=determine_coverer(line=1, favored_team='TITANS'.replace('*', ''), underdog_team='COLTS*'.replace('*', ''), favored_team_score=31, underdog_team_score=17)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='BUCCANEERS'), favored_team_score=28, underdog_team_score=11, status='F', coverer=determine_coverer(line=3, favored_team='BUCCANEERS'.replace('*', ''), underdog_team='JAGUARS*'.replace('*', ''), favored_team_score=28, underdog_team_score=11)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='EAGLES'), favored_team_score=31, underdog_team_score=37, status='F', coverer=determine_coverer(line=10.5, favored_team='EAGLES'.replace('*', ''), underdog_team='DOLPHINS*'.replace('*', ''), favored_team_score=31, underdog_team_score=37)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='PACKERS'), favored_team_score=31, underdog_team_score=13, status='F', coverer=determine_coverer(line=6.5, favored_team='PACKERS'.replace('*', ''), underdog_team='GIANTS*'.replace('*', ''), favored_team_score=31, underdog_team_score=13)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='BROWNS'), favored_team_score=13, underdog_team_score=20, status='F', coverer=determine_coverer(line=1, favored_team='BROWNS'.replace('*', ''), underdog_team='STEELERS*'.replace('*', ''), favored_team_score=13, underdog_team_score=20)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='RAMS'), favored_team_score=34, underdog_team_score=7, status='F', coverer=determine_coverer(line=2.5, favored_team='RAMS'.replace('*', ''), underdog_team='CARDINALS*'.replace('*', ''), favored_team_score=34, underdog_team_score=7)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='CHARGERS'), favored_team_score=20, underdog_team_score=23, status='F', coverer=determine_coverer(line=4.5, favored_team='CHARGERS'.replace('*', ''), underdog_team='BRONCOS*'.replace('*', ''), favored_team_score=20, underdog_team_score=23)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='CHIEFS*'), favored_team_score=40, underdog_team_score=9, status='F', coverer=determine_coverer(line=11.5, favored_team='CHIEFS*'.replace('*', ''), underdog_team='RAIDERS'.replace('*', ''), favored_team_score=40, underdog_team_score=9)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='PATRIOTS'), favored_team_score=22, underdog_team_score=28, status='F', coverer=determine_coverer(line=3.5, favored_team='PATRIOTS'.replace('*', ''), underdog_team='TEXANS*'.replace('*', ''), favored_team_score=22, underdog_team_score=28)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='SEAHAWKS*'), favored_team_score=37, underdog_team_score=30, status='F', coverer=determine_coverer(line=2.5, favored_team='SEAHAWKS*'.replace('*', ''), underdog_team='VIKINGS'.replace('*', ''), favored_team_score=37, underdog_team_score=30)),
    ]
    db.session.add_all(scores)
    db.session.commit()

    week = 14
    week_id = queries.get_week_id(season=season, week=week)
    lines = [
        ['COWBOYS', 'BEARS*', 'THURSDAY, DECEMBER 5, 2019 5:20 PM', '3'],
        ['FALCONS*', 'PANTHERS', 'SUNDAY, DECEMBER 8, 2019 10:00 AM', '3.5'],
        ['RAVENS', 'BILLS*', 'SUNDAY, DECEMBER 8, 2019 10:00 AM', '6.5'],
        ['BROWNS*', 'BENGALS', 'SUNDAY, DECEMBER 8, 2019 10:00 AM', '6.5'],
        ['PACKERS*', 'COMMANDERS', 'SUNDAY, DECEMBER 8, 2019 10:00 AM', '13'],
        ['TEXANS*', 'BRONCOS', 'SUNDAY, DECEMBER 8, 2019 10:00 AM', '8'],
        ['VIKINGS*', 'LIONS', 'SUNDAY, DECEMBER 8, 2019 10:00 AM', '12'],
        ['SAINTS*', '49ERS', 'SUNDAY, DECEMBER 8, 2019 10:00 AM', '1'],
        ['JETS*', 'DOLPHINS', 'SUNDAY, DECEMBER 8, 2019 10:00 AM', '5'],
        ['BUCCANEERS*', 'COLTS', 'SUNDAY, DECEMBER 8, 2019 10:00 AM', '3'],
        ['CHARGERS', 'JAGUARS*', 'SUNDAY, DECEMBER 8, 2019 1:04 PM', '3.5'],
        ['STEELERS', 'CARDINALS*', 'SUNDAY, DECEMBER 8, 2019 1:25 PM', '2.5'],
        ['PATRIOTS*', 'CHIEFS', 'SUNDAY, DECEMBER 8, 2019 1:25 PM', '3'],
        ['TITANS', 'RAIDERS*', 'SUNDAY, DECEMBER 8, 2019 1:25 PM', '3'],
        ['RAMS*', 'SEAHAWKS', 'SUNDAY, DECEMBER 8, 2019 5:20 PM', '1'],
        ['EAGLES*', 'GIANTS', 'MONDAY, DECEMBER 9, 2019 5:15 PM', '9.5'],
    ]
    commits.write_lines(week_id=week_id, lines=lines)
    scores = [
        Score(line_id=queries.get_line_id(season=season, week=week, team='COWBOYS'), favored_team_score=24, underdog_team_score=31, status='F', coverer=determine_coverer(line=3, favored_team='COWBOYS'.replace('*', ''), underdog_team='BEARS*'.replace('*', ''), favored_team_score=24, underdog_team_score=31)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='FALCONS*'), favored_team_score=40, underdog_team_score=20, status='F', coverer=determine_coverer(line=3.5, favored_team='FALCONS*'.replace('*', ''), underdog_team='PANTHERS'.replace('*', ''), favored_team_score=40, underdog_team_score=20)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='RAVENS'), favored_team_score=24, underdog_team_score=17, status='F', coverer=determine_coverer(line=6.5, favored_team='RAVENS'.replace('*', ''), underdog_team='BILLS*'.replace('*', ''), favored_team_score=24, underdog_team_score=17)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='BROWNS*'), favored_team_score=27, underdog_team_score=19, status='F', coverer=determine_coverer(line=6.5, favored_team='BROWNS*'.replace('*', ''), underdog_team='BENGALS'.replace('*', ''), favored_team_score=27, underdog_team_score=19)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='PACKERS*'), favored_team_score=20, underdog_team_score=15, status='F', coverer=determine_coverer(line=13, favored_team='PACKERS*'.replace('*', ''), underdog_team='COMMANDERS'.replace('*', ''), favored_team_score=20, underdog_team_score=15)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='TEXANS*'), favored_team_score=24, underdog_team_score=38, status='F', coverer=determine_coverer(line=8, favored_team='TEXANS*'.replace('*', ''), underdog_team='BRONCOS'.replace('*', ''), favored_team_score=24, underdog_team_score=38)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='VIKINGS*'), favored_team_score=20, underdog_team_score=7, status='F', coverer=determine_coverer(line=12, favored_team='VIKINGS*'.replace('*', ''), underdog_team='LIONS'.replace('*', ''), favored_team_score=20, underdog_team_score=7)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='SAINTS*'), favored_team_score=46, underdog_team_score=48, status='F', coverer=determine_coverer(line=1, favored_team='SAINTS*'.replace('*', ''), underdog_team='49ERS'.replace('*', ''), favored_team_score=46, underdog_team_score=48)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='JETS*'), favored_team_score=22, underdog_team_score=21, status='F', coverer=determine_coverer(line=5, favored_team='JETS*'.replace('*', ''), underdog_team='DOLPHINS'.replace('*', ''), favored_team_score=22, underdog_team_score=21)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='BUCCANEERS*'), favored_team_score=38, underdog_team_score=35, status='F', coverer=determine_coverer(line=3, favored_team='BUCCANEERS*'.replace('*', ''), underdog_team='COLTS'.replace('*', ''), favored_team_score=38, underdog_team_score=35)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='CHARGERS'), favored_team_score=45, underdog_team_score=10, status='F', coverer=determine_coverer(line=3.5, favored_team='CHARGERS'.replace('*', ''), underdog_team='JAGUARS*'.replace('*', ''), favored_team_score=45, underdog_team_score=10)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='STEELERS'), favored_team_score=23, underdog_team_score=17, status='F', coverer=determine_coverer(line=2.5, favored_team='STEELERS'.replace('*', ''), underdog_team='CARDINALS*'.replace('*', ''), favored_team_score=23, underdog_team_score=17)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='PATRIOTS*'), favored_team_score=16, underdog_team_score=23, status='F', coverer=determine_coverer(line=3, favored_team='PATRIOTS*'.replace('*', ''), underdog_team='CHIEFS'.replace('*', ''), favored_team_score=16, underdog_team_score=23)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='TITANS'), favored_team_score=42, underdog_team_score=21, status='F', coverer=determine_coverer(line=3, favored_team='TITANS'.replace('*', ''), underdog_team='RAIDERS*'.replace('*', ''), favored_team_score=42, underdog_team_score=21)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='RAMS*'), favored_team_score=28, underdog_team_score=12, status='F', coverer=determine_coverer(line=1, favored_team='RAMS*'.replace('*', ''), underdog_team='SEAHAWKS'.replace('*', ''), favored_team_score=28, underdog_team_score=12)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='EAGLES*'), favored_team_score=23, underdog_team_score=17, status='FO', coverer=determine_coverer(line=9.5, favored_team='EAGLES*'.replace('*', ''), underdog_team='GIANTS'.replace('*', ''), favored_team_score=23, underdog_team_score=17)),
    ]
    db.session.add_all(scores)
    db.session.commit()

    week = 15
    week_id = queries.get_week_id(season=season, week=week)
    lines = [
        ['RAVENS*', 'JETS', 'THURSDAY, DECEMBER 12, 2019 5:20 PM', '17'],
        ['SEAHAWKS', 'PANTHERS*', 'SUNDAY, DECEMBER 15, 2019 10:00 AM', '6'],
        ['PATRIOTS', 'BENGALS*', 'SUNDAY, DECEMBER 15, 2019 10:00 AM', '10.5'],
        ['BUCCANEERS', 'LIONS*', 'SUNDAY, DECEMBER 15, 2019 10:00 AM', '5.5'],
        ['PACKERS*', 'BEARS', 'SUNDAY, DECEMBER 15, 2019 10:00 AM', '4'],
        ['CHIEFS*', 'BRONCOS', 'SUNDAY, DECEMBER 15, 2019 10:00 AM', '10'],
        ['GIANTS*', 'DOLPHINS', 'SUNDAY, DECEMBER 15, 2019 10:00 AM', '3.5'],
        ['TITANS*', 'TEXANS', 'SUNDAY, DECEMBER 15, 2019 10:00 AM', '3'],
        ['EAGLES', 'COMMANDERS*', 'SUNDAY, DECEMBER 15, 2019 10:00 AM', '7'],
        ['BROWNS', 'CARDINALS*', 'SUNDAY, DECEMBER 15, 2019 1:04 PM', '3'],
        ['VIKINGS', 'CHARGERS*', 'SUNDAY, DECEMBER 15, 2019 1:04 PM', '1'],
        ['RAIDERS*', 'JAGUARS', 'SUNDAY, DECEMBER 15, 2019 1:04 PM', '7'],
        ['RAMS', 'COWBOYS*', 'SUNDAY, DECEMBER 15, 2019 1:25 PM', '1'],
        ['49ERS*', 'FALCONS', 'SUNDAY, DECEMBER 15, 2019 1:25 PM', '10.5'],
        ['STEELERS*', 'BILLS', 'SUNDAY, DECEMBER 15, 2019 5:20 PM', '1'],
        ['SAINTS*', 'COLTS', 'MONDAY, DECEMBER 16, 2019 5:15 PM', '8'],
    ]
    commits.write_lines(week_id=week_id, lines=lines)
    scores = [
        Score(line_id=queries.get_line_id(season=season, week=week, team='RAVENS*'), favored_team_score=42, underdog_team_score=21, status='F', coverer=determine_coverer(line=17, favored_team='RAVENS*'.replace('*', ''), underdog_team='JETS'.replace('*', ''), favored_team_score=42, underdog_team_score=21)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='SEAHAWKS'), favored_team_score=30, underdog_team_score=24, status='F', coverer=determine_coverer(line=6, favored_team='SEAHAWKS'.replace('*', ''), underdog_team='PANTHERS*'.replace('*', ''), favored_team_score=30, underdog_team_score=24)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='PATRIOTS'), favored_team_score=34, underdog_team_score=13, status='F', coverer=determine_coverer(line=10.5, favored_team='PATRIOTS'.replace('*', ''), underdog_team='BENGALS*'.replace('*', ''), favored_team_score=34, underdog_team_score=13)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='BUCCANEERS'), favored_team_score=38, underdog_team_score=17, status='F', coverer=determine_coverer(line=5.5, favored_team='BUCCANEERS'.replace('*', ''), underdog_team='LIONS*'.replace('*', ''), favored_team_score=38, underdog_team_score=17)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='PACKERS*'), favored_team_score=21, underdog_team_score=13, status='F', coverer=determine_coverer(line=4, favored_team='PACKERS*'.replace('*', ''), underdog_team='BEARS'.replace('*', ''), favored_team_score=21, underdog_team_score=13)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='CHIEFS*'), favored_team_score=23, underdog_team_score=3, status='F', coverer=determine_coverer(line=10, favored_team='CHIEFS*'.replace('*', ''), underdog_team='BRONCOS'.replace('*', ''), favored_team_score=23, underdog_team_score=3)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='GIANTS*'), favored_team_score=36, underdog_team_score=20, status='F', coverer=determine_coverer(line=3.5, favored_team='GIANTS*'.replace('*', ''), underdog_team='DOLPHINS'.replace('*', ''), favored_team_score=36, underdog_team_score=20)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='TITANS*'), favored_team_score=21, underdog_team_score=24, status='F', coverer=determine_coverer(line=3, favored_team='TITANS*'.replace('*', ''), underdog_team='TEXANS'.replace('*', ''), favored_team_score=21, underdog_team_score=24)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='EAGLES'), favored_team_score=37, underdog_team_score=27, status='F', coverer=determine_coverer(line=7, favored_team='EAGLES'.replace('*', ''), underdog_team='COMMANDERS*'.replace('*', ''), favored_team_score=37, underdog_team_score=27)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='BROWNS'), favored_team_score=24, underdog_team_score=38, status='F', coverer=determine_coverer(line=3, favored_team='BROWNS'.replace('*', ''), underdog_team='CARDINALS*'.replace('*', ''), favored_team_score=24, underdog_team_score=38)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='VIKINGS'), favored_team_score=39, underdog_team_score=10, status='F', coverer=determine_coverer(line=1, favored_team='VIKINGS'.replace('*', ''), underdog_team='CHARGERS*'.replace('*', ''), favored_team_score=39, underdog_team_score=10)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='RAIDERS*'), favored_team_score=16, underdog_team_score=20, status='F', coverer=determine_coverer(line=7, favored_team='RAIDERS*'.replace('*', ''), underdog_team='JAGUARS'.replace('*', ''), favored_team_score=16, underdog_team_score=20)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='RAMS'), favored_team_score=21, underdog_team_score=44, status='F', coverer=determine_coverer(line=1, favored_team='RAMS'.replace('*', ''), underdog_team='COWBOYS*'.replace('*', ''), favored_team_score=21, underdog_team_score=44)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='49ERS*'), favored_team_score=22, underdog_team_score=29, status='F', coverer=determine_coverer(line=10.5, favored_team='49ERS*'.replace('*', ''), underdog_team='FALCONS'.replace('*', ''), favored_team_score=22, underdog_team_score=29)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='STEELERS*'), favored_team_score=10, underdog_team_score=17, status='F', coverer=determine_coverer(line=1, favored_team='STEELERS*'.replace('*', ''), underdog_team='BILLS'.replace('*', ''), favored_team_score=10, underdog_team_score=17)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='SAINTS*'), favored_team_score=34, underdog_team_score=7, status='F', coverer=determine_coverer(line=8, favored_team='SAINTS*'.replace('*', ''), underdog_team='COLTS'.replace('*', ''), favored_team_score=34, underdog_team_score=7)),
    ]
    db.session.add_all(scores)
    db.session.commit()

    week = 16
    week_id = queries.get_week_id(season=season, week=week)
    lines = [
        ['TEXANS', 'BUCCANEERS*', 'SATURDAY, DECEMBER 21, 2019 10:00 AM', '3'],
        ['PATRIOTS*', 'BILLS', 'SATURDAY, DECEMBER 21, 2019 1:30 PM', '7'],
        ['49ERS*', 'RAMS', 'SATURDAY, DECEMBER 21, 2019 5:15 PM', '7'],
        ['FALCONS*', 'JAGUARS', 'SUNDAY, DECEMBER 22, 2019 10:00 AM', '7.5'],
        ['RAVENS', 'BROWNS*', 'SUNDAY, DECEMBER 22, 2019 10:00 AM', '9.5'],
        ['COLTS*', 'PANTHERS', 'SUNDAY, DECEMBER 22, 2019 10:00 AM', '7.5'],
        ['BENGALS', 'DOLPHINS*', 'SUNDAY, DECEMBER 22, 2019 10:00 AM', '1.5'],
        ['STEELERS', 'JETS*', 'SUNDAY, DECEMBER 22, 2019 10:00 AM', '3'],
        ['SAINTS', 'TITANS*', 'SUNDAY, DECEMBER 22, 2019 10:00 AM', '3.5'],
        ['GIANTS', 'COMMANDERS*', 'SUNDAY, DECEMBER 22, 2019 10:00 AM', '1'],
        ['BRONCOS*', 'LIONS', 'SUNDAY, DECEMBER 22, 2019 1:04 PM', '9.5'],
        ['CHARGERS*', 'RAIDERS', 'SUNDAY, DECEMBER 22, 2019 1:04 PM', '7.5'],
        ['COWBOYS', 'EAGLES*', 'SUNDAY, DECEMBER 22, 2019 1:25 PM', '2'],
        ['SEAHAWKS*', 'CARDINALS', 'SUNDAY, DECEMBER 22, 2019 1:25 PM', '8'],
        ['CHIEFS', 'BEARS*', 'SUNDAY, DECEMBER 22, 2019 5:20 PM', '6.5'],
        ['VIKINGS*', 'PACKERS', 'MONDAY, DECEMBER 23, 2019 5:15 PM', '4'],
    ]
    commits.write_lines(week_id=week_id, lines=lines)
    scores = [
        Score(line_id=queries.get_line_id(season=season, week=week, team='TEXANS'), favored_team_score=23, underdog_team_score=20, status='F', coverer=determine_coverer(line=3, favored_team='TEXANS'.replace('*', ''), underdog_team='BUCCANEERS*'.replace('*', ''), favored_team_score=23, underdog_team_score=20)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='PATRIOTS*'), favored_team_score=24, underdog_team_score=17, status='F', coverer=determine_coverer(line=7, favored_team='PATRIOTS*'.replace('*', ''), underdog_team='BILLS'.replace('*', ''), favored_team_score=24, underdog_team_score=17)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='49ERS*'), favored_team_score=34, underdog_team_score=31, status='F', coverer=determine_coverer(line=7, favored_team='49ERS*'.replace('*', ''), underdog_team='RAMS'.replace('*', ''), favored_team_score=34, underdog_team_score=31)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='FALCONS*'), favored_team_score=24, underdog_team_score=12, status='F', coverer=determine_coverer(line=7.5, favored_team='FALCONS*'.replace('*', ''), underdog_team='JAGUARS'.replace('*', ''), favored_team_score=24, underdog_team_score=12)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='RAVENS'), favored_team_score=31, underdog_team_score=15, status='F', coverer=determine_coverer(line=9.5, favored_team='RAVENS'.replace('*', ''), underdog_team='BROWNS*'.replace('*', ''), favored_team_score=31, underdog_team_score=15)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='COLTS*'), favored_team_score=38, underdog_team_score=6, status='F', coverer=determine_coverer(line=7.5, favored_team='COLTS*'.replace('*', ''), underdog_team='PANTHERS'.replace('*', ''), favored_team_score=38, underdog_team_score=6)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='BENGALS'), favored_team_score=35, underdog_team_score=38, status='FO', coverer=determine_coverer(line=1.5, favored_team='BENGALS'.replace('*', ''), underdog_team='DOLPHINS*'.replace('*', ''), favored_team_score=35, underdog_team_score=38)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='STEELERS'), favored_team_score=10, underdog_team_score=16, status='F', coverer=determine_coverer(line=3, favored_team='STEELERS'.replace('*', ''), underdog_team='JETS*'.replace('*', ''), favored_team_score=10, underdog_team_score=16)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='SAINTS'), favored_team_score=38, underdog_team_score=28, status='F', coverer=determine_coverer(line=3.5, favored_team='SAINTS'.replace('*', ''), underdog_team='TITANS*'.replace('*', ''), favored_team_score=38, underdog_team_score=28)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='GIANTS'), favored_team_score=41, underdog_team_score=35, status='FO', coverer=determine_coverer(line=1, favored_team='GIANTS'.replace('*', ''), underdog_team='COMMANDERS*'.replace('*', ''), favored_team_score=41, underdog_team_score=35)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='BRONCOS*'), favored_team_score=27, underdog_team_score=17, status='F', coverer=determine_coverer(line=9.5, favored_team='BRONCOS*'.replace('*', ''), underdog_team='LIONS'.replace('*', ''), favored_team_score=27, underdog_team_score=17)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='CHARGERS*'), favored_team_score=17, underdog_team_score=24, status='F', coverer=determine_coverer(line=7.5, favored_team='CHARGERS*'.replace('*', ''), underdog_team='RAIDERS'.replace('*', ''), favored_team_score=17, underdog_team_score=24)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='COWBOYS'), favored_team_score=9, underdog_team_score=17, status='F', coverer=determine_coverer(line=2, favored_team='COWBOYS'.replace('*', ''), underdog_team='EAGLES*'.replace('*', ''), favored_team_score=9, underdog_team_score=17)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='SEAHAWKS*'), favored_team_score=13, underdog_team_score=27, status='F', coverer=determine_coverer(line=8, favored_team='SEAHAWKS*'.replace('*', ''), underdog_team='CARDINALS'.replace('*', ''), favored_team_score=13, underdog_team_score=27)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='CHIEFS'), favored_team_score=26, underdog_team_score=3, status='F', coverer=determine_coverer(line=6.5, favored_team='CHIEFS'.replace('*', ''), underdog_team='BEARS*'.replace('*', ''), favored_team_score=26, underdog_team_score=3)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='VIKINGS*'), favored_team_score=10, underdog_team_score=23, status='F', coverer=determine_coverer(line=4, favored_team='VIKINGS*'.replace('*', ''), underdog_team='PACKERS'.replace('*', ''), favored_team_score=10, underdog_team_score=23)),
    ]
    db.session.add_all(scores)
    db.session.commit()

    week = 17
    week_id = queries.get_week_id(season=season, week=week)
    lines = [
        ['JETS', 'BILLS*', 'SUNDAY, DECEMBER 29, 2019 10:00 AM', '1.5'],
        ['SAINTS', 'PANTHERS*', 'SUNDAY, DECEMBER 29, 2019 10:00 AM', '13.5'],
        ['BROWNS', 'BENGALS*', 'SUNDAY, DECEMBER 29, 2019 10:00 AM', '2.5'],
        ['PACKERS', 'LIONS*', 'SUNDAY, DECEMBER 29, 2019 10:00 AM', '13.5'],
        ['CHIEFS*', 'CHARGERS', 'SUNDAY, DECEMBER 29, 2019 10:00 AM', '10'],
        ['BEARS', 'VIKINGS*', 'SUNDAY, DECEMBER 29, 2019 10:00 AM', '5'],
        ['PATRIOTS*', 'DOLPHINS', 'SUNDAY, DECEMBER 29, 2019 10:00 AM', '17.5'],
        ['BUCCANEERS*', 'FALCONS', 'SUNDAY, DECEMBER 29, 2019 10:00 AM', '1'],
        ['STEELERS', 'RAVENS*', 'SUNDAY, DECEMBER 29, 2019 1:25 PM', '2'],
        ['COWBOYS*', 'COMMANDERS', 'SUNDAY, DECEMBER 29, 2019 1:25 PM', '12.5'],
        ['BRONCOS*', 'RAIDERS', 'SUNDAY, DECEMBER 29, 2019 1:25 PM', '5.5'],
        ['TITANS', 'TEXANS*', 'SUNDAY, DECEMBER 29, 2019 1:25 PM', '10'],
        ['COLTS', 'JAGUARS*', 'SUNDAY, DECEMBER 29, 2019 1:25 PM', '5'],
        ['RAMS*', 'CARDINALS', 'SUNDAY, DECEMBER 29, 2019 1:25 PM', '7'],
        ['EAGLES', 'GIANTS*', 'SUNDAY, DECEMBER 29, 2019 1:25 PM', '4'],
        ['49ERS', 'SEAHAWKS*', 'SUNDAY, DECEMBER 29, 2019 5:20 PM', '3.5'],
    ]
    commits.write_lines(week_id=week_id, lines=lines)
    scores = [
        Score(line_id=queries.get_line_id(season=season, week=week, team='JETS'), favored_team_score=13, underdog_team_score=6, status='F', coverer=determine_coverer(line=1.5, favored_team='JETS'.replace('*', ''), underdog_team='BILLS*'.replace('*', ''), favored_team_score=13, underdog_team_score=6)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='SAINTS'), favored_team_score=42, underdog_team_score=10, status='F', coverer=determine_coverer(line=13.5, favored_team='SAINTS'.replace('*', ''), underdog_team='PANTHERS*'.replace('*', ''), favored_team_score=42, underdog_team_score=10)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='BROWNS'), favored_team_score=23, underdog_team_score=33, status='F', coverer=determine_coverer(line=2.5, favored_team='BROWNS'.replace('*', ''), underdog_team='BENGALS*'.replace('*', ''), favored_team_score=23, underdog_team_score=33)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='PACKERS'), favored_team_score=23, underdog_team_score=20, status='F', coverer=determine_coverer(line=13.5, favored_team='PACKERS'.replace('*', ''), underdog_team='LIONS*'.replace('*', ''), favored_team_score=23, underdog_team_score=20)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='CHIEFS*'), favored_team_score=31, underdog_team_score=21, status='F', coverer=determine_coverer(line=10, favored_team='CHIEFS*'.replace('*', ''), underdog_team='CHARGERS'.replace('*', ''), favored_team_score=31, underdog_team_score=21)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='BEARS'), favored_team_score=21, underdog_team_score=19, status='F', coverer=determine_coverer(line=5, favored_team='BEARS'.replace('*', ''), underdog_team='VIKINGS*'.replace('*', ''), favored_team_score=21, underdog_team_score=19)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='PATRIOTS*'), favored_team_score=24, underdog_team_score=27, status='F', coverer=determine_coverer(line=17.5, favored_team='PATRIOTS*'.replace('*', ''), underdog_team='DOLPHINS'.replace('*', ''), favored_team_score=24, underdog_team_score=27)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='BUCCANEERS*'), favored_team_score=22, underdog_team_score=28, status='FO', coverer=determine_coverer(line=1, favored_team='BUCCANEERS*'.replace('*', ''), underdog_team='FALCONS'.replace('*', ''), favored_team_score=22, underdog_team_score=28)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='STEELERS'), favored_team_score=10, underdog_team_score=28, status='F', coverer=determine_coverer(line=2, favored_team='STEELERS'.replace('*', ''), underdog_team='RAVENS*'.replace('*', ''), favored_team_score=10, underdog_team_score=28)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='COWBOYS*'), favored_team_score=47, underdog_team_score=16, status='F', coverer=determine_coverer(line=12.5, favored_team='COWBOYS*'.replace('*', ''), underdog_team='COMMANDERS'.replace('*', ''), favored_team_score=47, underdog_team_score=16)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='BRONCOS*'), favored_team_score=16, underdog_team_score=15, status='F', coverer=determine_coverer(line=5.5, favored_team='BRONCOS*'.replace('*', ''), underdog_team='RAIDERS'.replace('*', ''), favored_team_score=16, underdog_team_score=15)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='TITANS'), favored_team_score=35, underdog_team_score=14, status='F', coverer=determine_coverer(line=10, favored_team='TITANS'.replace('*', ''), underdog_team='TEXANS*'.replace('*', ''), favored_team_score=35, underdog_team_score=14)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='COLTS'), favored_team_score=20, underdog_team_score=38, status='F', coverer=determine_coverer(line=5, favored_team='COLTS'.replace('*', ''), underdog_team='JAGUARS*'.replace('*', ''), favored_team_score=20, underdog_team_score=38)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='RAMS*'), favored_team_score=31, underdog_team_score=24, status='F', coverer=determine_coverer(line=7, favored_team='RAMS*'.replace('*', ''), underdog_team='CARDINALS'.replace('*', ''), favored_team_score=31, underdog_team_score=24)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='EAGLES'), favored_team_score=34, underdog_team_score=17, status='F', coverer=determine_coverer(line=4, favored_team='EAGLES'.replace('*', ''), underdog_team='GIANTS*'.replace('*', ''), favored_team_score=34, underdog_team_score=17)),
        Score(line_id=queries.get_line_id(season=season, week=week, team='49ERS'), favored_team_score=26, underdog_team_score=21, status='F', coverer=determine_coverer(line=3.5, favored_team='49ERS'.replace('*', ''), underdog_team='SEAHAWKS*'.replace('*', ''), favored_team_score=26, underdog_team_score=21)),
    ]
    db.session.add_all(scores)
    db.session.commit()

def downgrade():
    pass
