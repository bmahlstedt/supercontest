"""removing scores.coverer and picks.points

Revision ID: 89940b36ca12
Revises: 260f7788ae48
Create Date: 2023-02-21 16:26:38.836744

"""
from typing import Any
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

revision = '89940b36ca12'
down_revision = '260f7788ae48'
branch_labels = None
depends_on = None

op: Any


def upgrade():
    with op.batch_alter_table('lines', schema=None) as batch_op:
        batch_op.alter_column('id',
               existing_type=sa.BIGINT(),
               type_=sa.Integer(),
               existing_nullable=False)
        batch_op.alter_column('favored_team',
               existing_type=sa.TEXT(),
               type_=sa.String(),
               existing_nullable=False)
        batch_op.alter_column('underdog_team',
               existing_type=sa.TEXT(),
               type_=sa.String(),
               existing_nullable=False)
        batch_op.alter_column('home_team',
               existing_type=sa.TEXT(),
               type_=sa.String(),
               existing_nullable=True)

    with op.batch_alter_table('picks', schema=None) as batch_op:
        batch_op.alter_column('id',
               existing_type=sa.BIGINT(),
               type_=sa.Integer(),
               existing_nullable=False)
        batch_op.alter_column('user_id',
               existing_type=sa.BIGINT(),
               type_=sa.Integer(),
               existing_nullable=False)
        batch_op.alter_column('team',
               existing_type=sa.TEXT(),
               type_=sa.String(),
               existing_nullable=False)
        batch_op.drop_column('points')  # the actual change

    with op.batch_alter_table('scores', schema=None) as batch_op:
        batch_op.drop_column('coverer')  # the actual change

    with op.batch_alter_table('users', schema=None) as batch_op:
        batch_op.alter_column('id',
               existing_type=sa.BIGINT(),
               type_=sa.Integer(),
               existing_nullable=False)
        batch_op.alter_column('email',
               existing_type=sa.TEXT(),
               type_=sa.String(),
               existing_nullable=False)
        batch_op.alter_column('email_confirmed_at',
               existing_type=postgresql.TIMESTAMP(timezone=True),
               type_=sa.DateTime(),
               existing_nullable=True)
        batch_op.alter_column('password',
               existing_type=sa.TEXT(),
               type_=sa.String(),
               existing_nullable=False)
        batch_op.alter_column('first_name',
               existing_type=sa.TEXT(),
               type_=sa.String(),
               existing_nullable=True,
               existing_server_default=sa.text("''::text"))
        batch_op.alter_column('last_name',
               existing_type=sa.TEXT(),
               type_=sa.String(),
               existing_nullable=True,
               existing_server_default=sa.text("''::text"))


def downgrade():
    pass
