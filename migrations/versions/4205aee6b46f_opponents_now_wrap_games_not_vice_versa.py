"""opponents now wrap games not vice versa

Revision ID: 4205aee6b46f
Revises: 1ce22a7d5a87
Create Date: 2023-03-17 18:31:09.445826

"""
from typing import Any
from alembic import op
import sqlalchemy as sa

op: Any

# revision identifiers, used by Alembic.
revision = '4205aee6b46f'
down_revision = '1ce22a7d5a87'
branch_labels = None
depends_on = None


def upgrade():
    # Drop the col you added for testing in the last migration.
    op.drop_column('opponents', 'margin')
    # Add the FK to Game within the Opponent table.
    with op.batch_alter_table('opponents', schema=None) as batch_op:
        batch_op.add_column(sa.Column('game_id', sa.Integer(), nullable=True))
        batch_op.create_foreign_key(None, 'games', ['game_id'], ['id'])
    # Populate the game_id col.
    op.execute(
        'UPDATE opponents SET game_id = games.id FROM games '
        'WHERE opponents.id = games.opponent1_id OR opponents.id = games.opponent2_id'
    )
    # Make non-nullable.
    with op.batch_alter_table('opponents', schema=None) as batch_op:
        batch_op.alter_column('game_id', nullable=False)
    # Remove the FKs to Opponent within the Game table.
    with op.batch_alter_table('games', schema=None) as batch_op:
        batch_op.drop_constraint('games_opponent1_id_fkey', type_='foreignkey')
        batch_op.drop_constraint('games_opponent2_id_fkey', type_='foreignkey')
        batch_op.drop_column('opponent2_id')
        batch_op.drop_column('opponent1_id')
    # Remove the FK to Game within the Pick table.
    with op.batch_alter_table('picks', schema=None) as batch_op:
        batch_op.drop_constraint('picks_game_id_fkey', type_='foreignkey')
        batch_op.drop_column('game_id')


def downgrade():
    pass
