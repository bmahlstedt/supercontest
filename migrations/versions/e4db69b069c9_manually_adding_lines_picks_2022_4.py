"""manually adding lines picks 2022-4

Revision ID: e4db69b069c9
Revises: f5a5e0d42653
Create Date: 2022-10-03 15:49:59.839119

"""
from alembic import op
import sqlalchemy as sa

from supercontest.core.picks import commit_picks

# revision identifiers, used by Alembic.
revision = 'e4db69b069c9'
down_revision = 'f5a5e0d42653'
branch_labels = None
depends_on = None


def upgrade():
    commit_picks(15, 2022, 4, ['BEARS', 'RAVENS', 'CARDINALS', 'BRONCOS', 'PACKERS'], verify=False)  # andrewwhite175@gmail.com
    commit_picks(85, 2022, 4, ['VIKINGS', 'BILLS', 'CHARGERS', 'LIONS', 'PACKERS'], verify=False)  # anuj12@gmail.com
    commit_picks(40, 2022, 4, ['BEARS', 'BILLS', 'CHARGERS', 'FALCONS', 'CHIEFS'], verify=False)  # artavetisyan24@gmail.com
    commit_picks(90, 2022, 4, ['CARDINALS', 'RAIDERS', 'PACKERS', 'CHIEFS', 'RAMS'], verify=False)  # barrjon1988@gmail.com
    commit_picks(23, 2022, 4, ['DOLPHINS', 'TITANS', 'CHARGERS', 'SEAHAWKS', 'CARDINALS'], verify=False)  # brian.mahlstedt@gmail.com
    commit_picks(13, 2022, 4, ['VIKINGS', 'CHARGERS', 'LIONS', 'COWBOYS', 'RAMS'], verify=False)  # brittneygmerek@gmail.com
    commit_picks(20, 2022, 4, ['VIKINGS', 'TITANS', 'COWBOYS', 'CARDINALS', 'PATRIOTS'], verify=False)  # buckeyes64@hotmail.com
    commit_picks(77, 2022, 4, ['COLTS', 'LIONS', 'CARDINALS', 'RAIDERS', 'RAMS'], verify=False)  # calebgmerek1@gmail.com
    commit_picks(10, 2022, 4, ['BEARS', 'LIONS', 'JAGUARS', 'COWBOYS', 'CHIEFS'], verify=False)  # charner4@gmail.com
    commit_picks(70, 2022, 4, ['VIKINGS', 'BROWNS', 'CARDINALS', 'CHIEFS', '49ERS'], verify=False)  # exxxplosivo@gmail.com
    commit_picks(2, 2022, 4, ['VIKINGS', 'GIANTS', 'TEXANS', 'BROWNS', 'PANTHERS'], verify=False)  # fenton.craigalan@gmail.com
    commit_picks(84, 2022, 4, ['BENGALS', 'LIONS', 'JAGUARS', 'PANTHERS', 'RAMS'], verify=False)  # frye.garrett@gmail.com
    commit_picks(79, 2022, 4, ['BILLS', 'COWBOYS', 'BRONCOS', 'CHIEFS', 'RAMS'], verify=False)  # garcia.g86@gmail.com
    commit_picks(1, 2022, 4, ['BENGALS', 'BEARS', 'BILLS', 'JAGUARS', 'PACKERS'], verify=False)  # gjm112@gmail.com
    commit_picks(3, 2022, 4, ['TEXANS', 'JETS', 'JAGUARS', 'BRONCOS', 'PATRIOTS'], verify=False)  # grdich@gmail.com
    commit_picks(6, 2022, 4, ['VIKINGS', 'TEXANS', 'COWBOYS', 'BRONCOS', 'BUCCANEERS'], verify=False)  # jeremy.d.abramson@gmail.com
    commit_picks(91, 2022, 4, ['VIKINGS', 'RAVENS', 'BRONCOS', 'PACKERS', 'BUCCANEERS'], verify=False)  # joshua.d.merry@gmail.com
    commit_picks(14, 2022, 4, ['GIANTS', 'CHARGERS', 'JETS', 'COWBOYS', 'CARDINALS'], verify=False)  # jsatter4@me.com
    commit_picks(57, 2022, 4, ['TITANS', 'BEARS', 'CHARGERS', 'STEELERS', 'PACKERS'], verify=False)  # katilintierney@yahoo.com
    commit_picks(12, 2022, 4, ['TITANS', 'EAGLES', 'COWBOYS', 'CHIEFS', '49ERS'], verify=False)  # kengmerek@gmail.com
    commit_picks(31, 2022, 4, ['SAINTS', 'BEARS', 'TEXANS', 'JAGUARS', 'RAIDERS'], verify=False)  # lynell1308@gmail.com
    commit_picks(16, 2022, 4, ['GIANTS', 'BILLS', 'COWBOYS', 'CARDINALS', 'CHIEFS'], verify=False)  # lynne.defilippo@gmail.com
    commit_picks(72, 2022, 4, ['BILLS', 'FALCONS', 'RAIDERS', 'CHIEFS', '49ERS'], verify=False)  # Mark.geyen@jbgroup.com
    commit_picks(38, 2022, 4, ['COLTS', 'STEELERS', 'COWBOYS', 'RAIDERS', 'BUCCANEERS'], verify=False)  # markcoronella@gmail.com
    commit_picks(71, 2022, 4, ['VIKINGS', 'BILLS', 'CHARGERS', 'FALCONS', 'CARDINALS'], verify=False)  # markgeyen@yahoo.com
    commit_picks(78, 2022, 4, ['COLTS', 'GIANTS', 'CHARGERS', 'EAGLES', 'CARDINALS'], verify=False)  # matt.cioppa@gmail.com
    commit_picks(4, 2022, 4, ['TEXANS', 'SEAHAWKS', 'COMMANDERS', 'PANTHERS', 'PACKERS'], verify=False)  # mbpetty@gmail.com
    commit_picks(88, 2022, 4, ['GIANTS', 'CHARGERS', 'STEELERS', 'JAGUARS', 'COWBOYS'], verify=False)  # moser721@yahoo.com
    commit_picks(86, 2022, 4, ['COWBOYS', 'FALCONS', 'CARDINALS', 'RAIDERS', 'PATRIOTS'], verify=False)  # nicholas.galano@gmail.com
    commit_picks(50, 2022, 4, ['BILLS', 'CHARGERS', 'LIONS', 'EAGLES'], verify=False)  # omar.mc93@gmail.com
    commit_picks(58, 2022, 4, ['COLTS', 'RAVENS', 'CHARGERS', 'BROWNS', 'RAMS'], verify=False)  # ptwist.jms@gmail.com
    commit_picks(25, 2022, 4, ['VIKINGS', 'LIONS', 'COWBOYS', 'CARDINALS', 'CHIEFS'], verify=False)  # rajarshi692@gmail.com
    commit_picks(33, 2022, 4, ['CHARGERS', 'CARDINALS', 'RAIDERS', 'CHIEFS', 'RAMS'], verify=False)  # remy.verdeille@gmail.com
    commit_picks(17, 2022, 4, ['SEAHAWKS', 'STEELERS', 'JAGUARS', 'BROWNS', 'CARDINALS'], verify=False)  # rossmullenix@gmail.com
    commit_picks(32, 2022, 4, ['BENGALS', 'CHARGERS', 'STEELERS', 'COWBOYS', 'RAIDERS'], verify=False)  # swigmore80@gmail.com


def downgrade():
    # delete all picks from week 4 and rerun upgrade
    pass
