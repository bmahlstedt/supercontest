"""adding statuses table

Revision ID: 96c8d56cebb7
Revises: fa08d62190dc
Create Date: 2023-02-26 17:46:38.398180

"""
from typing import Any
from alembic import op
import sqlalchemy as sa

op: Any  # Alembic has unknowns in its own types and I don't want to stub it.

# revision identifiers, used by Alembic.
revision = '96c8d56cebb7'
down_revision = 'fa08d62190dc'
branch_labels = None
depends_on = None


def upgrade():
    # Create the new table.
    statuses_table = op.create_table(
        'statuses',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('abbv', sa.String(), nullable=False),
        sa.Column('espn_abbv', sa.String(), nullable=False),
        sa.Column('unstarted', sa.Boolean(), nullable=False),
        sa.Column('inprogress', sa.Boolean(), nullable=False),
        sa.Column('finished', sa.Boolean(), nullable=False),
        sa.PrimaryKeyConstraint('id'),
    )
    # Insert the static data into the new table.
    statuses_rows = [
        {"name": "Not Started", "abbv": "P", "espn_abbv": "P", "unstarted": True, "inprogress": False, "finished": False},
        {"name": "Delayed", "abbv": "D", "espn_abbv": "DELAYED", "unstarted": False, "inprogress": True, "finished": False},
        {"name": "1st Quarter", "abbv": "1", "espn_abbv": "1ST", "unstarted": False, "inprogress": True, "finished": False},
        {"name": "2nd Quarter", "abbv": "2", "espn_abbv": "2ND", "unstarted": False, "inprogress": True, "finished": False},
        {"name": "Halftime", "abbv": "H", "espn_abbv": "HALFTIME", "unstarted": False, "inprogress": True, "finished": False},
        {"name": "3rd Quarter", "abbv": "3", "espn_abbv": "3RD", "unstarted": False, "inprogress": True, "finished": False},
        {"name": "4th Quarter", "abbv": "4", "espn_abbv": "4TH", "unstarted": False, "inprogress": True, "finished": False},
        {"name": "Overtime", "abbv": "O", "espn_abbv": "O", "unstarted": False, "inprogress": True, "finished": False},
        {"name": "Final", "abbv": "F", "espn_abbv": "FINAL", "unstarted": False, "inprogress": False, "finished": True},
        {"name": "Final (OT)", "abbv": "FO", "espn_abbv": "OT", "unstarted": False, "inprogress": False, "finished": True},
    ]
    op.bulk_insert(table=statuses_table, rows=statuses_rows)
    # Create the new col and FK. Make the column nullable to start (we need to populate it still).
    op.add_column(
        table_name="scores",
        column=sa.Column("status_id", sa.Integer(), nullable=True),
    )
    op.create_foreign_key(
        constraint_name=None,
        source_table="scores",
        local_cols=["status_id"],
        referent_table="statuses",
        remote_cols=["id"],
    )
    # Populate the col by doing a lookup of the current col value against the name in the new table.
    op.execute(
        "UPDATE scores SET status_id = statuses.id "
        "FROM statuses WHERE statuses.espn_abbv = scores.status"
    )
    # Now change to not nullable, the desired setting.
    op.alter_column(table_name="scores", column_name="status_id", nullable=False)
    # Drop the old column.
    op.drop_column(table_name="scores", column_name="status")


def downgrade():
    pass
