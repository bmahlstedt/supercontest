"""actually adding margin

Revision ID: b2886ac3feda
Revises: 4205aee6b46f
Create Date: 2023-03-17 18:45:23.801185

"""
from typing import Any
from alembic import op
import sqlalchemy as sa

op: Any

# revision identifiers, used by Alembic.
revision = 'b2886ac3feda'
down_revision = '4205aee6b46f'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('opponents', sa.Column('opponent_score', sa.Integer(), nullable=True))
    op.execute(
        'UPDATE opponents SET opponent_score = subquery.score '
        'FROM (SELECT opponents2.id, opponents2.game_id, opponents2.score '
        '      FROM opponents AS opponents2) AS subquery '
        'WHERE opponents.game_id = subquery.game_id AND opponents.id != subquery.id'
    )
    op.alter_column('opponents', 'opponent_score', nullable=False)

    op.add_column('opponents', sa.Column('margin', sa.Float(), nullable=True))
    op.execute(
        'UPDATE opponents SET margin = score - opponent_score - subquery.line '
        'FROM (SELECT '
        '          opponents2.id, '
        '          CASE '
        '              WHEN opponents2.prediction_id = 1 THEN -games.line '
        '              ELSE games.line '
        '          END AS line '
        '      FROM opponents as opponents2 '
        '          JOIN games on opponents2.game_id = games.id '
        '      ) AS subquery '
        'WHERE opponents.id = subquery.id'
    )
    op.alter_column('opponents', 'margin', nullable=False)


def downgrade():
    op.drop_column('opponents', 'opponent_score')
    op.drop_column('opponents', 'margin')
