"""adding lines 2023 week 3

Revision ID: c120a4ee964c
Revises: c383cb62ad76
Create Date: 2023-09-21 15:21:56.564227

"""
from supercontest.core import lines
from supercontest.dbsession import queries, commits


# revision identifiers, used by Alembic.
revision = 'c120a4ee964c'
down_revision = 'c383cb62ad76'
branch_labels = None
depends_on = None


def upgrade():
    # general
    season = 2023
    week = 3

    # lines
    fetched_lines = lines.fetch_lines(week=week)
    week_id = queries.get_week_id(season=season, week=week)
    commits.insert_contestants(week_id=week_id, linesfetch=fetched_lines)

def downgrade():
    pass
