"""adding opponents table and merging lines scores to games table

Revision ID: 129c511a81f3
Revises: 4c29e8587950
Create Date: 2023-03-16 19:24:12.488512

"""
from typing import Any
from alembic import op
import sqlalchemy as sa

op: Any

# revision identifiers, used by Alembic.
revision = '129c511a81f3'
down_revision = '4c29e8587950'
branch_labels = None
depends_on = None


def upgrade():
    # Create the new opponents table.
    op.create_table(
        'opponents',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('score', sa.Integer(), nullable=True),
        sa.Column('team_id', sa.Integer(), nullable=True),
        sa.Column('prediction_id', sa.Integer(), nullable=True),
        sa.Column('location_id', sa.Integer(), nullable=True),
        sa.Column('coverage_id', sa.Integer(), nullable=True),
        sa.Column('line_id', sa.Integer(), nullable=False),  # only for migration
        sa.Column('team', sa.Integer(), nullable=False),  # only for migration
        sa.ForeignKeyConstraint(['coverage_id'], ['coverages.id'], ),
        sa.ForeignKeyConstraint(['location_id'], ['locations.id'], ),
        sa.ForeignKeyConstraint(['prediction_id'], ['predictions.id'], ),
        sa.ForeignKeyConstraint(['team_id'], ['teams.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    # Move team/prediction/location cols over from the lines table for team1.
    op.execute(
        'INSERT INTO opponents (team_id, prediction_id, location_id, line_id, team) '
        'SELECT team1_id, team1_prediction_id, team1_location_id, id, 1 FROM lines'
    )
    # Move team/prediction/location cols over from the lines table for team2.
    op.execute(
        'INSERT INTO opponents (team_id, prediction_id, location_id, line_id, team) '
        'SELECT team2_id, team2_prediction_id, team2_location_id, id, 2 FROM lines'
    )
    # Move score/coverage cols over from the scores table for team1.
    op.execute(
        'UPDATE opponents SET score = team1_score, coverage_id = team1_coverage_id FROM scores '
        'WHERE opponents.line_id = scores.line_id AND opponents.team = 1'
    )
    # Move team/prediction/location cols over from the lines table for team2.
    op.execute(
        'UPDATE opponents SET score = team2_score, coverage_id = team2_coverage_id FROM scores '
        'WHERE opponents.line_id = scores.line_id AND opponents.team = 2'
    )
    # Make non-nullable.
    with op.batch_alter_table('opponents', schema=None) as batch_op:
        batch_op.alter_column('score', nullable=False)
        batch_op.alter_column('team_id', nullable=False)
        batch_op.alter_column('prediction_id', nullable=False)
        batch_op.alter_column('location_id', nullable=False)
        batch_op.alter_column('coverage_id', nullable=False)
    # Change the lines table to games. Replace all the team1/2 stuff with just opponent1/2.
    op.rename_table('lines', 'games')
    with op.batch_alter_table('games', schema=None) as batch_op:
        batch_op.add_column(sa.Column('status_id', sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column('opponent1_id', sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column('opponent2_id', sa.Integer(), nullable=True))
        batch_op.create_foreign_key(None, 'statuses', ['status_id'], ['id'])
        batch_op.create_foreign_key(None, 'opponents', ['opponent1_id'], ['id'])
        batch_op.create_foreign_key(None, 'opponents', ['opponent2_id'], ['id'])
        batch_op.drop_constraint('lines_team1_id_fkey', type_='foreignkey')
        batch_op.drop_constraint('lines_team1_prediction_id_fkey', type_='foreignkey')
        batch_op.drop_constraint('lines_team1_location_id_fkey', type_='foreignkey')
        batch_op.drop_constraint('lines_team2_id_fkey', type_='foreignkey')
        batch_op.drop_constraint('lines_team2_prediction_id_fkey', type_='foreignkey')
        batch_op.drop_constraint('lines_team2_location_id_fkey', type_='foreignkey')
        batch_op.drop_column('team1_id')
        batch_op.drop_column('team1_prediction_id')
        batch_op.drop_column('team1_location_id')
        batch_op.drop_column('team2_id')
        batch_op.drop_column('team2_prediction_id')
        batch_op.drop_column('team2_location_id')
    # Move status over to the games table (from scores).
    op.execute(
        'UPDATE games SET status_id = scores.status_id FROM scores '
        'WHERE games.id = scores.line_id'
    )
    # Point the opponent FKs to the new opponents table.
    op.execute(
        'UPDATE games SET opponent1_id = opponents.id FROM opponents '
        'WHERE games.id = opponents.line_id AND opponents.team = 1'
    )
    op.execute(
        'UPDATE games SET opponent2_id = opponents.id FROM opponents '
        'WHERE games.id = opponents.line_id AND opponents.team = 2'
    )
    # Make non-nullable.
    with op.batch_alter_table('games', schema=None) as batch_op:
        batch_op.alter_column('status_id', nullable=False)
        batch_op.alter_column('opponent1_id', nullable=False)
        batch_op.alter_column('opponent2_id', nullable=False)
    # Drop the old scores table. Its cols are in opponents/games now.
    op.drop_table('scores')
    # Light modifications to the picks table. This is just a modification of col names and FK names.
    with op.batch_alter_table('picks', schema=None) as batch_op:
        batch_op.add_column(sa.Column('game_id', sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column('opponent_id', sa.Integer(), nullable=True))
        batch_op.create_foreign_key(None, 'games', ['game_id'], ['id'])
        batch_op.create_foreign_key(None, 'opponents', ['opponent_id'], ['id'])
    op.execute(
        'UPDATE picks SET game_id = line_id'
    )
    # Update the FK from Team to Opponent.
    op.execute(
        'UPDATE picks SET opponent_id = opponents.id FROM opponents '
        'WHERE picks.line_id = opponents.line_id AND picks.team_id = opponents.team_id'
    )
    # Drop the old cols and make new ones non-nullable.
    with op.batch_alter_table('picks', schema=None) as batch_op:
        batch_op.alter_column('game_id', nullable=False)
        batch_op.alter_column('opponent_id', nullable=False)
        batch_op.drop_constraint('picks_line_id_fkey', type_='foreignkey')
        batch_op.drop_constraint('picks_team_id_fkey', type_='foreignkey')
        batch_op.drop_column('line_id')
        batch_op.drop_column('team_id')
    # Drop the opponents.line_id and opponents.team cols we created just to make migration easier.
    with op.batch_alter_table('opponents', schema=None) as batch_op:
        batch_op.drop_column('line_id')
        batch_op.drop_column('team')


def downgrade():
    pass
