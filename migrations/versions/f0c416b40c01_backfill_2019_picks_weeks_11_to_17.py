"""backfill 2019 picks weeks 11 to 17

Revision ID: f0c416b40c01
Revises: 1b7e8c38b8b2
Create Date: 2023-01-16 20:06:23.379105

"""
from alembic import op
import sqlalchemy as sa

import random
from supercontest.dbsession import queries, commits


# revision identifiers, used by Alembic.
revision = 'f0c416b40c01'
down_revision = '1b7e8c38b8b2'
branch_labels = None
depends_on = None


def upgrade():
    season = 2019
    weeks = range(11, 17+1)

    # Map user IDs to lists of points per week.
    all_points_all_weeks_per_user = {
        4: [5, 2, 3, 2, 4, 4, 2],
        5: [5, 3, 3, 3, 3, 3.5, 2],
        12: [4, 4, 5, 4, 2, 4, 2],
        51: [5, 3, 3, 4, 3, 4, 2],
        27: [3, 4, 3, 3, 3, 2.5, 2],
        46: [2, 3, 4, 4, 3, 1, 2],
        17: [3, 4, 2, 3.5, 3, 4, 2],
        53: [4, 5, 1, 2, 3, 1.5, 3],
        33: [5, 3, 3, 2, 3, 3.5, 2],
        40: [1, 5, 4, 2, 4, 3.5, 4],
        2: [4, 4, 2, 2, 3, 4, 2],
        1: [1, 3, 1, 3, 3, 3, 5],
        6: [3, 4, 3, 4, 3, 4, 3],
        14: [5, 4, 1, 2.5, 4, 3.5, 3],
        3: [3, 2, 3, 3, 2, 4, 3],
        54: [4, 0, 3, None, 3, 4, 2],
        23: [3, 4, 2, 3, 4, 4, 2],
        15: [3, 3, 1, 4, 2, 4, None],
        49: [3, 2, 2, 3, 4, 4, 3],
        11: [3, 3, 2, 3, 4, 5, 3],
        10: [2, 3, 3, 4, 2, 2, 1],
        13: [4, 1, 3, 3, 3, 3, 1],
        25: [2, 2, 2, 2.5, 2, 5, 1],
        22: [0, 3, 2, 1.5, 1, 2, 4],
        50: [2, 3, 2, 4, 1, 2, None],
        57: [2, 1, 3, 1, 3, 0, 2],
        9: [2, 3, 1, 1.5, 2, 1, 4],
        26: [3, 3, 3, 0, 2, 1.5, 3],
        31: [4, 1, 1, 3, 2, 3, 1],
        30: [4, 3, 2, 2.5, 3, 2, 2],
        20: [2, 2, 3, 2.5, 2, 2.5, None],
        36: [None, 1, 2, 3, 4, 2, None],
        32: [3, 1, 2, 1, 4, 2, 3],
        28: [5, 2, None, None, None, 4.5, None],
        58: [4, 1, 2, 1, 3, None, 3],
        38: [3, 1, None, 0, None, None, None],
    }

    # Reshape so keys are weeks, so we can iterate over weeks.
    all_points_all_users_per_week = {week: {} for week in weeks}
    # week: {user: points, user: points, ...}
    for user_id, points_per_week in all_points_all_weeks_per_user.items():
        for index, points in enumerate(points_per_week):
            all_points_all_users_per_week[index+11][user_id] = points

    # Randomly pick teams, based on coverers/pushers/noncoverers, so that
    # the total points are achieved.
    all_teams_all_users_per_week = {week: {} for week in weeks}
    # week: {user: [teams], user: [teams], ...}
    for week, user_points in all_points_all_users_per_week.items():
        coverers, pushers, noncoverers, opponent_map = \
            queries.get_coverage_results_for_week(season=season, week=week)
        for user_id, points in user_points.items():
            # Make copies of the lists, since we'll remove options are they're
            # selected (can't pick the same team twice, can't choose both teams
            # in a matchup). Don't need to do this for pushers, since we'll
            # never select more than 1 push in a week.
            valid_coverers = coverers[:]
            valid_noncoverers = noncoverers[:]
            # check if they submitted 0 that week, and skip if so
            if points is None:
                continue
            teams = []
            # handle the half point if they had a push
            if not float(points).is_integer():
                teams.append(random.choice(pushers))
                points = int(points - 0.5)
            # handle covers
            for _ in range(points):
                team = random.choice(valid_coverers)
                teams.append(team)
                valid_coverers.remove(team)
                valid_noncoverers.remove(opponent_map[team])
            # handle noncovers
            for _ in range(5 - len(teams)):
                team = random.choice(valid_noncoverers)
                teams.append(team)
                valid_noncoverers.remove(team)
                valid_coverers.remove(opponent_map[team])
            all_teams_all_users_per_week[week][user_id] = teams

    # Commit.
    for week in weeks:
        for user_id, teams in all_teams_all_users_per_week[week].items():
            commits._commit_picks(
                season=season,
                week=week,
                user_id=user_id,
                teams=teams,
            )


def downgrade():
    pass
