"""adding 2021-2022 season

Revision ID: 8e05905f1f92
Revises: b88c03df3187
Create Date: 2021-09-04 17:14:34.392261

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8e05905f1f92'
down_revision = 'b88c03df3187'
branch_labels = None
depends_on = None


def upgrade():
    ########################## THIS IS THE SECTION TO MODIFY ##################
    season = 2021  # the year in which it starts (not the next year when the season ends)
    season_id = 4  # just query the table and increment this manually for the next season
    start_date = '2021-09-08'  # this is the date BEFORE the thursday game of week 1 (so wednesday)
    ########################## THIS IS THE SECTION TO MODIFY ##################

    # Add seasons
    op.execute('INSERT INTO seasons (season, season_start, season_end) '
               'VALUES ({}, \'{}-09-01\', \'{}-02-01\')'.format(season, season, season+1))
    # Add weeks
    op.alter_column('weeks', 'week_start', nullable=True)
    op.alter_column('weeks', 'week_end', nullable=True)
    weeks = range(1, 18+1)
    for week in weeks:
        op.execute('INSERT INTO weeks (season_id, week) '
                   'VALUES ({}, {})'.format(season_id, week))
        op.execute('UPDATE weeks '
                   'SET week_start = TIMESTAMP \'{} 17:00:00\' + '  # season opener always pdt
                                    'INTERVAL \'7 hours\' + '  # convert to utc
                                    'INTERVAL \'1 week\' * (weeks2.rn - 1) '
                                    'FROM (SELECT id, row_number() OVER (ORDER BY week) rn '
                                          'FROM weeks '
                                          'WHERE season_id = {} '
                                          ') weeks2 '
                                    'WHERE weeks2.id = weeks.id'.format(start_date, season_id))
    op.execute('UPDATE weeks '
               'SET week_end = week_start + INTERVAL \'1 week\'')
    op.alter_column('weeks', 'week_start', nullable=False)
    op.alter_column('weeks', 'week_end', nullable=False)


def downgrade():
    op.execute('DELETE FROM weeks '
               'WHERE season_id = {}'.format(season_id))
    op.execute('DELETE FROM seasons '
               'WHERE season_id = {}'.format(season_id))
