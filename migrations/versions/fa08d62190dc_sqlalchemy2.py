"""sqlalchemy2

Revision ID: fa08d62190dc
Revises: 89940b36ca12
Create Date: 2023-02-25 01:26:29.541758

"""
from typing import Any
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'fa08d62190dc'
down_revision = '89940b36ca12'
branch_labels = None
depends_on = None

op: Any


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('league_user_association', schema=None) as batch_op:
        batch_op.alter_column('league_id',
               existing_type=sa.INTEGER(),
               nullable=False)
        batch_op.alter_column('user_id',
               existing_type=sa.INTEGER(),
               nullable=False)

    with op.batch_alter_table('role_user_association', schema=None) as batch_op:
        batch_op.alter_column('role_id',
               existing_type=sa.INTEGER(),
               nullable=False)
        batch_op.alter_column('user_id',
               existing_type=sa.INTEGER(),
               nullable=False)

    with op.batch_alter_table('users', schema=None) as batch_op:
        batch_op.alter_column('email_confirmed_at',
               existing_type=postgresql.TIMESTAMP(),
               type_=sa.DateTime(timezone=True),
               existing_nullable=True)

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('users', schema=None) as batch_op:
        batch_op.alter_column('email_confirmed_at',
               existing_type=sa.DateTime(timezone=True),
               type_=postgresql.TIMESTAMP(),
               existing_nullable=True)

    with op.batch_alter_table('role_user_association', schema=None) as batch_op:
        batch_op.alter_column('user_id',
               existing_type=sa.INTEGER(),
               nullable=True)
        batch_op.alter_column('role_id',
               existing_type=sa.INTEGER(),
               nullable=True)

    with op.batch_alter_table('league_user_association', schema=None) as batch_op:
        batch_op.alter_column('user_id',
               existing_type=sa.INTEGER(),
               nullable=True)
        batch_op.alter_column('league_id',
               existing_type=sa.INTEGER(),
               nullable=True)

    # ### end Alembic commands ###
