"""adding roles

Revision ID: 35be3987283c
Revises: 35cea3b5c1ff
Create Date: 2019-10-09 15:15:01.910683

"""
from alembic import op
import sqlalchemy as sa

from sqlalchemy.schema import Sequence, CreateSequence

# revision identifiers, used by Alembic.
revision = '35be3987283c'
down_revision = '35cea3b5c1ff'
branch_labels = None
depends_on = None


def upgrade():
    # Create the roles table.
    op.execute(CreateSequence(Sequence('roles_id_seq')))
    op.create_table('roles',
                    sa.Column('id',
                              sa.Integer(),
                              server_default=sa.text("nextval('roles_id_seq')"),
                              nullable=False),
                    sa.Column('name',
                              sa.String(),
                              nullable=False),
                    sa.PrimaryKeyConstraint('id')
    )
    # Create the role-user association table.
    op.create_table('role_user_association',
                    sa.Column('role_id', sa.Integer(), nullable=True),
                    sa.Column('user_id', sa.Integer(), nullable=True),
                    sa.ForeignKeyConstraint(['role_id'], ['roles.id'], ),
                    sa.ForeignKeyConstraint(['user_id'], ['users.id'], )
    )
    # Add the admin role.
    op.execute("INSERT INTO roles (name) VALUES ('Admin')")
    # Add bmahlstedt as admin.
    op.execute("INSERT INTO role_user_association (role_id, user_id) VALUES (1, 23)")


def downgrade():
    op.drop_table('role_user_association')
    op.drop_table('roles')
