"""renaming opponent table to contestant

Revision ID: 2c2070d7c3e1
Revises: b2886ac3feda
Create Date: 2023-03-18 19:38:21.388705

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = '2c2070d7c3e1'
down_revision = 'b2886ac3feda'
branch_labels = None
depends_on = None


def upgrade():
    op.rename_table('opponents', 'contestants')


def downgrade():
    op.rename_table('contestants', 'opponents')
