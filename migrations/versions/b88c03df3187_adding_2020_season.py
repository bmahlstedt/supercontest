"""adding 2020 season

Revision ID: b88c03df3187
Revises: 59d9f966c571
Create Date: 2020-09-12 19:00:49.983593

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b88c03df3187'
down_revision = '59d9f966c571'
branch_labels = None
depends_on = None


def upgrade():
    ########################## THIS IS THE SECTION TO MODIFY ##################
    season = 2020  # the year in which it starts (not the next year when the season ends)
    season_id = 3  # just query the table and increment this manually for the next season
    start_date = '2020-09-09'  # this is the date BEFORE the thursday game of week 1 (so wednesday)
    ########################## THIS IS THE SECTION TO MODIFY ##################

    # Add seasons
    op.execute('INSERT INTO seasons (season, season_start, season_end) '
               'VALUES ({}, \'{}-09-01\', \'{}-02-01\')'.format(season, season, season+1))
    # Add weeks
    op.alter_column('weeks', 'week_start', nullable=True)
    op.alter_column('weeks', 'week_end', nullable=True)
    weeks = range(1, 17+1)
    for week in weeks:
        op.execute('INSERT INTO weeks (season_id, week) '
                   'VALUES ({}, {})'.format(season_id, week))
        op.execute('UPDATE weeks '
                   'SET week_start = TIMESTAMP \'{} 17:00:00\' + '  # season opener always pdt
                                    'INTERVAL \'7 hours\' + '  # convert to utc
                                    'INTERVAL \'1 week\' * (weeks2.rn - 1) '
                                    'FROM (SELECT id, row_number() OVER (ORDER BY week) rn '
                                          'FROM weeks '
                                          'WHERE season_id = {} '
                                          ') weeks2 '
                                    'WHERE weeks2.id = weeks.id'.format(start_date, season_id))
    op.execute('UPDATE weeks '
               'SET week_end = week_start + INTERVAL \'1 week\'')
    op.alter_column('weeks', 'week_start', nullable=False)
    op.alter_column('weeks', 'week_end', nullable=False)

def downgrade():
    op.execute('DELETE FROM weeks '
               'WHERE season_id = {}'.format(season_id))
    op.execute('DELETE FROM seasons '
               'WHERE season_id = {}'.format(season_id))
