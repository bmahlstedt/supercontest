"""add seasons weeks 2014-2017

Revision ID: 409b1415201d
Revises: f0c416b40c01
Create Date: 2023-01-17 13:33:41.552791

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '409b1415201d'
down_revision = 'f0c416b40c01'
branch_labels = None
depends_on = None


def upgrade():
    season = 2014
    season_id = 6
    start_date = '2014-09-04'
    op.execute('INSERT INTO seasons (season, season_start, season_end) '
               'VALUES ({}, \'{}-09-01\', \'{}-02-01\')'.format(season, season, season+1))
    op.alter_column('weeks', 'week_start', nullable=True)
    op.alter_column('weeks', 'week_end', nullable=True)
    weeks = range(1, 17+1)
    for week in weeks:
        op.execute('INSERT INTO weeks (season_id, week) '
                   'VALUES ({}, {})'.format(season_id, week))
        op.execute('UPDATE weeks '
                   'SET week_start = TIMESTAMP \'{} 17:00:00\' + '
                                    'INTERVAL \'7 hours\' + '
                                    'INTERVAL \'1 week\' * (weeks2.rn - 1) '
                                    'FROM (SELECT id, row_number() OVER (ORDER BY week) rn '
                                          'FROM weeks '
                                          'WHERE season_id = {} '
                                          ') weeks2 '
                                    'WHERE weeks2.id = weeks.id'.format(start_date, season_id))
    op.execute('UPDATE weeks '
               'SET week_end = week_start + INTERVAL \'1 week\'')
    op.alter_column('weeks', 'week_start', nullable=False)
    op.alter_column('weeks', 'week_end', nullable=False)

    season = 2015
    season_id = 7
    start_date = '2015-09-09'
    op.execute('INSERT INTO seasons (season, season_start, season_end) '
               'VALUES ({}, \'{}-09-01\', \'{}-02-01\')'.format(season, season, season+1))
    op.alter_column('weeks', 'week_start', nullable=True)
    op.alter_column('weeks', 'week_end', nullable=True)
    weeks = range(1, 17+1)
    for week in weeks:
        op.execute('INSERT INTO weeks (season_id, week) '
                   'VALUES ({}, {})'.format(season_id, week))
        op.execute('UPDATE weeks '
                   'SET week_start = TIMESTAMP \'{} 17:00:00\' + '
                                    'INTERVAL \'7 hours\' + '
                                    'INTERVAL \'1 week\' * (weeks2.rn - 1) '
                                    'FROM (SELECT id, row_number() OVER (ORDER BY week) rn '
                                          'FROM weeks '
                                          'WHERE season_id = {} '
                                          ') weeks2 '
                                    'WHERE weeks2.id = weeks.id'.format(start_date, season_id))
    op.execute('UPDATE weeks '
               'SET week_end = week_start + INTERVAL \'1 week\'')
    op.alter_column('weeks', 'week_start', nullable=False)
    op.alter_column('weeks', 'week_end', nullable=False)

    season = 2016
    season_id = 8
    start_date = '2016-09-07'
    op.execute('INSERT INTO seasons (season, season_start, season_end) '
               'VALUES ({}, \'{}-09-01\', \'{}-02-01\')'.format(season, season, season+1))
    op.alter_column('weeks', 'week_start', nullable=True)
    op.alter_column('weeks', 'week_end', nullable=True)
    weeks = range(1, 17+1)
    for week in weeks:
        op.execute('INSERT INTO weeks (season_id, week) '
                   'VALUES ({}, {})'.format(season_id, week))
        op.execute('UPDATE weeks '
                   'SET week_start = TIMESTAMP \'{} 17:00:00\' + '
                                    'INTERVAL \'7 hours\' + '
                                    'INTERVAL \'1 week\' * (weeks2.rn - 1) '
                                    'FROM (SELECT id, row_number() OVER (ORDER BY week) rn '
                                          'FROM weeks '
                                          'WHERE season_id = {} '
                                          ') weeks2 '
                                    'WHERE weeks2.id = weeks.id'.format(start_date, season_id))
    op.execute('UPDATE weeks '
               'SET week_end = week_start + INTERVAL \'1 week\'')
    op.alter_column('weeks', 'week_start', nullable=False)
    op.alter_column('weeks', 'week_end', nullable=False)

    season = 2017
    season_id = 9
    start_date = '2017-09-06'
    op.execute('INSERT INTO seasons (season, season_start, season_end) '
               'VALUES ({}, \'{}-09-01\', \'{}-02-01\')'.format(season, season, season+1))
    op.alter_column('weeks', 'week_start', nullable=True)
    op.alter_column('weeks', 'week_end', nullable=True)
    weeks = range(1, 17+1)
    for week in weeks:
        op.execute('INSERT INTO weeks (season_id, week) '
                   'VALUES ({}, {})'.format(season_id, week))
        op.execute('UPDATE weeks '
                   'SET week_start = TIMESTAMP \'{} 17:00:00\' + '
                                    'INTERVAL \'7 hours\' + '
                                    'INTERVAL \'1 week\' * (weeks2.rn - 1) '
                                    'FROM (SELECT id, row_number() OVER (ORDER BY week) rn '
                                          'FROM weeks '
                                          'WHERE season_id = {} '
                                          ') weeks2 '
                                    'WHERE weeks2.id = weeks.id'.format(start_date, season_id))
    op.execute('UPDATE weeks '
               'SET week_end = week_start + INTERVAL \'1 week\'')
    op.alter_column('weeks', 'week_start', nullable=False)
    op.alter_column('weeks', 'week_end', nullable=False)


def downgrade():
    pass
