"""changing opponent to contestant

Revision ID: 0da6b8b48f2b
Revises: 2c2070d7c3e1
Create Date: 2023-03-20 00:16:51.393895

"""
from typing import Any
from alembic import op

op: Any

# revision identifiers, used by Alembic.
revision = '0da6b8b48f2b'
down_revision = '2c2070d7c3e1'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column("picks", "opponent_id", new_column_name="contestant_id")


def downgrade():
    op.alter_column("picks", "contestant_id", new_column_name="opponent_id")
