"""changing statuses nfl to espn

Revision ID: 260f7788ae48
Revises: 6c52ea2689d6
Create Date: 2023-02-05 00:27:49.824790

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '260f7788ae48'
down_revision = '6c52ea2689d6'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("UPDATE scores SET status='FINAL' WHERE status='F'")
    op.execute("UPDATE scores SET status='OT' WHERE status='FO'")


def downgrade():
    op.execute("UPDATE scores SET status='F' WHERE status='FINAL'")
    op.execute("UPDATE scores SET status='FO' WHERE status='OT'")
