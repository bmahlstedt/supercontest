"""adding 2023 2024 season

Revision ID: e67a7d9a7a77
Revises: 0da6b8b48f2b
Create Date: 2023-09-13 23:51:53.499293

"""
from sqlalchemy import text
from supercontest.models import db, Season, League

# revision identifiers, used by Alembic.
revision = 'e67a7d9a7a77'
down_revision = '0da6b8b48f2b'
branch_labels = None
depends_on = None

def upgrade():
    ########################## THIS IS THE SECTION TO MODIFY ##################
    year = 2023  # the year in which it starts (not the next year when the season ends)
    first_wed_MM_DD = "09-06"  # this is the wednesday (1 day) before TNF week 1
    dst_shift_week = 9  # the week DURING which daylight savings time shifts (btw wed -> tues)
    ################################ END MODIFY SECTION #######################

    # Secondary configurations. You probably don't have to change these.
    weeks = 18  # number of weeks in the season
    season_start_MM_DD = "09-01"  # september 1st, arbitrary, just enough before season starts
    season_end_MM_DD = "02-01"  # february 1st, arbitrary, just enough after season ends
    season_start = f"'{year}-{season_start_MM_DD}'"
    season_end = f"'{year + 1}-{season_end_MM_DD}'"
    first_wed_date = f"{year}-{first_wed_MM_DD}"
    first_wed_time = "17:00:00"  # 5pm PDT
    delta_to_utc = "7 hours"  # difference from PDT to UTC
    first_wed_datetime = f"TIMESTAMP '{first_wed_date} {first_wed_time}'"  # for pg
    utc_interval = f"INTERVAL '{delta_to_utc}'"  # for pg
    week_length_interval = "INTERVAL '1 week'"  # for pg
    dst_interval = "INTERVAL '1 hour'"  # for pg

    # Add season and get its ID.
    season = Season(season=year, season_start=season_start, season_end=season_end)
    db.session.add(season)
    db.session.commit()
    db.session.refresh(season)  # ID is determined from the sequence on commit

    # Add weeks. Could switch to ORM.
    for week in range(1, weeks + 1):
        num_weeks_interval = f"{week_length_interval} * {week - 1}"
        week_start = " + ".join([first_wed_datetime, utc_interval, num_weeks_interval])
        week_end = " + ".join([week_start, week_length_interval])
        db.session.execute(text(
            "INSERT INTO weeks (season_id, week, week_start, week_end) "
            f"VALUES ({season.id}, {week}, {week_start}, {week_end})"

        ))

    # Handle daylight savings time. Could switch to ORM.
    db.session.execute(text(
        f"UPDATE weeks SET week_end = week_end + {dst_interval}"
        f"WHERE season_id = {season.id} AND week >= {dst_shift_week}"
    ))
    db.session.execute(text(
        f"UPDATE weeks SET week_start = week_start + {dst_interval}"
        f"WHERE season_id = {season.id} AND week >= {dst_shift_week + 1}"
    ))

    # Create new paid league.
    db.session.add(League(name='Paid', season_id=season.id))
    db.session.commit()


def downgrade():
    pass
