API
===

Click below to traverse the autodocs for the application.

.. autosummary::
   :toctree: modules
   :template: module.rst
   :recursive:

   supercontest
