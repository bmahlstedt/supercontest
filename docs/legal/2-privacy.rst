Privacy Policy
==============

*Last Updated: 2020-01-01*

1. **Introduction**. South Bay Supercontest ("we", "us", "our") respects your privacy and is
committed to protecting it through our compliance with this policy. This policy describes the types
of information we may collect from you or that you may provide when you visit our website
(our "Website") and our practices for collecting, using, maintaining, protecting, and
disclosing that information.

2. **Information We Collect**. We collect several types of information from and about users of
our Website, specifically:

* Information by which you may be personally identified, such as name, e-mail address,
  telephone number ("personal information").
* Information that is about you but individually does not identify you.
* Information about your internet connection, the equipment you use to access our Website, and
  usage details.
* Information directly provided by you.

3. **How We Use Your Information**. We use information that we collect about you or that you
provide to us, including any personal information:

* To present our Website and its contents to you.
* To provide you with information, products, or services that you request from us.
* To fulfill any other purpose for which you provide it.
* To carry out our obligations and enforce our rights arising from any contracts entered into
  between you and us, including for billing and collection.
* To notify you about changes to our Website or any products or services we offer or
  provide though it.
* In any other way we may describe when you provide the information.

4. **Changes to Our Privacy Policy**. It is our policy to post any changes we make to our
privacy policy on this page. If we make material changes to how we treat our users' personal
information, we will notify you through a notice on the Website home page. The date the
privacy policy was last revised is identified at the top of the page. You are responsible for
ensuring we have an up-to-date active and deliverable email address for you, and for periodically
visiting our Website and this privacy policy to check for any changes.

5. **Contact Information**. To ask questions or comment about this privacy policy and our
privacy practices, contact us at: southbaysupercontest@gmail.com.
