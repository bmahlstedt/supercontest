Cookie Policy
=============

*Last Updated: 2020-01-01*

1. **Introduction**. South Bay Supercontest ("we", "us", "our") uses cookies on our website
(our "Website"). By using our Website, you consent to the use of cookies. Our Cookie Policy
explains what cookies are, how we use cookies, how third-parties we may partner with may use
cookies on the Website, and your choices regarding cookies.

2. **What Are Cookies**. Cookies are small pieces of text managed by your web browser. A cookie is
stored in your web browser and allows the Website or a third-party to recognize you and make your
next visit more smooth/useful to you.

3. **How South Bay Supercontest Uses Cookies**. When you use and access the Website, we may
place a number of cookies files in your web browser. We use cookies to enable certain functions
of the Website, to provide analytics, and to store your preferences. We may use both session and
persistent cookies on the Website. We may use essential cookies to authenticate users and
prevent fraudulent use of user accounts.

4. **Third Party Cookies**. In addition to our own cookies, we may also use various third-parties
cookies to report usage statistics of the Website.

5. **What Are Your Choices Regarding Cookies**. If you'd like to delete cookies or instruct your
web browser to delete or refuse cookies, please visit the help pages of your web browser.
Please note, however, that if you delete cookies or refuse to accept them, you might not be able
to use all of the features of the Website. You may not be able to store your preferences,
and some of our pages might not display properly.

6. **Where Can You Find More Information About Cookies**. You can learn more about cookies at the
following third-party websites:

* AllAboutCookies: http://www.allaboutcookies.org/
* Network Advertising Initiative: http://www.networkadvertising.org/
