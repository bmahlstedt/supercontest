project = 'South Bay Supercontest'
copyright = 'South Bay Supercontest'
author = 'southbaysupercontest@gmail.com'
extensions = ['sphinx.ext.autodoc', 'sphinx.ext.autosummary', 'sphinx_rtd_theme']
html_theme = 'sphinx_rtd_theme'
templates_path = ['_templates']
