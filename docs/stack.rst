Stack
=====

.. list-table::
   :stub-columns: 1

   * - Language
     - Python
     - .. image:: https://southbaysupercontest.com/assets/stack/python.svg
          :width: 40
          :height: 40
   * - Web Framework
     - Flask
     - .. image:: https://southbaysupercontest.com/assets/stack/flask.svg
          :width: 40
          :height: 40
   * - Server
     - Gunicorn
     - .. image:: https://southbaysupercontest.com/assets/stack/gunicorn.svg
          :width: 40
          :height: 40
   * - Proxy
     - Nginx
     - .. image:: https://southbaysupercontest.com/assets/stack/nginx.svg
          :width: 40
          :height: 40
   * - Load Balancer
     - AWS ELB
     - .. image:: https://southbaysupercontest.com/assets/stack/elb.svg
          :width: 40
          :height: 40
   * - Certs
     - AWS ACM
     - .. image:: https://southbaysupercontest.com/assets/stack/acm.svg
          :width: 40
          :height: 40
   * - Compute
     - AWS EC2
     - .. image:: https://southbaysupercontest.com/assets/stack/ec2.svg
          :width: 40
          :height: 40
   * - Serverless
     - AWS Lambda
     - .. image:: https://southbaysupercontest.com/assets/stack/lambda.svg
          :width: 40
          :height: 40
   * - Static
     - AWS S3
     - .. image:: https://southbaysupercontest.com/assets/stack/s3.svg
          :width: 40
          :height: 40
   * - CDN
     - AWS CloudFront
     - .. image:: https://southbaysupercontest.com/assets/stack/cloudfront.svg
          :width: 40
          :height: 40
   * - Observability
     - AWS CloudWatch
     - .. image:: https://southbaysupercontest.com/assets/stack/cloudwatch.svg
          :width: 40
          :height: 40
   * - DNS
     - AWS Route53
     - .. image:: https://southbaysupercontest.com/assets/stack/route53.svg
          :width: 40
          :height: 40
   * - Firewall
     - AWS WAF
     - .. image:: https://southbaysupercontest.com/assets/stack/waf.svg
          :width: 40
          :height: 40
   * - Notifications
     - AWS SNS
     - .. image:: https://southbaysupercontest.com/assets/stack/sns.svg
          :width: 40
          :height: 40
   * - CIAM
     - AWS Cognito
     - .. image:: https://southbaysupercontest.com/assets/stack/cognito.svg
          :width: 40
          :height: 40
   * - Scheduler
     - AWS EventBridge
     - .. image:: https://southbaysupercontest.com/assets/stack/eventbridge.svg
          :width: 40
          :height: 40
   * - DB Manager
     - AWS RDS
     - .. image:: https://southbaysupercontest.com/assets/stack/rds.svg
          :width: 40
          :height: 40
   * - DB Engine
     - PostgresSQL
     - .. image:: https://southbaysupercontest.com/assets/stack/postgres.svg
          :width: 40
          :height: 40
   * - Cache Manager
     - AWS ElastiCache
     - .. image:: https://southbaysupercontest.com/assets/stack/elasticache.svg
          :width: 40
          :height: 40
   * - Cache Engine
     - Redis
     - .. image:: https://southbaysupercontest.com/assets/stack/redis.svg
          :width: 40
          :height: 40
   * - ORM
     - SQLAlchemy
     - .. image:: https://southbaysupercontest.com/assets/stack/sqla.svg
          :width: 40
          :height: 40
   * - API
     - REST
     - .. image:: https://southbaysupercontest.com/assets/stack/rest.svg
          :width: 40
          :height: 40
   * - Graph
     - GraphQL
     - .. image:: https://southbaysupercontest.com/assets/stack/graphql.svg
          :width: 40
          :height: 40
   * - Virtualization
     - Docker
     - .. image:: https://southbaysupercontest.com/assets/stack/docker.svg
          :width: 40
          :height: 40
   * - Traffic
     - Google Analytics
     - .. image:: https://southbaysupercontest.com/assets/stack/googleanalytics.svg
          :width: 40
          :height: 40
   * - SCM
     - GitLab
     - .. image:: https://southbaysupercontest.com/assets/stack/gitlab.svg
          :width: 40
          :height: 40
   * - Doc Gen
     - Sphinx
     - .. image:: https://southbaysupercontest.com/assets/stack/sphinx.svg
          :width: 40
          :height: 40
   * - Doc Host
     - Read the Docs
     - .. image:: https://southbaysupercontest.com/assets/stack/readthedocs.svg
          :width: 40
          :height: 40
   * - Chat
     - Discord
     - .. image:: https://southbaysupercontest.com/assets/stack/discord.svg
          :width: 40
          :height: 40
   * - Payments
     - Stripe
     - .. image:: https://southbaysupercontest.com/assets/stack/stripe.svg
          :width: 40
          :height: 40
   * - Scraping
     - Selenium
     - .. image:: https://southbaysupercontest.com/assets/stack/selenium.svg
          :width: 40
          :height: 40
   * - Templating
     - Jinja
     - .. image:: https://southbaysupercontest.com/assets/stack/jinja.svg
          :width: 40
          :height: 40
   * - Styles
     - Bootstrap
     - .. image:: https://southbaysupercontest.com/assets/stack/bootstrap.svg
          :width: 40
          :height: 40
   * - Visualization
     - Plotly
     - .. image:: https://southbaysupercontest.com/assets/stack/plotly.svg
          :width: 40
          :height: 40

Framework Extensions
--------------------

::

   flask-admin
   flask-assets
   flask-caching
   flask-graphql
   flask-htmlmin
   flask-login
   flask-mail
   flask-marshmallow
   flask-migrate
   flask-sqlalchemy
   flask-user
   flask-wtf

Test Suite
----------

::

   bandit
   black
   coverage
   pylint
   pyright
   pytest
