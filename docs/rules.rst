Rules
=====

Scoring
-------

- For each matchup, a line will be set at the number of points the favored team is expected to
  beat the underdog by.
- Example: The line is 6, 49ers over Patriots. If you pick the 49ers, and they win by a
  touchdown (7), or more, you got the pick correct. If the 49ers win by 5, or anything less than
  6 (including a Patriots win), you got the pick incorrect. If the 49ers win by 6 exactly, your
  pick is a push.
- A correct pick is ``1.0`` point. An incorrect pick is ``0.0`` points. A push is ``0.5`` points.
- Example: The line is 0, Dolphins vs Jets. If you pick the Jets, and they win, you got the pick
  correct. If they tie, you get ``0.5`` points no matter which team you chose, because in both
  cases, that team pushed the line.
- You get to pick 5 teams every week of the regular season.
- The lines are set deliberately to split bets, averaging close to the same 50/50 chance as a
  coin toss. Historically, if you average 3 picks correct out of 5 weekly, you are going to end in
  the top standings. 60% is great for Vegas.

Logistics
---------

- All times on the site (and in these docs) are in the pacific timezone, unless otherwise specified.
- Lines for each upcoming week will be posted on Wednesday evening at 6pm.
- You have until Saturday night at midnight to place your picks.
- Everyone's picks will be emailed out at 1am after lockdown Saturday night.
- The app gracefully handles daylight savings time. Before the shift, picks lock at midnight.
  After the shift, picks lock at midnight. Every timed schedule is consistent for both PDT and
  PST (and all other timezones, just shifted).
- Scores are fetched every minute during game windows (from 30min before kickoff until the status
  is reported as finished).
- Scores tick live on the matchups and picks views. No need to refresh.

Payment
-------

- You may play in the free (default) league at will. Pick as many or as few weeks as you'd like.
  Take breaks. Enjoy your time.
- The contest has an entry donation of $50. This must be received by week 3 to enter.
- Payments are either made through Venmo (Petty) or Stripe (follow the ``Upgrade`` link on your
  menu). If you've already paid, this will change to show ``Premium``. In the offseason, it will
  display ``Offseason`` and the tooltip will indicate your status from the most recent season.
- If you are in the paid league, all of your views will default to only show the paid league
  (leaderboard, all-picks view, etc). If you are not in the paid league for that season, it will
  show everyone (the free league).
- $2 of each $50 goes to the end-of-season party in Hermosa Beach. Fly to LA for your
  hard-earned beer.
- The other $48 of each $50 goes to the prize pool. We are not pocketing any of the dues. The
  service is free.
- Payout is displayed on the leaderboard, based on the current number of paid players for that
  season.
- It's a discrete logarithmic distribution (p=0.9), with the floor payout being determined by
  the buyin.
- Round-downs for ranks 2-N go to the 1st place prize, and the 1st place round-down goes to the
  party fund.
- If multiple people are tied for the same total points, their purses are summed and distributed
  evenly among them.
- **A $10 weekly prize will be awarded to anyone who gets all 5 picks correctly**. If no one goes
  5-0 then the prize will carry over to the next week, accumulating another $10. If multiple people
  get 5-0, then the current prize total will be split evenly among them. If no one goes 5-0 on the
  final week of the regular season, then the current prize total gets added to the party fund.

Banner
------

- Every week, any user has the option to buy the jumbotron for $5 and fill it with whatever
  content he/she desires. First come first serve.
- You may use this to talk trash, rep your team, advertise, gloat, or anything else. It's your space.
- The banner will be visible for 7 days, rolling over on Wednesday evening when lines update.
- This can be a static image, text, gif, combination, or any other reasonable media container.
- ``current_user`` is available to this element, which means that you can put customized messages
  for whoever the logged-in user is, and it can be different for each user.
- You can put visible text to describe your intent, in addition to ``img alt`` or ``tooltip``
  descriptors. It's up to you.
- The content can obviously be centered, stretched, repeated horizontally, or whatever
  configuration you choose.
- Size is flexible, but you're going to get pushback if you request anything with a height more
  than a couple hundred pixels.
- See the ``Contact`` page for booking.
