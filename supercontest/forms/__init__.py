"""A collection of forms that allow user input on the client."""
from wtforms import BooleanField
from flask_wtf import FlaskForm


class EmailPreferencesForm(FlaskForm):
    """Gives a user options to disable/enable email notifications."""

    email_when_picks_open = BooleanField(
        ("Email me when the weekly lines are posted and picks open")
    )
    email_when_picks_closing = BooleanField(
        (
            "Email me a reminder a few hours before lockdown if I did not "
            + "submit the full 5 picks"
        )
    )
    email_all_picks = BooleanField(
        ("Email me the image attachment of all user picks weekly after " + "lockdown")
    )
