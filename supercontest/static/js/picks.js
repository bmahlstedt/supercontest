/**
 * Fetches new calculated results (rank, grade, picks, etc) to tick w/o reload.
 */
function updateGamesAndResults() {
  $.ajax({
    type: 'GET',
    url: '/picks/results/season' + season + '/league' + league + '/week' + week,
    success: function(data) {
      [sortedGames, finalWeekResults] = data;
      renderLineScoreRows();
      renderUserRows();
      styleUserRows();
      colorRanks();
      addRankSuffixes();
      highlightUserRow();
    },
    error: function(request, status, message) {
      // error for serverside. the same problem is "warn" clientside.
      $.notify(request.responseText, 'error');
    },
  });
}

/**
 * Iterates through the returned list of Game objects (dumped by our schema)
 * to render the rows at the top of the allpicks table.
 */
function renderLineScoreRows() {
  const linesRow = document.getElementById('linesRow');
  linesRow.innerHTML = '';
  let cell = linesRow.insertCell();
  cell.colSpan = 8;
  cell.textContent = 'Lines';
  const scoresRow = document.getElementById('scoresRow');
  scoresRow.innerHTML = '';
  cell = scoresRow.insertCell();
  cell.colSpan = 8;
  cell.textContent = 'Score';
  sortedGames.forEach((game) => {
    cell = linesRow.insertCell();
    cell.textContent = '-' + game.line;
    cell.classList.add('border-left-thick');
    cell = linesRow.insertCell();
    cell.textContent = '+' + game.line;
    cell.classList.add('border-right-thick');
    const favorite = getFavorite(game.contestants);
    const underdog = getUnderdog(game.contestants);
    cell = scoresRow.insertCell();
    cell.textContent = favorite.score;
    cell.classList.add('border-left-thick');
    cell = scoresRow.insertCell();
    cell.textContent = underdog.score;
    cell.classList.add('border-right-thick');
  });
}

/**
 * Iterates through the calculated week results (ranks, grade, etc) and
 * populates the main table on the page. All user rows. Then styles them.
 */
function renderUserRows() {
  const userRows = document.getElementById('userRows');
  userRows.innerHTML = '';
  finalWeekResults.forEach((result) => {
    const userRow = userRows.insertRow();
    userRow.classList.add('userRow');
    userRow.id = result.user_id;

    let cell = userRow.insertCell();
    cell.classList.add('rankCell');
    cell.innerHTML = result.rank + '<span class="rankSuffix text-body-secondary tiny"></span>';

    for (let i = 0; i < 5; i++) {
      cell = userRow.insertCell();
      cell.classList.add('rollupCell');
      cell.innerHTML = '<div class="logo"></div>';
    }

    cell = userRow.insertCell();
    cell.classList.add('w-max-250');
    cell.textContent = idNameMap[result.user_id];
    cell.setAttribute('title', idEmailMap[result.user_id]);

    cell = userRow.insertCell();
    cell.classList.add('percentage');
    cell.innerHTML = result.grade + '<span class="text-body-secondary tiny">%</span>';

    sortedGames.forEach((game) => {
      const favorite = getFavorite(game.contestants);
      const underdog = getUnderdog(game.contestants);
      cell = userRow.insertCell();
      cell.classList.add('favoredTeamCell');
      cell.classList.add('text-body-secondary');
      cell.classList.add('border-left-thick');
      cell.textContent = favorite.team.abbv;
      cell = userRow.insertCell();
      cell.classList.add('underdogTeamCell');
      cell.classList.add('text-body-secondary');
      cell.classList.add('border-left-thick');
      cell.textContent = underdog.team.abbv;
    });
  });
}

/**
 * Iterate through each sorted pick row, coloring cells, adding team
 * logos, and swapping team names for abbreviations.
 */
function styleUserRows() {
  $('tr.userRow').each(function(rank) {
    // The template renders the userRows in the same order as the sorted finalWeekResults.
    const pickedTeams = finalWeekResults[rank]['sorted_pick_teams'];
    // Iterate over each picked team in the left rollup. Python already sorted them.
    $(this).find('td.rollupCell').each(function(pickNum) {
      // If they have fewer than 5 teams, iterate until break.
      if (typeof pickedTeams[pickNum] === 'undefined') {
        return false;
      }
      const pickedTeam = pickedTeams[pickNum];
      const game = sortedGames.find(game => (game.contestants[0].team.name == pickedTeam) || (game.contestants[1].team.name == pickedTeam));
      const contestant = game.contestants.find(contestant => contestant.team.name == pickedTeam);
      colorPickCell($(this).get(0), contestant.coverage.name, 'Cover', 'Push', game.status, null, true);
      $(this).find('div.logo').css('background-image', getLogoUrl(pickedTeam));
    });
    // Iterate over each team cell in that row. The main part of the table.
    $(this).find('td.favoredTeamCell').each(function(index) {
      const favoredTeamAbbv = $(this).text();
      const game = sortedGames[index]; // same order
      const favoredContestant = game.contestants.find(contestant => contestant.team.abbv == favoredTeamAbbv);
      const favoredTeamName = favoredContestant.team.name;
      colorPickCell($(this).get(0), favoredContestant.coverage.name, 'Cover', 'Push', game.status, null, pickedTeams.includes(favoredTeamName));
    });
    $(this).find('td.underdogTeamCell').each(function(index) {
      const underdogTeamAbbv = $(this).text();
      const game = sortedGames[index]; // same order
      const underdogContestant = game.contestants.find(contestant => contestant.team.abbv == underdogTeamAbbv);
      const underdogTeamName = underdogContestant.team.name;
      colorPickCell($(this).get(0), underdogContestant.coverage.name, 'Cover', 'Push', game.status, null, pickedTeams.includes(underdogTeamName));
    });
  });
}

if (window.location.href.indexOf('picks') != -1) {
  checkInLeague();
  if (picksOpen == false) {
    renderLineScoreRows();
    renderUserRows();
    styleUserRows();
    colorRanks();
    addRankSuffixes();
    highlightUserRow();
    setInterval(updateGamesAndResults, 60 * 1000);
  }
}
