/**
 * Graph the dumped json string passed from plotly serverside.
 */
function renderGraph() {
  Plotly.plot('plotGraph', dumpedGraph);
}

if (window.location.href.indexOf('progression') != -1) {
  checkInLeague();
  renderGraph();
}
