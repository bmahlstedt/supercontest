"""Simple commit wrappers for writing data to the database."""
import logging
from supercontest.models import db, Game, Contestant, Pick, Role
from supercontest.util.types import LinesFetch, ScoresFetch
from supercontest.util.timing import convert_westgate_datetime_to_utc
from supercontest.dbsession import queries

logger = logging.getLogger(__name__)


def create_role(name: str) -> Role:
    """Creates a new role. Easy, exemplary.

    :param name: Name of new role (typically first-letter-capitalized).
    :return: Newly created role object.
    """
    role = Role(name=name)
    db.session.add(role)
    db.session.commit()
    return role


def add_user_to_role(user_email: str, role_name: str) -> None:
    """Adds a known user to a known role.

    :param user_email: The user to add.
    :param role_name: The name of the role to provide.
    """
    user = queries.get_user_from_email(email=user_email)
    role = queries.get_role_from_name(name=role_name)
    user.roles.append(role)
    db.session.commit()


def add_user_to_current_paid_league(user_email: str) -> None:
    """Gets the paid league for the current season, then adds the user to it.

    :param user_email: User email.
    """
    current_season = queries.get_current_season()
    paid_league = queries.get_paid_league_for_season(season=current_season)
    user = queries.get_user_from_email(email=user_email)
    paid_league.users.append(user)
    db.session.commit()


def add_users_to_current_paid_league(user_emails: list[str]) -> None:
    """Gets the paid league for the current season, then adds the users to it.

    :param user_emails: User emails.
    """
    current_season = queries.get_current_season()
    paid_league = queries.get_paid_league_for_season(season=current_season)
    users = [queries.get_user_from_email(email=user_email) for user_email in user_emails]
    for user in users:
        paid_league.users.append(user)
    db.session.commit()


def change_game_status(game_id: int, status_name: str) -> None:
    """Changes the status of a game.

    :param game_id: The ID of the matchup that you're changing the status for.
    :param status_name: The name of the status to change to (via FK to statuses table).
    """
    game = db.session.get(Game, game_id)
    status_id = queries.get_status_id_from_name(name=status_name)
    game.status_id = status_id
    db.session.commit()


def delete_rows(rows: list[Game | Pick]) -> None:
    """Wrapper around deleting rows. You may delete one or multiple.

    :param rows: The rows to delete.
    """
    for row in rows:
        db.session.delete(row)
    db.session.commit()


def insert_contestants(week_id: int, linesfetch: list[LinesFetch]) -> None:
    """Writes the lines and initial opponent info to the database. All cleaning is done before this.

    :param week_id: The ID of the week to write lines for.
    :param linesfetch: The return from :func:`~supercontest.core.lines.fetch_lines`.
    """
    # Initialize constants and other information about the matchup.
    score = 0  # start 0
    opponent_score = 0  # needed for the denormalized margin/coverage observer calc
    status_id = queries.get_map_espn_abbv_status_id()["P"]  # always pregame at insert time
    for linefetch in linesfetch:
        # Extract info from linefetch about teams.
        line = float(linefetch["line"])
        datetime = convert_westgate_datetime_to_utc(linefetch["datetime"])
        team1_name = linefetch["favored_team"]
        team2_name = linefetch["underdog_team"]
        home_name = linefetch["home_team"]
        # Instantiate the Game object.
        game = Game(
            datetime=datetime,
            line=line,
            week_id=week_id,
            status_id=status_id,
        )
        # Add and commit the game, then refresh so we have the newly inserted game's ID.
        db.session.add(game)
        db.session.commit()
        db.session.refresh(game)
        # Instantiate both Contestant objects.
        contestants: list[Contestant] = []
        for team_name in [team1_name, team2_name]:
            contestants.append(
                Contestant(
                    score=score,
                    opponent_score=opponent_score,
                    game_id=game.id,
                    team_id=queries.get_team_id(team_name),
                    prediction_id=queries.get_prediction_id(
                        team_name, team1_name, team2_name, line
                    ),
                    location_id=queries.get_location_id(
                        team_name, team1_name, team2_name, home_name
                    ),
                    game=game,  # relationship needed for observer calc (game.line is the dep)
                )
            )
            db.session.refresh(game)  # need new transient ref to relationship
        db.session.add_all(contestants)
        db.session.commit()


def write_scores(games: list[Game], scores: list[ScoresFetch]) -> None:
    """Commits the scores to the database (insert and/or update). Games and scores must be in the
    same corresponding order.

    :param games: The return from :func:`~supercontest.core.scores.fetch_scores`.
    :param scores: The return from :func:`~supercontest.core.scores.fetch_scores`.
    :raises ValueError: If one of home/visitor doesn't match favorite/underdog.
    """
    espn_abbv_status_id_map = queries.get_map_espn_abbv_status_id()
    for game, score in zip(games, scores):
        # The ESPN score API is finicky. Once a game has been determined over ONCE, leave it that
        # way (otherwise, it will hover for HOURS sometimes before staying on F).
        if not game.status.finished:
            game.status_id = espn_abbv_status_id_map[score["status"]]
            for contestant in game.contestants:
                contestant.score = score["team_scores"][contestant.team.name]
                contestant.get_opponent().opponent_score = score["team_scores"][
                    contestant.team.name
                ]
    db.session.commit()


def write_picks(
    old_pick_objs_update: list[Pick],
    new_pick_contestant_objs: list[Contestant],
    user_id: int,
) -> None:
    """Deletes, updates, and inserts pick objects to the database as necessary. The caller already
    checked which existing picks to keep. So we have a remainder to update.

    * If there's the same number of existing picks and new picks, it will update them.
    * If there are more existing picks than new picks, it will update for the new and delete the
      remaining rows from existing.
    * If there are more new picks than existing picks, it will update the existing and then insert
      the remainder for the new.

    All validity of the picks must be analyzed prior to this function.

    :param old_pick_objs_update: Existing pick objects to update.
    :param new_pick_contestant_objs: Contestant objects for the new picks.
    :param user_id: The ID of the user submitting these picks.
    """
    old_pick_objs_delete: list[Pick] = []
    for old_pick_obj in old_pick_objs_update:
        # If we have new picks to add, pull that info over for update. Else delete the old row.
        if new_pick_contestant_objs:
            # Pop will deterministically remove from the end, so order association is assured.
            new_pick_contestant_obj = new_pick_contestant_objs.pop()
            old_pick_obj.contestant_id = new_pick_contestant_obj.id
        else:
            old_pick_objs_delete.append(old_pick_obj)
    # If we still have new picks to add after all existing picks have been updated, then insert.
    new_pick_objs_insert = [
        Pick(user_id=user_id, contestant_id=new_pick_contestant_obj.id)
        for new_pick_contestant_obj in new_pick_contestant_objs
    ]
    delete_rows(old_pick_objs_delete)
    db.session.add_all(new_pick_objs_insert)
    db.session.commit()
