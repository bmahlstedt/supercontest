"""Common wrappers around select statements. This is the only module that calls
``selects``, the module that makes the actual queries.

For the most part, these wrappers extract the relevant information from the db rows.
In some cases (rarer), the wrappers simply pass through the sqla objects.

This module contains the lazy-loading functionality of the cache, for the function
level (memoization). View caching is handled in the views.
A read will try the cache first. If hit, it will return. If miss, it will query the db,
then update the cache, then return.
Note that redis caches serialized values well - but NOT nested sqla objects. The parent
won't be bound to a session, so all relationships are unloadable. For this reason, caching
the functions in THIS module is preferred over caching the functions in the ``selects`` module.
"""
from datetime import datetime
from typing import overload, Literal, Callable
from supercontest.models import cache, Role, League, User, Game, Contestant, Pick
from supercontest.util.timing import get_utc_now, hours_since
from supercontest.dbsession import selects

#: How long to keep cache entries alive (in seconds), by default. This is mostly to optimize
#: multiple function calls WITHIN the same request session, so that I don't have to be careful
#: about reentrance in app logic (eg checking membership, recreating maps, ``get_current_week``,
#: stuff like that).
MEMOIZE_TTL = 30


def get_role_from_name(name: str) -> Role:
    """Returns the id for a Role row, given its name.

    :param name: Name of the Role to lookup.
    :return: The Role object.
    """
    return selects.get_role_from_name(name=name)


@cache.memoize(MEMOIZE_TTL)
def get_all_user_emails() -> list[str]:
    """Simply gives the full list of user emails (unique).

    :return: All user email addresses in the database.
    """
    users = selects.get_all_users()
    return [user.email for user in users]


def get_user_from_email(email: str) -> User:
    """Takes an email, returns a user object. This is case-insensitive. If the email address has
    been lowered, like the ``admin.late_picks`` form does, it will still find the right user.

    :param email: Email.
    :return: The user object.
    """
    return selects.get_user_from_email(email=email)


@cache.memoize(MEMOIZE_TTL)
def get_users_with_email_pref(pref: str) -> list[str]:
    """Gives the list of user email addresses for all users that have a given subscription
    preference.

    :param pref: The name of the preference, eg ``email_when_picks_open``.
    :return: The email addresses of the users which have that preference enabled.
    """
    users = selects.get_all_users()
    return [user.email for user in users if getattr(user, pref) is True]


@cache.memoize(MEMOIZE_TTL)
def get_admin_emails() -> list[str]:
    """Gives all the email addresses for users with the 'Admin' role.

    :return: Admin email addresses.
    """
    admin = selects.get_admin()
    return [user.email for user in admin]


@cache.memoize(MEMOIZE_TTL)
def get_id_name_map() -> dict[int, str]:
    """Grabs the display name for every row in the user table, returning the first+last name
    if populated. If not, returns the email.

    :return: Keys are user IDs and values are names (or emails if no name).
    """
    users = selects.get_all_users()
    id_name_map: dict[int, str] = {}
    for user in users:
        if user.first_name and user.last_name:
            name = " ".join([user.first_name, user.last_name])
        else:
            name = user.email
        id_name_map[user.id] = name
    return id_name_map


@cache.memoize(MEMOIZE_TTL)
def get_id_email_map() -> dict[int, str]:
    """Grabs the email for every row in the user table.

    :return: Keys are user IDs and values are emails.
    """
    users = selects.get_all_users()
    return {user.id: user.email for user in users}


@cache.memoize(MEMOIZE_TTL)
def get_week_id(season: int | None, week: int | None) -> int:
    """Returns the id for a week, given its number and the season year.

    :param season: Season year.
    :param week: Week number.
    :return: Week ID.
    """
    week_obj = selects.get_week(season=season, week=week)
    return week_obj.id


@cache.memoize(MEMOIZE_TTL)
def get_sorted_seasons() -> list[int]:
    """Numerically sorted season years, ascending.

    :return: All season years, from oldest to newest.
    """
    sorted_seasons = selects.get_sorted_seasons()
    return [season.season for season in sorted_seasons]


@cache.memoize(MEMOIZE_TTL)
def get_max_season() -> int:
    """Simple return of max season year.

    :return: The most recent season in the database.
    """
    sorted_seasons = selects.get_sorted_seasons()
    return sorted_seasons[-1].season


@cache.memoize(MEMOIZE_TTL)
def get_max_week(season: int) -> int:
    """Used for "how many weeks are in a season?" Basically "17 if season <= 2020 else 18" but
    hits the database to be sure.

    :param season: Season year.
    :return: The number of weeks in that season.
    """
    sorted_weeks = selects.get_sorted_weeks(season=season)
    return sorted_weeks[-1].week


@cache.memoize(MEMOIZE_TTL)
def get_current_season() -> int | None:
    """Simple return of current season year.

    :return: Current season year or None if not in an active season.
    """
    season = selects.get_season_by_time(dt_obj=get_utc_now())
    return season.season if season else None


@cache.memoize(MEMOIZE_TTL)
def get_current_week() -> int | None:
    """Simple return of current week number.

    :return: Current week number or None if not in an active week.
    """
    week_obj = selects.get_week_by_time(dt_obj=get_utc_now())
    return getattr(week_obj, "week", None)


@cache.memoize(MEMOIZE_TTL)
def get_current_week_start() -> datetime | None:
    """Simple return of the start date of the current week.

    :return: The start date of the current week or None if not in an active week.
    """
    week_obj = selects.get_week_by_time(dt_obj=get_utc_now())
    return getattr(week_obj, "week_start", None)


@cache.memoize(MEMOIZE_TTL)
def get_current_season_and_week() -> tuple[int | None, int | None]:
    """Simple return of the current season year and week number.

    :return: Current season year or None if not in an active season.
    :return: Current week number or None if not in an active week.
    """
    season = get_current_season()
    week = get_current_week()
    return season, week


@cache.memoize(MEMOIZE_TTL)
def is_regseason() -> bool:
    """If you're in an active regular season week.

    :return: ``False`` if offseason, preseason, or postseason. ``True`` if regseason.
    """
    return get_current_week() is not None


@cache.memoize(MEMOIZE_TTL)
def is_offseason() -> bool:
    """If you're in the offseason.

    :return: ``False`` if regseason, preseason, or postseason. ``True`` if offseason.
    """
    current_season, current_week = get_current_season_and_week()
    return current_season is None and current_week is None


@cache.memoize(MEMOIZE_TTL)
def is_preseason() -> bool:
    """If you're in the time after ``season_start`` but also before ``week_start`` for week 1.

    :return: ``False`` if regseason, offseason, or postseason. ``True`` if preseason.
    """
    if is_regseason():
        return False
    current_season = get_current_season()
    if current_season is None:
        return False
    week_1_start = selects.get_sorted_weeks(season=current_season)[0].week_start
    return week_1_start > get_utc_now()


@cache.memoize(MEMOIZE_TTL)
def is_postseason() -> bool:
    """If you're in the time after the last week_end (for week 18) but before ``season_end``.
    Just take the complement of the others.

    :return: ``False`` if regseason, offseason, or preseason. ``True`` if postseason.
    """
    return not any([is_regseason(), is_offseason(), is_preseason()])


@cache.memoize(MEMOIZE_TTL)
def are_picks_open() -> bool:
    """The primary determinant of whether or not picks can be submitted. The condition is that the
    current timestamp is within 79 hours of the current week's start (5pm Wed -> Midnight Saturday).
    This is used by views to configure many things, functional and visual. If you're not in
    the current week, this obviously returns False.

    :return: Whether or not picks are open for the current week.
    """
    lockdown_after_week_start = 3 * 24 + 7
    if not is_regseason():
        return False
    if (week_start := get_current_week_start()) is None:
        return False
    hours_since_week_start = hours_since(week_start)
    return hours_since_week_start <= lockdown_after_week_start


@overload
def get_users_in_league(league_id: int, return_emails: Literal[False] = False) -> list[int]:
    ...


@overload
def get_users_in_league(league_id: int, return_emails: Literal[True]) -> list[str]:
    ...


@overload
def get_users_in_league(league_id: int, return_emails: bool) -> list[int] | list[str]:
    ...


@cache.memoize(MEMOIZE_TTL)
def get_users_in_league(league_id: int, return_emails: bool = False) -> list[int] | list[str]:
    """Gets the users in a league. Handles the ``league_id=0`` case (the free league, all users).

    :param league_id: League ID.
    :param return_emails: Returns IDs by default. Pass ``True`` to return emails instead.
    :return: User IDs or emails, depending on the input.
    """
    if league_id == 0:
        users = selects.get_all_users()
    else:
        users = selects.get_users_in_league(league_id=league_id)
    if return_emails:
        user_data = [user.email for user in users]
    else:
        user_data = [user.id for user in users]
    return user_data


@cache.memoize(MEMOIZE_TTL)
def is_user_in_league(user_id: int, league_id: int) -> bool:
    """Checks if a user is in a league. If checking against the free league (``id=0``), then every
    user is considered active.

    :param user_id: User ID.
    :param league_id: League ID.
    :return: If the user is in the league.
    """
    if league_id == 0:
        return True
    user_ids = get_users_in_league(league_id=league_id)
    return user_id in user_ids


@cache.memoize(MEMOIZE_TTL)
def get_league_names(season: int) -> list[tuple[int, str]]:
    """Get leagues associated with a season.

    :param season: Season year.
    :return: Pairs of league ID and league name.
    """
    leagues = selects.get_leagues(season=season)
    data = [(league.id, league.name) for league in leagues]
    return data


@cache.memoize(MEMOIZE_TTL)
def is_league_in_season(league_id: int, season: int) -> bool:
    """Checks if a league is valid for a season. Free league (``id=0``) is considered to be in
    all seasons.

    :param league_id: League ID.
    :param season: Season year.
    :return: If the league is valid for that season.
    """
    if league_id == 0:
        return True
    valid_league_ids = [league.id for league in selects.get_leagues(season=season)]
    return league_id in valid_league_ids


@cache.memoize(MEMOIZE_TTL)
def get_paid_league_for_season(season: int | None) -> League:
    """Returns the paid league for a specific season. This is denoted by the league name being
    "Paid" - if there isn't one with that name for that season, this errors. If multiple exist
    (should never happen), this takes the first one. Raises an error (via ``one()``) if there are
    no paid leagues for that season.

    :param season: Season year.
    :return: The paid league object.
    """
    paid_league = selects.get_paid_league_for_season(season=season)
    return paid_league


@cache.memoize(MEMOIZE_TTL)
def get_paid_league_id_for_season(season: int | None) -> int:
    """Returns the ID of the official paid league for a specific season. This is denoted by the
    league name being "Paid" - if there isn't one with that name for that season, this errors. If
    multiple exist (should never happen), this takes the first one. Raises an error (via ``one()``)
    if there are no paid leagues for that season.

    :param season: Season year.
    :return: The paid league ID.
    """
    paid_league = selects.get_paid_league_for_season(season=season)
    return paid_league.id


@cache.memoize(MEMOIZE_TTL)
def is_paid_user(season: int | None, user_id: int) -> bool:
    """Checks if a given user has paid for a given season. If a user is requesting this info during
    an offseason, it defaults to the status of the most recent season (i.e. if you are a paid player
    during a season, it stays that way until the next season starts).

    :param season: Season year.
    :param user_id: User ID.
    :return: If the user is in the paid league for that season.
    """
    if season is None:
        season = get_max_season()
    paid_league_id = get_paid_league_id_for_season(season)
    return is_user_in_league(user_id=user_id, league_id=paid_league_id)


@overload
def get_paid_users_for_season(season: int, return_emails: Literal[False] = False) -> list[int]:
    ...


@overload
def get_paid_users_for_season(season: int, return_emails: Literal[True]) -> list[str]:
    ...


@overload
def get_paid_users_for_season(season: int, return_emails: bool) -> list[int] | list[str]:
    ...


@cache.memoize(MEMOIZE_TTL)
def get_paid_users_for_season(season: int, return_emails: bool = False) -> list[int] | list[str]:
    """Given a year, returns the IDs of the paid users in that season. Can obviously just ``len()``
    this function's return to determine league size, for calculations like total payout purse.
    Raises an error if there's no paid league for that season.

    :param season: Season year.
    :param return_emails: Returns IDs by default. Pass ``True`` to return emails instead.
    :return: User IDs or emails, depending on the input.
    """
    paid_league_id = get_paid_league_id_for_season(season=season)
    paid_users = get_users_in_league(league_id=paid_league_id, return_emails=return_emails)
    return paid_users


def print_paid_player_emails(season: int) -> None:
    """Just prints out an alphabetically sorted list of official players for a season, for easy
    copy-paste to email the league. Raises an error if there's no paid league for that season.

    :param season: Season year.
    """
    emails = get_paid_users_for_season(season, return_emails=True)
    # Intentionally writing this to stdout instead of a formatted logger.
    print("\n".join(sorted([str(email).lower() for email in emails])))


@cache.memoize(MEMOIZE_TTL)
def get_team_id(name: str) -> int:
    """Returns the id for a team, given its name. Compares with ``ilike``, so the argument can be
    capitalized, lowered, uppered, etc.

    :param name: The name of the team.
    :return: Team ID.
    """
    team = selects.get_team(name=name)
    return team.id


@cache.memoize(MEMOIZE_TTL)
def get_map_espn_city_team() -> dict[str, str]:
    """Provides a lookup for common team-city pairs. Note that it's the ESPN city (returned from
    score fetch), not the SBSC standard city.

    :return: Keys are team cities, values are team names.
    """
    teams = selects.get_teams()
    espn_city_team_map = {team.espn_city: team.name for team in teams}
    return espn_city_team_map


@cache.memoize(MEMOIZE_TTL)
def get_all_team_names() -> list[str]:
    """Provides a simple list of all team names.

    :return: Team names (first letter capitalized).
    """
    teams = selects.get_teams()
    team_names = [team.name for team in teams]
    return team_names


@cache.memoize(MEMOIZE_TTL)
def get_prediction_id(team: str, favored_team: str, underdog_team: str, line: float) -> int:
    """Provide both teams in a matchup, and the team to check, and the line (for push conditions).

    :param team: The team to get the prediction for.
    :param favored_team: The favored team in the matchup.
    :param underdog_team: The underdog team in the matchup.
    :param line: The value of the line in the matchup:
    :return: The ID of the prediction object.
    :raises ValueError: If this function doesn't match the expected prediction (internal error).
    """
    predictions = selects.get_predictions()
    if line == 0.0:
        prediction_id = next(pred.id for pred in predictions if pred.name == "Pickem")
    elif team == favored_team:
        prediction_id = next(pred.id for pred in predictions if pred.name == "Favorite")
    elif team == underdog_team:
        prediction_id = next(pred.id for pred in predictions if pred.name == "Underdog")
    else:
        raise ValueError("Predictions don't match expected values.")
    return prediction_id


@cache.memoize(MEMOIZE_TTL)
def get_location_id(team: str, favored_team: str, underdog_team: str, home_team: str | None) -> int:
    """Provide both teams in a matchup, and the team to check, and the home team.

    :param team: The team to get the prediction for.
    :param favored_team: The favored team in the matchup.
    :param underdog_team: The underdog team in the matchup.
    :param home_team: The specified home team.
    :return: The ID of the location object.
    :raises ValueError: If the provided team is not in the matchup.
    """
    if team not in [favored_team, underdog_team]:
        raise ValueError("Team is not in matchup.")
    if home_team not in [favored_team, underdog_team, None]:
        raise ValueError("Home team is not in matchup.")
    locations = selects.get_locations()
    if home_team is None:
        location_id = next(loc.id for loc in locations if loc.name == "Neutral")
    elif team == home_team:
        location_id = next(loc.id for loc in locations if loc.name == "Home")
    else:
        location_id = next(loc.id for loc in locations if loc.name == "Visitor")
    return location_id


@cache.memoize(MEMOIZE_TTL)
def get_status_names() -> list[str]:
    """Provides a simple list of all status names.

    :return: Status names (first letter capitalized).
    """
    statuses = selects.get_statuses()
    status_names = [status.name for status in statuses]
    return status_names


@cache.memoize(MEMOIZE_TTL)
def get_status_id_from_name(name: str) -> int:
    """For a given name, get the SBSC status ID.

    :param name: The name of the status.
    :return: The ID of that status row.
    """
    statuses = selects.get_statuses()
    status_id = [status.id for status in statuses if status.name == name][0]
    return status_id


@cache.memoize(MEMOIZE_TTL)
def get_map_espn_abbv_status_id() -> dict[str, int]:
    """Provides a lookup for common scorefetch/dbstatus refs.

    :return: ESPN games statuses -> SBSC status IDs.
    """
    statuses = selects.get_statuses()
    espn_abbv_status_id_map = {status.espn_abbv: status.id for status in statuses}
    return espn_abbv_status_id_map


def get_games() -> list[Game]:
    """Returns all games in the database.

    :return: All games in the database.
    """
    games = selects.get_games()
    return games


def get_finished_games_in_season(season: int) -> list[Game]:
    """Returns all games that have finished in a given season.

    :param season: Season year.
    :return: All games that have finished.
    """
    games = selects.get_games_in_season(season=season)
    finished_games = [game for game in games if game.status.finished is True]
    return finished_games


def get_games_in_week(season: int, week: int) -> list[Game]:
    """Returns all games in a given week.

    :param season: Season year.
    :param week: Week number.
    :return: All games in that week.
    """
    games = selects.get_games_in_week(season=season, week=week)
    return games


def get_ordered_games_in_week(season: int, week: int) -> list[Game]:
    """Returns all games in a given week. Sort is by datetime chronologically ascending, then
    ``favored_team`` alphabetically.

    :param season: Season year.
    :param week: Week number.
    :return: All games in that week, ordered by datetime.
    """
    games = selects.get_games_in_week(season=season, week=week)
    sort_func: Callable[[Game], tuple[datetime, str]] = lambda game: (
        game.datetime,
        game.get_contestants(by="prediction", fail=False)[0].team.name,
    )
    games.sort(key=sort_func)
    return games


@cache.memoize(MEMOIZE_TTL)
def are_games_in_db(season: int, week: int) -> bool:
    """Checks if there are any rows in the Game table for a given season and week. Usually called
    to determine if you're in limbo after ``week_start`` (5pm PT) but before Westgate has posted.

    :param season: Season year.
    :param week: Week number.
    :return: If the games for that week are already in the database.
    """
    games = selects.get_games_in_week(season=season, week=week)
    return bool(games)


@cache.memoize(MEMOIZE_TTL)
def get_distinct_lines() -> list[float]:
    """Fetches lines from the DB. Adds negative lines.

    :return: An unordered list of all distinct lines.
    """
    games = selects.get_games()
    distinct_lines = list({game.line for game in games})
    return distinct_lines + [-line for line in distinct_lines]


@cache.memoize(MEMOIZE_TTL)
def get_distinct_margins() -> list[float]:
    """Fetches performance margins from the DB.

    :return: An unordered list of all distinct margins.
    """
    contestants = selects.get_contestants()
    distinct_margins = list({contestant.margin for contestant in contestants})
    return distinct_margins


def get_contestant_from_team(season: int, week: int, team: str) -> Contestant:
    """For a given week, simply provide the team name and this will give the contestant obj.

    :param season: Season year.
    :param week: Week number.
    :param team: Team name.
    :return: The Contestant row corresponding to the inputs.
    """
    contestants = selects.get_contestants_in_week(season=season, week=week)
    team = team.replace("*", "").lower()
    return [contestant for contestant in contestants if contestant.team.name.lower() == team][0]


def get_picks_in_week(season: int, week: int) -> list[Pick]:
    """Returns all picks in a given week.

    :param season: Season year.
    :param week: Week number.
    :return: All picks in that week.
    """
    picks = selects.get_picks_in_week(season=season, week=week)
    return picks


def get_picks_in_week_for_user(season: int, week: int, user_id: int) -> list[Pick]:
    """Returns all picks for a given user in a given week.

    :param season: Season year.
    :param week: Week number.
    :param user_id: User ID.
    :return: All picks for that user in that week.
    """
    picks = selects.get_picks_in_week(season=season, week=week)
    return [pick for pick in picks if pick.user_id == user_id]


def get_picks_in_week_for_league(season: int, week: int, league_id: int) -> list[Pick]:
    """Returns all picks for a given league in a given week.

    :param season: Season year.
    :param week: Week number.
    :param league_id: League ID.
    :return: All picks for that league in that week.
    """
    picks = selects.get_picks_in_week(season=season, week=week)
    user_ids_in_league = get_users_in_league(league_id=league_id)
    return [pick for pick in picks if pick.user.id in user_ids_in_league]


def get_picks_in_season_for_league(season: int, league_id: int) -> list[Pick]:
    """Returns all picks for a given league in a given season.

    :param season: Season year.
    :param league_id: League ID.
    :return: All picks for that league in that season.
    """
    picks = selects.get_picks_in_season(season=season)
    user_ids_in_league = get_users_in_league(league_id=league_id)
    return [pick for pick in picks if pick.user.id in user_ids_in_league]


def get_finished_picks() -> list[Pick]:
    """Returns all picks that have finished.

    :return: All picks that have finished.
    """
    picks = selects.get_picks()
    return [pick for pick in picks if pick.contestant.game.status.finished is True]


def get_users_in_season(season: int, league_id: int = 0) -> set[User]:
    """Uses picks to determine who is in a season. Can filter down into any league as well.
    Most commonly used for the main paid league in each season.

    :param season: Season year.
    :param league_id: League ID. If None, returns all users. Effectively the Free league, since it
        does not filter the results on league.
    :return: User objects.
    """
    picks = get_picks_in_season_for_league(season=season, league_id=league_id)
    return {pick.user for pick in picks}
