"""Calculations for the portions of views with results. Ordered by increasing complexity.

General scopes for the views:

* :func:`view_matchups`: single season, single week, single user
* :func:`view_allpicks`: single season, single week, all users
* :func:`view_progression`: single season, all weeks, all users
* :func:`view_leaderboard`: single season, all weeks, all users
* :func:`view_alltime_leaderboard`: all seasons, all weeks, users

And therefore we divide utilities into buckets:

* ``_week_results``: provides data to the matchups view and the allpicks view
* ``_season_results``: provides data to the graph view and the leaderboard view
* ``_alltime_results``: provides data to the alltime leaderboard view

General flows for each view:

#. ``query_``: get the raw data, usually picks / matchups, scoped by the view
#. ``map_``: iterate over raw picks and key pick counts / points by season / week / user
#. ``calculate_``: calculate / restructure as necessary to yield lists of user-result dicts
#. ``sort_``: order those elements by the approprate keys
#. ``inject_``: some information can only be calculated post-sort (eg ranks, payouts) - add those
#. ``toprow_``: a final list of aggregations across a col (rather than row), for leaguewide results

"""
from collections import defaultdict, Counter
from supercontest.models import Game, Pick
from supercontest.dbsession import queries
from supercontest.util import types as sct
from supercontest.util.results import (
    calc_ranks_from_points,
    calc_avg_user_pick_margins,
    calc_weekly_bonus_winners,
    determine_payout,
    adjust_purses_for_tied_ranks,
)
from supercontest.util.math import get_percentage, convert_num_to_int_if_whole


def map_picks(
    picks: list[Pick],
) -> tuple[
    sct.PickCountsByUserByWeekBySeason,
    sct.PointRollupsByUserByWeekBySeason,
    sct.RawPicksByUserByWeekBySeason,
]:
    """Takes a raw list of pick rows and iterates over them to create the maps of pick counts and
    point rollups, all by season/week/user. Is scoped by the picks that you pass it; not some
    manual logic of seasons/weeks/users. Therefore this function works for all views.

    :param picks: The return from pick obj fetch. Pick counts and point rollups.
    :return: Pick counts.
    :return: Point rollups.
    :return: Raw pick objects.
    """
    # Iterate over the picks and aggregate/extract the necessary results info.
    pick_counts: sct.PickCountsByUserByWeekBySeason = defaultdict(
        lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(int)))
    )
    point_rollups: sct.PointRollupsByUserByWeekBySeason = defaultdict(
        lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(float)))
    )
    raw_picks: sct.RawPicksByUserByWeekBySeason = defaultdict(
        lambda: defaultdict(lambda: defaultdict(list))
    )
    for pick in picks:
        # Convenience references.
        season = pick.contestant.game.week.season.season
        week = pick.contestant.game.week.week
        user = pick.user_id
        # Then count picks by status and add points respectively.
        if pick.contestant.game.status.unstarted:
            pick_counts[season][week][user]["unstarted"] += 1
            # no points for unstarted games
        elif pick.contestant.game.status.finished:
            pick_counts[season][week][user][pick.contestant.coverage.name.lower()] += 1
            point_rollups[season][week][user]["finished"] += pick.contestant.coverage.points
        else:
            pick_counts[season][week][user][pick.contestant.coverage.name.lower() + "ing"] += 1
            point_rollups[season][week][user]["inprogress"] += pick.contestant.coverage.points
        raw_picks[season][week][user].append(pick)
    return pick_counts, point_rollups, raw_picks


def calc_week_results(
    pick_counts: sct.PickCountsByUserByWeekBySeason,
    point_rollups: sct.PointRollupsByUserByWeekBySeason,
    raw_picks: sct.RawPicksByUserByWeekBySeason,
    sorted_games: list[Game],
    season: int,
    week: int,
) -> list[sct.WeekResults]:
    """Picks counts, rollups (points, total picks, etc) - data dictionaries for every user.
    Uses the ``sorted_scores`` input to sort the picked teams for each user; the left rollup on the
    allpicks view. First goes by status, then points, then mirrors the matchups sort (datetime
    then favoredteam name).

    :param pick_counts: The return from :func:`map_picks`.
    :param point_rollups: The return from :func:`map_picks`.
    :param raw_picks: The return from :func:`map_picks`.
    :param sorted_games: The return from :func:`~supercontest.dbsession.queries.get_scores`.
    :param season: Which season to extract from the other inputs.
    :param week: Which week to extract from the other inputs.
    :return: All results, an element per user.
    """
    # Filter input data for week, since this is for a week view.
    pick_counts_by_user = pick_counts[season][week]
    point_rollups_by_user = point_rollups[season][week]
    raw_picks_by_user = raw_picks[season][week]
    # Sort the teams for left rollup of allpicks view.
    sorted_picks_by_user: dict[int, list[Pick]] = {}
    for user, picks in raw_picks_by_user.items():
        sorted_picks_by_user[user] = sorted(
            picks,
            reverse=True,
            key=lambda pick: (
                (
                    1
                    if pick.contestant.game.status.unstarted is True
                    else 10
                    if pick.contestant.game.status.finished is True
                    else 5
                ),
                pick.contestant.coverage.points,
                -sorted_games.index(pick.contestant.game),
            ),
        )
    # Create all results sets for users.
    week_results: list[sct.WeekResults] = []
    for user in pick_counts_by_user.keys():
        week_results.append(
            sct.WeekResults(
                pick_counts=pick_counts_by_user[user],
                user_id=user,
                points_finished=point_rollups_by_user[user]["finished"],
                points_inprogress=point_rollups_by_user[user]["inprogress"],
                num_picks=sum(pick_counts_by_user[user].values()),
                sorted_pick_teams=[pck.contestant.team.name for pck in sorted_picks_by_user[user]],
            )
        )
    return week_results


def sort_week_results(week_results: list[sct.WeekResults]) -> list[sct.WeekResults]:
    """At the end of the day, only "total points" matters. However, we must display the rows on the
    site in a specific order, so we make that deterministic with the following logic:

    #. Greater total points COMPLETED
    #. Greater covers COMPLETED
    #. Fewer noncovers COMPLETED
    #. Greater total points IN PROGRESS
    #. Greater covers IN PROGRESS
    #. Fewer noncovers IN PROGRESS
    #. Greater number of total picks (give advantage to more active players)
    #. User ID (basically tenure, just keeping the sort deterministic)

    :param week_results: The return from :func:`calc_week_results`.
    :return: The same as the argument list, but sorted.
    """
    sorted_week_results = sorted(
        week_results,
        reverse=True,
        key=lambda user_dict: (
            user_dict["points_finished"],
            user_dict["pick_counts"]["cover"],
            -user_dict["pick_counts"]["noncover"],
            user_dict["points_inprogress"],
            user_dict["pick_counts"]["covering"],
            -user_dict["pick_counts"]["noncovering"],
            user_dict["num_picks"],
            -user_dict["user_id"],
        ),
    )
    return sorted_week_results


def get_toprow_week_results(
    sorted_games: list[Game],
    week_results: list[sct.WeekResults],
) -> list[tuple[int, int]]:
    """Takes all picks from all users and aggregates them into the top row for leaguewide pick
    counts for that week.

    :param sorted_games: The return from :func:`~supercontest.dbsession.queries.get_scores`.
    :param week_results: The return from :func:`calc_week_results`.
    :return: The picks counts for each team in each matchup that week.
    """
    users_picks = [user_dict["sorted_pick_teams"] for user_dict in week_results]
    flat_teams = [pick for user_picks in users_picks for pick in user_picks]
    pick_counts_by_team = Counter(flat_teams)
    toprow_week_results: list[tuple[int, int]] = []
    for game in sorted_games:
        contestant1, contestant2 = game.get_contestants(by="prediction", fail=False)
        toprow_week_results.append(
            (
                pick_counts_by_team[contestant1.team.name],
                pick_counts_by_team[contestant2.team.name],
            )
        )
    return toprow_week_results


def inject_week_results(  # pylint: disable=too-many-locals
    sorted_games: list[Game],
    sorted_week_results: list[sct.WeekResults],
    toprow_week_results: list[tuple[int, int]],
) -> list[sct.WeekResults]:
    """Does the post-sort value injection. For WEEK view this is rank and pick score/grade.
    Remember rank considers ONLY completed games (whereas full sort considers more). Note that
    this mutates the argument, the passed dict - and then returns it anyway to be explicit.
    Grade is the measure for how similar your picks are to other users (higher = you agreed
    with other users, lower = more users picked the opponent of your pick).

    :param sorted_games: The return from :func:`~supercontest.dbsession.queries.get_scores`.
    :param sorted_week_results: The return from :func:`sort_week_results`.
    :param toprow_week_results: The picks counts for each team in each matchup that week.
    :return: The same as the argument list, with keys injected into each dict element.
    """
    # Rank.
    point_totals = [user_data["points_finished"] for user_data in sorted_week_results]
    ranks = calc_ranks_from_points(points=point_totals)
    for index, rank in enumerate(ranks):
        sorted_week_results[index]["rank"] = rank
    # Pick Score. You have ordered/sorted lists, make a dict to lookup pick counts (+opponent) by
    # team, then iterate over each user and their picks.
    pick_counts_by_team: defaultdict[str, dict[str, int]] = defaultdict(dict)
    for game, counts in zip(sorted_games, toprow_week_results):
        contestant1, contestant2 = game.get_contestants(by="prediction", fail=False)
        team_picks1, team_picks2 = counts
        total = team_picks1 + team_picks2
        pick_counts_by_team[contestant1.team.name]["picks"] = counts[0]
        pick_counts_by_team[contestant1.team.name]["total"] = total
        pick_counts_by_team[contestant2.team.name]["picks"] = counts[1]
        pick_counts_by_team[contestant2.team.name]["total"] = total
    for week_result in sorted_week_results:
        num = sum(pick_counts_by_team[team]["picks"] for team in week_result["sorted_pick_teams"])
        den = sum(pick_counts_by_team[team]["total"] for team in week_result["sorted_pick_teams"])
        # If den=0 then num=0 and score=0 and no users have it. No concern for divide-by-zero.
        week_result["grade"] = get_percentage(num, den)
    return sorted_week_results


def view_matchups(
    season: int,
    week: int,
    user_id: int,
) -> tuple[list[Game], list[str]]:
    """The entry point for the matchups view.

    :param season: Season.
    :param week: Week.
    :param user_id: User ID.
    :return: Sorted score objects.
    :return: The names of the teams this user picked this week.
    """
    sorted_games = queries.get_ordered_games_in_week(season=season, week=week)
    picks = queries.get_picks_in_week_for_user(season=season, week=week, user_id=user_id)
    pick_teams = [pick.contestant.team.name for pick in picks]
    return sorted_games, pick_teams


def view_allpicks(
    season: int,
    week: int,
    league_id: int,
) -> tuple[list[Game], list[sct.WeekResults], list[tuple[int, int]]]:
    """The entry point for the allpicks view.

    :param season: Season.
    :param week: Week.
    :param league_id: League ID.
    :return: Sorted score objects.
    :return: The final/sorted/injected list of user:results dicts.
    :return: The leaguewide pick counts for each team, paired by matchups.
    :return: The map of points by team.
    """
    sorted_games = queries.get_ordered_games_in_week(season=season, week=week)
    picks = queries.get_picks_in_week_for_league(season=season, week=week, league_id=league_id)
    pick_counts, point_rollups, raw_picks = map_picks(picks=picks)
    week_results = calc_week_results(
        pick_counts=pick_counts,
        point_rollups=point_rollups,
        raw_picks=raw_picks,
        sorted_games=sorted_games,
        season=season,
        week=week,
    )
    sorted_week_results = sort_week_results(week_results=week_results)
    toprow_week_results = get_toprow_week_results(
        sorted_games=sorted_games,
        week_results=week_results,
    )
    final_week_results = inject_week_results(
        sorted_games=sorted_games,
        sorted_week_results=sorted_week_results,
        toprow_week_results=toprow_week_results,
    )
    return sorted_games, final_week_results, toprow_week_results


def calc_season_results(  # pylint: disable=too-many-locals
    pick_counts: sct.PickCountsByUserByWeekBySeason,
    point_rollups: sct.PointRollupsByUserByWeekBySeason,
    season: int,
) -> list[sct.SeasonResults]:
    """Picks counts, rollups (points, total picks, etc) - data dictionaries for every user.

    :param pick_counts: The return from :func:`map_picks`.
    :param point_rollups: The return from :func:`map_picks`.
    :param season: Which season to extract from the other inputs.
    :return: All results, an element per user.
    """
    # Filter input data for season, since this is for a season view.
    pick_counts_by_user_by_week = pick_counts[season]
    point_rollups_by_user_by_week = point_rollups[season]
    # Group the data by user at the toplevel key, since we're constructing results dicts per user.
    pick_counts_by_week_by_user: sct.PickCountsByWeekByUser = defaultdict(dict)
    for week, pick_counts_by_user in pick_counts_by_user_by_week.items():
        for user, _pick_counts in pick_counts_by_user.items():
            pick_counts_by_week_by_user[user][week] = _pick_counts
    point_rollups_by_week_by_user: sct.PointRollupsByWeekByUser = defaultdict(dict)
    for week, point_rollups_by_user in point_rollups_by_user_by_week.items():
        for user, _point_rollups in point_rollups_by_user.items():
            point_rollups_by_week_by_user[user][week] = _point_rollups
    # Then sum up all the weeks. For sums, we only need COMPLETED games. All the in-progress
    # data is stripped in this step, including for the "num_picks" attribute.
    season_results: list[sct.SeasonResults] = []
    for user, pick_counts_by_week in pick_counts_by_week_by_user.items():
        point_rollups_by_week = point_rollups_by_week_by_user[user]
        points = sum(point_rollups["finished"] for point_rollups in point_rollups_by_week.values())
        cover = sum(pick_counts["cover"] for pick_counts in pick_counts_by_week.values())
        push = sum(pick_counts["push"] for pick_counts in pick_counts_by_week.values())
        noncover = sum(pick_counts["noncover"] for pick_counts in pick_counts_by_week.values())
        num_picks = cover + push + noncover
        percentage = get_percentage(points, num_picks)
        season_results.append(
            sct.SeasonResults(
                pick_counts_by_week=pick_counts_by_week,
                point_rollups_by_week=point_rollups_by_week,
                user_id=user,
                cover=cover,
                push=push,
                noncover=noncover,
                points=convert_num_to_int_if_whole(points),
                num_picks=num_picks,
                percentage=percentage,
            )
        )
    return season_results


def sort_season_results(season_results: list[sct.SeasonResults]) -> list[sct.SeasonResults]:
    """Sorts all user-week-point info. This is the same as the weekly sort for the all-picks view,
    but because it's for the leaderboard (and graph), it only considers COMPLETED games. Sort logic:

    #. Greater total points COMPLETED
    #. Greater pick cover percentage
    #. Fewer pushes (to weight covers more than pushes, for same points)
    #. User ID (basically tenure, just keeping the sort deterministic)

    :param season_results: The return from :func:`calc_season_results`.
    :return: The same as the argument list, but sorted.
    """
    sorted_season_results = sorted(
        season_results,
        reverse=True,
        key=lambda user_dict: (
            user_dict["points"],
            user_dict["percentage"],
            -user_dict["push"],
            -user_dict["user_id"],
        ),
    )
    return sorted_season_results


def inject_season_results(  # pylint: disable=too-many-locals
    sorted_season_results: list[sct.SeasonResults],
    point_rollups: sct.PointRollupsByUserByWeekBySeason,
    season: int,
    league_id: int = 0,
) -> tuple[list[sct.SeasonResults], int]:
    """Does the post-sort value injection. The season views do a lot more than the week views.
    Rank, percentile are standards for free leagues. This function also handles the logic for
    paid leagues, adding the purse/bonus/payout keys.

    :param sorted_season_results: The return from :func:`sort_season_results`.
    :param point_rollups: The return from :func:`map_picks`.
    :param season: Season.
    :param league_id: League ID.
    :return: The same as the argument list, with keys injected into each dict element.
    :return: The remaining party fund after paid league distributions (0 if free league).
    """
    # Rank.
    points = [user_data["points"] for user_data in sorted_season_results]
    ranks = calc_ranks_from_points(points=points)
    for index, result in enumerate(sorted_season_results):
        result["rank"] = ranks[index]
    # Percentile
    num_total = len(sorted_season_results)  # num people in season
    for index, result in enumerate(sorted_season_results):
        this_rank = ranks[index]
        num_worse = sum((1 for rank in ranks if rank > this_rank))
        percentile = get_percentage(num_worse, num_total - 1, round_to=0)
        result["percentile"] = percentile
    if league_id == queries.get_paid_league_id_for_season(season=season):
        # Weekly bonuses (normalized).
        weekly_bonus_winners = calc_weekly_bonus_winners(point_rollups=point_rollups, season=season)
        # Purses, weekly bonuses (absolute), party fund.
        paid_players = queries.get_paid_users_for_season(season=season)
        weeks_in_season = queries.get_max_week(season=season)
        purses, weekly_bonus_winners_rounded, total_party = determine_payout(
            paid_players=paid_players,
            weeks_in_season=weeks_in_season,
            weekly_bonus_winners=weekly_bonus_winners,
        )
        purses, remainder = adjust_purses_for_tied_ranks(ranks=ranks, purses=purses)
        total_party += remainder
        for index, result in enumerate(sorted_season_results):
            purse = purses[index]
            bonus = weekly_bonus_winners_rounded[result["user_id"]]
            result["purse"] = purse
            result["bonus"] = bonus
            result["payout"] = purse + bonus
    else:
        total_party = 0  # just for consistent length return
    return sorted_season_results, total_party


def get_toprow_season_results(
    pick_counts: sct.PickCountsByUserByWeekBySeason,
    point_rollups: sct.PointRollupsByUserByWeekBySeason,
    season: int,
) -> tuple[list[float | int | None], float | int | None]:
    """Takes the season results by user and calculates the leaguewide perf. This is the top row of
    the leaderboard. Percentage per week, as well as overall percentage for the season.

    :param pick_counts: The return from :func:`map_picks`.
    :param point_rollups: The return from :func:`map_picks`.
    :param season: Season. Just used to find max week, padding the future ones with empty values.
    :return: Leaguewide pick percentages. Length is the number of weeks in the season. Elements are
        None for future weeks with no data.
    :return: Rollup for the whole league, whole season.
    """
    if len(point_rollups[season]) != 0:
        pick_counts_by_user_by_week = pick_counts[season]
        point_rollups_by_user_by_week = point_rollups[season]
        sorted_weeks = sorted(pick_counts_by_user_by_week.keys())
        data_cols: list[tuple[int | float, int | float]] = []
        for week in sorted_weeks:
            points = sum(
                _point_rollups["finished"]
                for _point_rollups in point_rollups_by_user_by_week[week].values()
            )
            picks = sum(
                _pick_counts["cover"] + _pick_counts["push"] + _pick_counts["noncover"]
                for _pick_counts in pick_counts_by_user_by_week[week].values()
            )
            data_cols.append((points, picks))
        toprow_cols = [get_percentage(points, picks) for points, picks in data_cols]
        data_rollup = tuple(map(sum, zip(*data_cols)))
        toprow_rollup = get_percentage(data_rollup[0], data_rollup[1])
    else:
        toprow_cols = []
        toprow_rollup = 0
    weeks_in_season = queries.get_max_week(season=season)
    toprow_cols += [None] * (weeks_in_season - len(toprow_cols))  # Pad with None
    return toprow_cols, toprow_rollup


def view_progression(season: int, league_id: int) -> list[sct.SeasonResults]:
    """The entry point for the graph view.

    :param season: Season.
    :param league_id: League ID.
    :return: The sorted list of user:results dicts.
    """
    picks = queries.get_picks_in_season_for_league(season=season, league_id=league_id)
    pick_counts, point_rollups, _ = map_picks(picks=picks)
    season_results = calc_season_results(
        pick_counts=pick_counts,
        point_rollups=point_rollups,
        season=season,
    )
    sorted_season_results = sort_season_results(season_results=season_results)
    return sorted_season_results


def view_leaderboard(
    season: int,
    league_id: int,
) -> tuple[list[sct.SeasonResults], list[float | int | None], float | int | None, int]:
    """The entry point for the leaderboard view.

    :param season: Season.
    :param league_id: League ID.
    :return: The final/sorted/injected list of user:results dicts.
    :return: Leaguewide pick percentages. Length is the number of weeks in the season. Elements are
        None for future weeks with no data.
    :return: Rollup for the whole league, whole season.
    :return: The remaining party fund after paid league distributions (0 if free league).
    """
    picks = queries.get_picks_in_season_for_league(season=season, league_id=league_id)
    pick_counts, point_rollups, _ = map_picks(picks=picks)
    season_results = calc_season_results(
        pick_counts=pick_counts,
        point_rollups=point_rollups,
        season=season,
    )
    sorted_season_results = sort_season_results(season_results=season_results)
    final_season_results, total_party = inject_season_results(
        sorted_season_results=sorted_season_results,
        point_rollups=point_rollups,
        season=season,
        league_id=league_id,
    )
    toprow_weeks, toprow_season = get_toprow_season_results(
        pick_counts=pick_counts,
        point_rollups=point_rollups,
        season=season,
    )
    return final_season_results, toprow_weeks, toprow_season, total_party


def calc_alltime_results(  # pylint: disable=too-many-locals
    pick_counts: sct.PickCountsByUserByWeekBySeason,
    point_rollups: sct.PointRollupsByUserByWeekBySeason,
) -> list[sct.AlltimeResults]:
    """Picks counts, rollups (points, total picks, etc) - data dictionaries for every user.

    :param pick_counts: The return from :func:`map_picks`.
    :param point_rollups: The return from :func:`map_picks`.
    :return: All aggregated results, an element per user.
    """
    # Group the data by user at the toplevel key, since we're constructing results dicts per user.
    pick_counts_by_season_by_user: sct.PickCountsBySeasonByUser = defaultdict(dict)
    for season, pick_counts_by_user_by_week in pick_counts.items():
        # Swap key order of per-season dataset so we can sum per user.
        pick_counts_by_week_by_user: sct.PickCountsByWeekByUser = defaultdict(dict)
        for week, pick_counts_by_user in pick_counts_by_user_by_week.items():
            for user, _pick_counts in pick_counts_by_user.items():
                pick_counts_by_week_by_user[user][week] = _pick_counts
        # Then sum over weeks per user.
        for user, pick_counts_by_week in pick_counts_by_week_by_user.items():
            pick_counts_by_season_by_user[user][season] = {
                "cover": sum(pc["cover"] for pc in pick_counts_by_week.values()),
                "push": sum(pc["push"] for pc in pick_counts_by_week.values()),
                "noncover": sum(pc["noncover"] for pc in pick_counts_by_week.values()),
            }
    point_rollups_by_season_by_user: sct.PointRollupsBySeasonByUser = defaultdict(dict)
    for season, point_rollups_by_user_by_week in point_rollups.items():
        # Swap key order of per-season dataset so we can sum per user.
        point_rollups_by_week_by_user: sct.PointRollupsByWeekByUser = defaultdict(dict)
        for week, point_rollups_by_user in point_rollups_by_user_by_week.items():
            for user, _point_rollups in point_rollups_by_user.items():
                point_rollups_by_week_by_user[user][week] = _point_rollups
        # Then sum over weeks per user.
        for user, point_rollups_by_week in point_rollups_by_week_by_user.items():
            point_rollups_by_season_by_user[user][season] = {
                "finished": convert_num_to_int_if_whole(
                    sum(pr["finished"] for pr in point_rollups_by_week.values())
                ),
            }
    # Then sum across all seasons. All completed games.
    alltime_results: list[sct.AlltimeResults] = []
    for user, pick_counts_by_season in pick_counts_by_season_by_user.items():
        point_rollups_by_season = point_rollups_by_season_by_user[user]
        points = sum(pr["finished"] for pr in point_rollups_by_season.values())
        cover = sum(pc["cover"] for pc in pick_counts_by_season.values())
        push = sum(pc["push"] for pc in pick_counts_by_season.values())
        noncover = sum(pc["noncover"] for pc in pick_counts_by_season.values())
        num_picks = cover + push + noncover
        percentage = get_percentage(points, num_picks)
        alltime_results.append(
            sct.AlltimeResults(
                pick_counts_by_season=pick_counts_by_season,
                point_rollups_by_season=point_rollups_by_season,
                user_id=user,
                cover=cover,
                push=push,
                noncover=noncover,
                points=convert_num_to_int_if_whole(points),
                num_picks=num_picks,
                percentage=percentage,
            )
        )
    return alltime_results


def sort_alltime_results(alltime_results: list[sct.AlltimeResults]) -> list[sct.AlltimeResults]:
    """Sorts the alltime leaderboard. Remember, rank is only dependent upon points (same as
    regular lb), but this sort considers more. It's deterministic, because the frontend has to
    show row by row. There are no ties in the visual organization space. Sort logic:

    #. Greater total points
    #. Greater pick cover percentage
    #. Fewer pushes (to weight covers more than pushes, for same points)
    #. User ID (similar to tenure, just keeping the sort deterministic)

    :param alltime_results: The return from :func:`calc_alltime_results`.
    :return: The same as the argument list, but sorted.
    """
    sorted_alltime_results = sorted(
        alltime_results,
        reverse=True,
        key=lambda user_dict: (
            user_dict["points"],
            user_dict["percentage"],
            -user_dict["push"],
            -user_dict["user_id"],
        ),
    )
    return sorted_alltime_results


def inject_alltime_results(
    sorted_alltime_results: list[sct.AlltimeResults],
    raw_picks: sct.RawPicksByUserByWeekBySeason,
) -> list[sct.AlltimeResults]:
    """Does the post-sort value injection. In-place, returning the same list-of-dicts with some
    new keys. Just rank for this one.

    :param sorted_alltime_results: The return from :func:`sort_alltime_results`.
    :param raw_picks: The return from :func:`map_picks`.
    :return: The same as the argument list, with keys injected into each dict element.
    """
    # Rank.
    points = [user_data["points"] for user_data in sorted_alltime_results]
    ranks = calc_ranks_from_points(points=points)
    user_pick_margins = calc_avg_user_pick_margins(raw_picks=raw_picks)
    for index, result in enumerate(sorted_alltime_results):
        result["rank"] = ranks[index]
        result["avg_pick_margin"] = user_pick_margins[result["user_id"]]
    return sorted_alltime_results


def get_toprow_alltime_results(
    pick_counts: sct.PickCountsByUserByWeekBySeason,
    point_rollups: sct.PointRollupsByUserByWeekBySeason,
) -> tuple[list[int | float], int | float]:
    """For each season, sums the total points across all users and the total picks across all users
    to determine a league-wide percentage. Then combines across all seasons to determine an alltime
    league-wide %. This doesn't filter by league, remember; it's the free league (all users).

    :param pick_counts: The return from :func:`map_picks`.
    :param point_rollups: The return from :func:`map_picks`.
    :return: Leaguewide pick percentages per season.
    :return: Rollup for alltime pick percentage, all users, all seasons, all leagues, etc.
    """
    sorted_seasons = sorted(pick_counts.keys())
    data_cols: list[tuple[int | float, int]] = []
    for season in sorted_seasons:
        points = sum(
            _point_rollups["finished"]
            for point_rollups_by_user in point_rollups[season].values()
            for _point_rollups in point_rollups_by_user.values()
        )
        picks = sum(
            _pick_counts["cover"] + _pick_counts["push"] + _pick_counts["noncover"]
            for pick_counts_by_user in pick_counts[season].values()
            for _pick_counts in pick_counts_by_user.values()
        )
        data_cols.append((points, picks))
    toprow_cols = [get_percentage(points, picks) for points, picks in data_cols]
    data_rollup = tuple(map(sum, zip(*data_cols)))
    toprow_rollup = get_percentage(data_rollup[0], data_rollup[1])
    return toprow_cols, toprow_rollup


def view_alltime_leaderboard() -> tuple[list[sct.AlltimeResults], list[int | float], int | float]:
    """The entry point for the alltimelb view.

    :return: The final/sorted/injected list of user:results dicts.
    :return: Leaguewide pick percentages per season.
    :return: Rollup for alltime pick percentage, all users, all seasons, all leagues, etc.
    """
    picks = queries.get_finished_picks()
    pick_counts, point_rollups, raw_picks = map_picks(picks=picks)
    alltime_results = calc_alltime_results(pick_counts=pick_counts, point_rollups=point_rollups)
    sorted_alltime_results = sort_alltime_results(alltime_results=alltime_results)
    final_alltime_results = inject_alltime_results(
        sorted_alltime_results=sorted_alltime_results,
        raw_picks=raw_picks,
    )
    toprow_seasons, toprow_alltime = get_toprow_alltime_results(
        pick_counts=pick_counts,
        point_rollups=point_rollups,
    )
    return final_alltime_results, toprow_seasons, toprow_alltime
