# pylint: disable=too-many-ancestors,missing-class-docstring,too-few-public-methods,no-member
"""Defines the serialization of models.

In most cases, app logic is kept to a minimum and frontend views map as close as possible to
DB tables/views. SQLAlchemy Rows are not json-serializable, so we have a few options: parse to
json-serializable objects (like dicts), dataclasses, or marshmallow. I've chosen the latter, since
(as a serialization lib) it has plugins to both flask and sqlalchemy.

By default, ``marshmallow-sqlalchemy`` will only include the PK for relationships. I want to
recurse down to include all objects, so I nest all relationships below. This handles all types: one
to one, one to many, many to one, and many to many.

Note, however, the relationships are unidirectional here (in the models they're bidirectional).
In the python ORM, we can have infinitely recursed pointers to the same objects. But when we're
serializing, we can't have an infinite-length string, so we keep the object hierarchy flowing in a
single direction. This is achieved by omitting the nesting on the children, and excluding the
backref on the parent. No data is *missing* - it's just located in a single place on the parent,
rather than both on the parent and child.

So only forward relationships (see models) are nested below, with the backward relationships
excluded from the nesting.
"""
from typing import Any
from flask_marshmallow import Marshmallow
from .models import (
    Season,
    Week,
    Role,
    League,
    User,
    Team,
    Prediction,
    Location,
    Coverage,
    Status,
    Game,
    Contestant,
    Pick,
)

#: The Marshmallow object used around the app to define schemas and serialize objects.
ma: Any = Marshmallow()


class SeasonSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Season
        include_fk = True
        include_relationships = True


class WeekSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Week
        include_fk = True
        include_relationships = True

    season = ma.Nested(SeasonSchema, exclude=("weeks",))


class RoleSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Role
        include_fk = True
        include_relationships = True


class LeagueSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = League
        include_fk = True
        include_relationships = True

    season = ma.Nested(SeasonSchema, exclude=("leagues",))


class UserSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = User
        include_fk = True
        include_relationships = True

    roles = ma.Nested(RoleSchema, many=True, exclude=("users",))
    leagues = ma.Nested(LeagueSchema, many=True, exclude=("users",))


class TeamSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Team
        include_fk = True
        include_relationships = True


class PredictionSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Prediction
        include_fk = True
        include_relationships = True


class LocationSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Location
        include_fk = True
        include_relationships = True


class CoverageSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Coverage
        include_fk = True
        include_relationships = True


class StatusSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Status
        include_fk = True
        include_relationships = True


class ContestantSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Contestant
        include_fk = True
        include_relationships = True

    team = ma.Nested(TeamSchema, exclude=("contestants",))
    prediction = ma.Nested(PredictionSchema, exclude=("contestants",))
    location = ma.Nested(LocationSchema, exclude=("contestants",))
    coverage = ma.Nested(CoverageSchema, exclude=("contestants",))


class GameSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Game
        include_fk = True
        include_relationships = True

    week = ma.Nested(WeekSchema, exclude=("games",))
    status = ma.Nested(StatusSchema, exclude=("games",))

    # This is intentionally ordered like this. While all other relationships flow in the
    # hierarchical direction, we switch Contestant and Game and nest on the backside of the
    # relationship here because the frontend uses Game objs at the toplevel, not Contestant objs.
    contestants = ma.Nested(ContestantSchema, many=True, exclude=("game",))


class PickSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Pick
        include_fk = True
        include_relationships = True

    user = ma.Nested(UserSchema, exclude=("picks",))
    contestant = ma.Nested(ContestantSchema, exclude=("picks",))
