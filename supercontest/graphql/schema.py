# pylint: disable=too-few-public-methods
"""The ``[...]`` specificiations in all the filter fields tells graphene-sqlalchemy-filter to infer
all the different filters that the field might support (``eq``, ``lt``, ``gt``, etc).

If you use pure ``SQLAlchemyConnectionField`` structures, then all of the field specifications
below would be unnecessary. It is a bit of a pain to have to update the API here when the models
change, but it's worth the convenience of being able to filter on column values.

You don't need to put ORM relationships in the fields below, just true columns.

Technically, there are no blockers for querying the current week's picks (sniffing
competition).
"""
import graphene
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyObjectType

from supercontest.models import (
    Season,
    Week,
    Role,
    League,
    User,
    Team,
    Prediction,
    Location,
    Coverage,
    Status,
    Game,
    Contestant,
    Pick,
)


class SeasonNode(SQLAlchemyObjectType):
    """Node for seasons."""

    class Meta:
        """Meta."""

        model = Season
        interfaces = (relay.Node,)


class SeasonConnection(relay.Connection):
    """Connection for seasons."""

    class Meta:
        """Meta."""

        node = SeasonNode


class WeekNode(SQLAlchemyObjectType):
    """Node for seasons."""

    class Meta:
        """Meta."""

        model = Week
        interfaces = (relay.Node,)


class WeekConnection(relay.Connection):
    """Connection for seasons."""

    class Meta:
        """Meta."""

        node = WeekNode


class RoleNode(SQLAlchemyObjectType):
    """Node for seasons."""

    class Meta:
        """Meta."""

        model = Role
        interfaces = (relay.Node,)


class RoleConnection(relay.Connection):
    """Connection for seasons."""

    class Meta:
        """Meta."""

        node = RoleNode


class LeagueNode(SQLAlchemyObjectType):
    """Node for seasons."""

    class Meta:
        """Meta."""

        model = League
        interfaces = (relay.Node,)


class LeagueConnection(relay.Connection):
    """Connection for seasons."""

    class Meta:
        """Meta."""

        node = LeagueNode


class UserNode(SQLAlchemyObjectType):
    """Node for seasons."""

    class Meta:
        """Meta."""

        model = User
        interfaces = (relay.Node,)


class UserConnection(relay.Connection):
    """Connection for seasons."""

    class Meta:
        """Meta."""

        node = UserNode


class TeamNode(SQLAlchemyObjectType):
    """Node for seasons."""

    class Meta:
        """Meta."""

        model = Team
        interfaces = (relay.Node,)


class TeamConnection(relay.Connection):
    """Connection for seasons."""

    class Meta:
        """Meta."""

        node = TeamNode


class PredictionNode(SQLAlchemyObjectType):
    """Node for seasons."""

    class Meta:
        """Meta."""

        model = Prediction
        interfaces = (relay.Node,)


class PredictionConnection(relay.Connection):
    """Connection for seasons."""

    class Meta:
        """Meta."""

        node = PredictionNode


class LocationNode(SQLAlchemyObjectType):
    """Node for seasons."""

    class Meta:
        """Meta."""

        model = Location
        interfaces = (relay.Node,)


class LocationConnection(relay.Connection):
    """Connection for seasons."""

    class Meta:
        """Meta."""

        node = LocationNode


class CoverageNode(SQLAlchemyObjectType):
    """Node for seasons."""

    class Meta:
        """Meta."""

        model = Coverage
        interfaces = (relay.Node,)


class CoverageConnection(relay.Connection):
    """Connection for seasons."""

    class Meta:
        """Meta."""

        node = CoverageNode


class StatusNode(SQLAlchemyObjectType):
    """Node for seasons."""

    class Meta:
        """Meta."""

        model = Status
        interfaces = (relay.Node,)


class StatusConnection(relay.Connection):
    """Connection for seasons."""

    class Meta:
        """Meta."""

        node = StatusNode


class GameNode(SQLAlchemyObjectType):
    """Node for seasons."""

    class Meta:
        """Meta."""

        model = Game
        interfaces = (relay.Node,)


class GameConnection(relay.Connection):
    """Connection for seasons."""

    class Meta:
        """Meta."""

        node = GameNode


class ContestantNode(SQLAlchemyObjectType):
    """Node for seasons."""

    class Meta:
        """Meta."""

        model = Contestant
        interfaces = (relay.Node,)


class ContestantConnection(relay.Connection):
    """Connection for seasons."""

    class Meta:
        """Meta."""

        node = ContestantNode


class PickNode(SQLAlchemyObjectType):
    """Node for seasons."""

    class Meta:
        """Meta."""

        model = Pick
        interfaces = (relay.Node,)


class PickConnection(relay.Connection):
    """Connection for seasons."""

    class Meta:
        """Meta."""

        node = PickNode


class Query(graphene.ObjectType):
    """Graphql overall query."""

    node = relay.Node.Field()
    seasons = relay.ConnectionField(SeasonConnection)
    weeks = relay.ConnectionField(WeekConnection)
    roles = relay.ConnectionField(RoleConnection)
    leagues = relay.ConnectionField(LeagueConnection)
    users = relay.ConnectionField(UserConnection)
    teams = relay.ConnectionField(TeamConnection)
    predictions = relay.ConnectionField(PredictionConnection)
    locations = relay.ConnectionField(LocationConnection)
    coverages = relay.ConnectionField(CoverageConnection)
    statuses = relay.ConnectionField(StatusConnection)
    games = relay.ConnectionField(GameConnection)
    contestants = relay.ConnectionField(ContestantConnection)
    picks = relay.ConnectionField(PickConnection)


schema = graphene.Schema(query=Query, auto_camelcase=False)
