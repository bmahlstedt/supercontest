# pyright: reportUnusedImport=false
"""Most the app is REST, but we expose a graph for users to query directly."""

from .schema import schema
