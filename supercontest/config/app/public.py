"""Configuration for the flask app.
"""
# custom
APP_NAME = "South Bay Supercontest"
APP_ADDRESS = "southbaysupercontest@gmail.com"

# sqlalchemy
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_ECHO = False

# flask-wtf
CRSF_ENABLED = True

# flask-user
USER_APP_NAME = APP_NAME
USER_EMAIL_SENDER_NAME = APP_NAME
USER_EMAIL_SENDER_EMAIL = APP_ADDRESS
USER_ENABLE_USERNAME = False
USER_ENABLE_CHANGE_USERNAME = False

# flask-mail
MAIL_SERVER = "smtp.gmail.com"
MAIL_PORT = 465
MAIL_USE_TLS = False
MAIL_USE_SSL = True
MAIL_USERNAME = APP_ADDRESS
MAIL_DEFAULT_SENDER = APP_ADDRESS

# stripe
STRIPE_PUBLIC_KEY_API = "pk_live_uMsP65dh3BkRw7utywCg0l9C"
STRIPE_PUBLIC_KEY_API_TEST = "pk_test_aShZiYysX8eMQTJzi3BarYym"
