import multiprocessing

NUM_CORES = multiprocessing.cpu_count()

wsgi_app = "supercontest:create_app()"
bind = ":8000"
worker_class = "gthread"
capture_output = True

workers = min(NUM_CORES * 2 + 1, 1)
threads = min(NUM_CORES * 2 + 1, 1)

loglevel = "debug"

reload = True
