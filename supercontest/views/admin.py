"""Defines all the views that are available on the admin panel."""
import logging
from typing import Any
from flask import redirect
from werkzeug.wrappers import Response as BaseResponse
from flask_user import current_user
from flask_admin import BaseView, expose, AdminIndexView
from flask_admin.form import SecureForm
from flask_admin.contrib.sqla import ModelView
from supercontest.dbsession import queries

logger = logging.getLogger(__name__)

#: The home route for the admin panel.
AdminHomeView = AdminIndexView(name="|", template="admin/home.html")


class SBSCBaseView(BaseView):
    """Common functionality across all supercontest admin views."""

    @expose("/")
    def view_caller(self: Any) -> str:
        """Generic caller for the actual view function.

        :return: The rendered template of the route.
        """
        return self.custom_view()  # pylint: disable=no-member

    def is_accessible(self) -> bool:
        """Checks if the view is reachable by the current user.

        :return: If the user is authenticated and has the admin role.
        """
        if not current_user.is_authenticated:
            return False
        return current_user.has_roles("Admin")

    def inaccessible_callback(self, name: str, **kwargs: Any) -> str:
        """Simply the callback for :meth:`is_accessible` failure.

        :param name: The flask_admin name.
        :param kwargs: The flask_admin kwargs.
        :return: The string when a user cannot access an admin view.
        """
        return "You must be an administrator to view this page."


class CommitLinesView(SBSCBaseView):
    """The weekly trigger for an admin to commit lines."""

    def custom_view(self: Any) -> str:
        """The explicit function to render the line commit template.

        :return: The rendered template of the route.
        """
        current_season, current_week = queries.get_current_season_and_week()
        output: str = self.render(
            "admin/commit_lines.html",
            current_week=current_week,
            current_season=current_season,
        )
        return output


class EmailAllUsersView(SBSCBaseView):
    """Allows admin to email the whole league, respecting notification settings.
    Sends to anyone who has ANY of the emails enabled (set union).
    Could filter down to current users who have picked this season, but then that
    wouldn't work in preseason to get people ready. Plus advertising is important -
    we want to increase reach as much as possible. Notification settings are
    custom per user, so they have full control, like any other app.
    """

    def custom_view(self: Any) -> str:
        """The explicit function to render the email-all-users template.

        :return: The rendered template of the route.
        """
        users1 = queries.get_users_with_email_pref(pref="email_when_picks_open")
        users2 = queries.get_users_with_email_pref(pref="email_when_picks_closing")
        users3 = queries.get_users_with_email_pref(pref="email_all_picks")
        recipients: list[str] = list(set(users1) | set(users2) | set(users3))
        mailto = f"mailto:{','.join(recipients)}"
        return self.render("admin/email_all_users.html", mailto=mailto)


class LatePicksView(SBSCBaseView):
    """Allows admin to submit late picks in an easy form."""

    def custom_view(self: Any) -> str:
        """The explicit function to render the late pick template.

        :return: The rendered template of the route.
        """
        current_season, current_week = queries.get_current_season_and_week()
        users = sorted([email.lower() for email in queries.get_all_user_emails()])
        games = []
        if current_season is not None and current_week is not None:
            games = queries.get_games_in_week(season=current_season, week=current_week)
        teams: list[str] = []
        for game in games:
            teams.append(game.contestants[0].team.name)
            teams.append(game.contestants[1].team.name)
        output: str = self.render(
            "admin/late_picks.html",
            current_week=current_week,
            current_season=current_season,
            users=users,
            teams=sorted(teams),
        )
        return output


class ChangeStatusView(SBSCBaseView):
    """Allows an admin to change the status of a game manually. Useful if
    the score-fetch is not updated to live/realtime.
    """

    def custom_view(self: Any) -> str:
        """The explicit view to render the status change template.

        :return: The rendered template of the route.
        """
        current_season, current_week = queries.get_current_season_and_week()
        games = []
        if current_season is not None and current_week is not None:
            games = queries.get_games_in_week(season=current_season, week=current_week)
        matchups = [
            " ".join(
                [
                    str(game.id),
                    game.contestants[0].team.name,
                    "vs",
                    game.contestants[1].team.name,
                ]
            )
            for game in games
        ]
        status_names = queries.get_status_names()
        output: str = self.render(
            "admin/change_status.html",
            current_week=current_week,
            current_season=current_season,
            matchups=matchups,
            status_names=status_names,
        )
        return output


class BackToSupercontestView(SBSCBaseView):
    """Takes you back to the actual supercontest, away from admin views."""

    def custom_view(self) -> BaseResponse:
        """The explicit redirect from the admin panel back to the supercontest.

        :return: A redirect to the root of the API.
        """
        return redirect("/")


class AdminModelView(ModelView):
    """Shows the admin the database tables for checking data. Note that I generalize all
    settings here, rather than a custom class per model.

    This one intentionally inherits from a different parent.
    """

    # Remove the CRUD abilities. I don't want untracked changes sourcing from the admin panel.
    can_edit = False
    can_create = False
    can_delete = False
    # CSRF.
    form_base_class = SecureForm
    # Pagination.
    page_size = 100
    # Allow csv export.
    can_export = True
    # Show the IDs too. And default to sort by them, descending.
    column_display_pk = True
    column_default_sort = ("id", True)
    # Show the eye at the left, "view record" button.
    can_view_details = True

    def __init__(self, model: Any, session: Any, **kwargs: Any):
        """Gets the columns of the model which (1) aren't FKs and (2) don't have remapped names.
        Allows the admin model to search, filter, and sort on those. Also sets the endpoint.
        """
        endpoint = f"{model.__name__.lower()}_model"
        cols = [
            col.name
            for col in model.__table__.columns
            if not col.foreign_keys and col.name in model.__dict__
        ]
        setattr(self, "column_searchable_list", cols)
        setattr(self, "column_filters", cols)
        setattr(self, "column_sortable_list", cols)
        parent: Any = super
        parent().__init__(model, session, endpoint=endpoint, **kwargs)

    def is_accessible(self) -> bool:
        """Checks if the view is reachable by the current user.

        :return: If the user is authenticated and has the admin role.
        """
        if not current_user.is_authenticated:
            return False
        return current_user.has_roles("Admin")

    def inaccessible_callback(self, name: str, **kwargs: Any) -> str:
        """Simply the callback for :meth:`is_accessible` failure.

        :param name: The flask_admin name.
        :param kwargs: The flask_admin kwargs.
        :return: The string when a user cannot access an admin view.
        """
        return "You must be an administrator to view this page."
