"""Utility functions for view preprocessors.

Much of this is logic surrounding the resolution of the requested values against logical
inferences for unpassed values. Also - retaining values of current view during linkbuilding
for other views in nav dropdowns.

Examples, for URL path params: Season, Week, League, Stats Scope.
"""
from typing import Any, ParamSpec
from collections.abc import Callable
from functools import wraps
from flask import g, Response, request, current_app
from flask_user import current_user
from supercontest.dbsession import queries

P = ParamSpec("P")

#: How long to keep views cached, by default. This is set to a minute frequency to
#: match the primary invalidator for views: scores changing.
VIEW_CACHE_TTL = 60


def service_account_only(func: Callable[P, Response]) -> Callable[P, Response]:
    """Route decorator to confirm that the caller is authorized as a service account.

    :param func: The decorated function.
    :return: The wrapper function.
    """

    @wraps(func)
    def wrapper(*args: P.args, **kwargs: P.kwargs) -> Response:
        """The wrapper function.

        :param args: Generic positional args to pass to the wrapped function.
        :param kwargs: Generic keyword args to pass to the wrapped function.
        :return: The response from this decorator's error checking, or the response from the
            decorated function.
        """
        if "cred" not in request.form:
            return Response("No cred provided", status=422)
        cred = request.form["cred"]
        user = queries.get_user_from_email("southbaysupercontest@gmail.com")
        if not current_app.user_manager.verify_password(cred, user.password):  # pyright: ignore
            return Response("Bad auth", status=401)
        return func(*args, **kwargs)

    return wrapper


def resolve_season(requested_season: int) -> int:
    """Called by ``url_defaults``, this converts requested values into final values before passing
    them to the route. If the user did not request a value, the default is set by ``url_defaults``
    (not this function).

    :param requested_season: The requested season.
    :return: The resolved season.
    """
    # If no season was provided, use current season. If None, use most recent.
    if requested_season == -1:
        resolved_season = queries.get_current_season() or queries.get_max_season()
    # If a season was requested, use it.
    else:
        resolved_season = requested_season
    return resolved_season


def resolve_league(requested_league: int, resolved_season: int) -> int:
    """Called by ``url_defaults``, this converts requested values into final values before passing
    them to the route. If the user did not request a value, the default is set by ``url_defaults``
    (not this function).

    :param requested_league: The requested league.
    :param resolved_season: The resolved season.
    :return: The resolved league.
    """
    # If the user requested the free league, or another league which exists for that season, use it.
    if requested_league == 0 or queries.is_league_in_season(requested_league, resolved_season):
        resolved_league = requested_league
    # If no league was requested, or the requested league isn't in that season, calculate it.
    # If paid -> paid league for that season. Else free.
    else:
        if queries.is_paid_user(season=resolved_season, user_id=current_user.id):
            resolved_league = queries.get_paid_league_id_for_season(season=resolved_season)
        else:
            resolved_league = 0
    return resolved_league


def resolve_week(  # pylint: disable=too-many-branches
    requested_week: int,
    resolved_season: int,
) -> int:
    """Called by ``url_defaults``, this converts requested values into final values before passing
    them to the route. If the user did not request a value, the default is set by ``url_defaults``
    (not this function).

    :param requested_week: The requested week.
    :param resolved_season: The resolved season.
    :return: The resolved week.
    :raises ValueError: If unable to resolve the week.
    """
    # You need to know the current season/week, no matter what. These can be None.
    current_season, current_week = queries.get_current_season_and_week()
    # Logic for resolving the week, based on if the user requested anything,
    # then if we're in the current season or an old season, then if the week
    # is available in that season and/or preseason coercion to 1 and post to 18.
    # resolved_season is a number - it cannot be None.
    if requested_week == -1:
        if resolved_season == current_season:
            if queries.is_regseason() and current_week is not None:  # implied
                resolved_week = current_week
            elif queries.is_preseason():
                resolved_week = 1
            elif queries.is_postseason() and current_season is not None:  # implied
                resolved_week = queries.get_max_week(season=current_season)
            else:
                print("Not sure which week to resolve to. Using 1.")
                resolved_week = 1
        else:  # offseason or any previous season
            resolved_week = queries.get_max_week(season=resolved_season)
    else:
        if resolved_season == current_season:
            if queries.is_regseason() and current_week is not None:  # implied
                resolved_week = requested_week if requested_week <= current_week else current_week
            elif queries.is_preseason():
                resolved_week = 1  # can ONLY request 1 for current preseason
            elif queries.is_postseason():
                resolved_week = requested_week
            else:
                raise ValueError("Not sure which week to resolve to")
        else:  # offseason or any previous season
            resolved_season_max_week = queries.get_max_week(season=resolved_season)
            # Eg cast 18 -> 17 if going to an earlier season.
            if requested_week > resolved_season_max_week:
                resolved_week = resolved_season_max_week
            else:
                resolved_week = requested_week
    return resolved_week


def resolve_stats_scope(requested_stats_scope: str) -> str:
    """Defaults to NFL. Also checks for unrecognized values.

    :param requested_stats_scope: Which scope the link wants.
    :raises ValueError: If an unknown stats scope is passed.
    :return: Resolved scope for the statistics view.
    """
    if requested_stats_scope not in ["nfl", "sbsc", ""]:
        raise ValueError(f"{requested_stats_scope=} must be 'nfl' or 'sbsc' or ''")
    resolved_stats_scope = requested_stats_scope or "nfl"
    return resolved_stats_scope


def url_defaults(
    endpoint: str,
    values: dict[str, Any],
    league: bool,
    week: bool,
    stats_scope: bool = False,
):
    """URL defaults are established before the route is run. There are many ``url_for()`` calls
    in the frontend, where we build links for all the nav tabs. When a page loads, it builds
    these links for all the other possible links, re-using as much of the current state as
    possible (i.e. if you're on the 2018 paid leaderboard, and you click 2019, it remembers
    you were on the lb and were filtering to the paid league).

    This simply pops off the ``values`` dict and replace the requested values with resolved
    values. Note that flask made the design decision of using a mutable ``values`` argument
    intentionally. You don't redefine and return the defaulted values, you manipulate the input
    ``values`` dict directly.

    Note also - the final resolved values for season and week are NOT the same as rhw
    ``current_season`` and ``current_week``. It's the page the user is visiting (you
    can visit old seasons and weeks that are not current).

    ``season`` is always required by contest views (although can be "all").

    If values are not provided, -1 is used to indicate null (for the resolvers above) for
    integers. For strings, it's an emptry string.

    Values of 0 are used to indicate ALL. Eg ``league=0`` means the free league, or all leagues.
    ``season=0`` means all seasons, as on statistical views.

    :param endpoint: The endpoint of the requested path that these params are defaulting for.
    :param values: The flask values.
    :param league: Whether or not league should be resolved with defaults.
    :param week: Whether or not week should be resolved with defaults.
    :param stats_scope: Whether or not the scope of a statistics view should be resolved.
    :raises ValueError: If an invalid scope is passed for a stats view.
    """
    requested_season = int(values.pop("season", -1))
    if "statistics" not in endpoint and requested_season == 0:
        requested_season = -1
    resolved_season = resolve_season(requested_season)
    values["season"] = resolved_season
    if league is True:
        requested_league = int(values.pop("league", -1))
        resolved_league = resolve_league(requested_league, resolved_season)
        values["league"] = resolved_league
    if week is True:
        requested_week = int(values.pop("week", -1))
        resolved_week = resolve_week(requested_week, resolved_season)
        values["week"] = resolved_week
    if stats_scope is True:
        requested_stats_scope = values.pop("stats_scope", "")
        resolved_stats_scope = resolve_stats_scope(requested_stats_scope)
        values["stats_scope"] = resolved_stats_scope


def url_value_preprocessor(
    values: dict[str, Any] | None,
    league: bool,
    week: bool,
    stats_scope: bool = False,
):
    """And then the URL value preprocessors take those resolved values, removing them as args
    to the route and instead attributing them to the flask global request object, g. This allows
    them to be used by views, templates, and javascript.

    Values will pass through the flask converter attached to each argument again, so although
    the defaults have casted the values to ints, we must do so here again (from strings).
    None should be empty at this point, however, so we don't have to default to avoid missing keys.

    Again, ``season`` is always required by contest views, and is a prerequisite to either
    ``week`` or ``league``.

    The ``params`` attribute is instantiated in the app's preprocessor. It is used for season,
    week, and league - which values to "retain" when creating the links in the navbar dropdowns.

    :param values: The flask values.
    :param league: Whether or not league should be attributed to g.
    :param week: Whether or not week should be attributed to g.
    :param stats_scope: Whether or not the scope of a statistics view should be added to g.
    :raises ValueError: If the flask values are empty (after my defaults and resolvers).
    """
    if values is None:
        raise ValueError
    g.season = int(values.pop("season"))
    if g.season != 0:
        g.weeks_in_season = queries.get_max_week(season=g.season)
    g.is_current_season_requested = g.season == g.current_season
    g.params["season"] = g.season
    if league is True:
        g.league = int(values.pop("league"))  # ID
        g.is_paid_league = g.league == queries.get_paid_league_id_for_season(season=g.season)
        g.available_leagues = queries.get_league_names(season=g.season)  # season specific!
        if g.league == 0:
            g.league_name = "Free"
        else:
            g.league_name = [name for id, name in g.available_leagues if id == g.league][0]
        if current_user.is_authenticated:
            g.user_in_league = queries.is_user_in_league(
                user_id=current_user.id,
                league_id=g.league,
            )
        else:
            g.user_in_league = False
        g.params["league"] = g.league
    if week is True:
        g.week = int(values.pop("week"))
        g.is_current_week_requested = g.is_current_season_requested and g.week == g.current_week
        g.picks_open = g.is_current_week_requested and queries.are_picks_open()
        g.params["week"] = g.week
    if stats_scope is True:
        g.stats_scope = values.pop("stats_scope")
        g.params["stats_scope"] = g.stats_scope
