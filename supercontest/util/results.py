"""Utilites for analysis; primarily the core results and stats modules."""
from statistics import fmean
from collections import defaultdict
from supercontest.util import types as sct
from supercontest.util.math import log_rank_distribution, round_down_to_nearest_10


def calc_ranks_from_points(points: list[int | float]) -> list[int]:
    """Give a list of total points (scores, basically), iterate through and rank them, giving the
    same rank for ties.

    :param points: The ordered list of point totals.
    :return: The ordered list of ranks, based on point totals.
    """
    if points == []:
        return []
    ranks = [1]
    previous_score = points[0]
    for index, score in enumerate(points[1:], start=2):
        rank = ranks[-1] if score == previous_score else index
        ranks.append(rank)
        previous_score = score
    return ranks


def calc_avg_user_pick_margins(raw_picks: sct.RawPicksByUserByWeekBySeason) -> dict[int, float]:
    """Takes all picks and returns the average overperformance margin per USER.

    :param raw_picks: The raw pick objects from the db.
    :return: Dictionary of user:avg.
    """
    user_pick_margins: defaultdict[int, list[float]] = defaultdict(list)
    for season in raw_picks:
        for week in raw_picks[season]:
            for user in raw_picks[season][week]:
                for pick in raw_picks[season][week][user]:
                    user_pick_margins[user].append(pick.contestant.margin)
    avg_user_pick_margins = {
        user: fmean(pick_margins) for user, pick_margins in user_pick_margins.items()
    }
    return avg_user_pick_margins


def calc_weekly_bonus_winners(
    point_rollups: sct.PointRollupsByUserByWeekBySeason,
    season: int,
) -> defaultdict[int, float]:
    """Takes weekly point totals for all users determines what the current payout for the bonus is
    (accumulating over weeks where it is not won).

    Note that this returns the normalized prize. If you win a single perfect week prize, it's 1.
    If it accumulates over 3 weeks and no one else hits 5 and then you do in the 3rd week, the
    return is 3. If you split a single week with 3 people, your return is 0.33.

    :param point_rollups: The return from :func:`map_picks`.
    :param season: The season to calculate for (just filters the first param).
    :return: Keys are user IDs, values are weekly prizes won. For a user without a prize, it will
        default to ``0.0``.
    """
    # Init.
    point_rollups_by_user_by_week = point_rollups[season]
    sorted_weeks = sorted(point_rollups_by_user_by_week.keys())
    # Iterate.
    weekly_bonus_winners: defaultdict[int, float] = defaultdict(lambda: 0.0)
    current_bonus = 1
    for week in sorted_weeks:
        point_rollups_by_user = point_rollups_by_user_by_week[week]
        winners = [
            user_id
            for user_id, point_rollups in point_rollups_by_user.items()
            if point_rollups["finished"] == 5
        ]
        if winners:
            for winner in winners:
                weekly_bonus_winners[winner] += current_bonus / len(winners)
            current_bonus = 1
        else:
            current_bonus += 1
    return weekly_bonus_winners


def determine_payout(  # pylint: disable=too-many-locals
    paid_players: list[int],
    weeks_in_season: int,
    weekly_bonus_winners: defaultdict[int, float],
    buyin: int | float = 50,
    party_fee: int | float = 2,
    weekly_bonus: int | float = 10,
) -> tuple[list[int], defaultdict[int, int], int]:
    """Determines payout for the paid league for a season. All logic to check ``league_id`` or if
    it's a free league must be done prior.

    Free users cannot win a weekly prize for 5 correct picks, obviously. Note the weekly prize is
    rounded down to a whole dollar (int), if split, and the remainder is given to the party fund
    (as well as if no one hits 5 at the end of the season).

    :param paid_players: The list of player IDs in the paid league to distribute payments to.
    :param weeks_in_season: The number of weeks in the season (used for weekly bonus purse size).
    :param weekly_bonus_winners: The return from :func:`calc_weekly_bonus_winners`.
    :param buyin: The cost of entry per player in the paid league.
    :param party_fee: The fee from ``buyin`` which goes toward the end-of-year party.
    :param weekly_bonus: The allocation toward each week's group of perfect-5-pickers, rolling over
        if not won.
    :return: Purses for position winnings, in order of rank, zero padded.
    :return: Weekly bonuses. Keys are ``user_id``, values are dollars won (default ``0.0``).
    :return: Party fund.
    """
    # Init.
    paid_player_count = len(paid_players)
    # Basic totals.
    total_payout = buyin * paid_player_count
    total_party = party_fee * paid_player_count
    total_bonus = weekly_bonus * weeks_in_season
    total_purse = total_payout - total_party - total_bonus
    # Calculate position purses.
    purses, remainder = log_rank_distribution(total=total_purse, floor=buyin)
    purses += [0] * (paid_player_count - len(purses))  # zero pad for everyone not winning a purse
    total_party += remainder  # 1st place round-down remainder
    # Calculate weekly prizes. We already have the normalized bonuses, we just need to scale them.
    # The int call rounds it down to an integer.
    weekly_bonus_winners_rounded: defaultdict[int, int] = defaultdict(int)
    for key, value in weekly_bonus_winners.items():
        weekly_bonus_winners_rounded[key] = int(value * weekly_bonus)
    remainder = total_bonus - sum(weekly_bonus_winners_rounded.values())
    total_party += remainder
    # Total party fund is rounded down to the dollar. So a number of cents are lost to the abyss.
    return purses, weekly_bonus_winners_rounded, int(total_party)


def adjust_purses_for_tied_ranks(
    ranks: list[int],
    purses: list[int | float] | list[int] | list[float],
    min_purse: int | float = 50,
    round_down_threshold: int = 10,
) -> tuple[list[int], int]:
    """The discrete log series gives us purses for each rank, but we need to handle ties. If
    multiple users are split for a rank, they evenly split the sum of purses across all of their
    ranks. This needs to repeat some of the logic from :func:`log_rank_distribution`.

    In the case that a tied rank at the bottom of the purse positions results
    in a split payment that is lower than the ``min_purse`` (usually buyin), this
    will cut them off and add to the 1st place winner(s).

    It then, of course, rounds all resultant purses down to the nearest 10,
    gives the extra to 1st place(s), then rounds down a final time, and returns
    the remainder.

    Since you can have multiple people tied for first, this function recurses
    until the remainder is small enough to be a roundoff (as defined by input).

    :param ranks: The return from :func:`calc_ranks_from_points`.
    :param purses: The return from :func:`determine_payout`.
    :param min_purse: This is needed here again, because a split purse may result in a purse that
        is smaller than the buyin.
    :param round_down_threshold: The terminal condition for recursion; it re-splits and re-rounds
        until the remainder is smaller than the round-down precision.
    :return: The modified purse list, with proper splits for ties.
    :return: The final remainder after recursion/redistribution (smaller than round-down precision).
    """
    # Exit early.
    if len(ranks) == 0:
        return [], 0
    # Establish structures.
    new_purses: list[int | float] = []
    total_purse = sum(purses)
    # Init.
    previous_rank = ranks[0]  # obviously should be 1
    active_sum = purses[0]
    split = 1  # how many people to split the active_sum with
    # Primary iteration.
    for rank, purse in zip(ranks[1:], purses[1:]):
        if rank == previous_rank:
            active_sum += purse
            split += 1
        else:
            for _ in range(split):
                new_purses.append(active_sum / split)
            active_sum = purse  # re-initialize purse
            split = 1  # re-initialize number of players on tied rank
        previous_rank = rank
    # And then no matter what, you'll end up with nonzero active sum at the end
    # (from the single last place person, or the sum if multiple people tied
    # for last). Distribute it.
    for _ in range(split):
        new_purses.append(active_sum / split)
    # Adjust if any new purses are less than the minimum.
    new_purses = [purse if purse >= min_purse else 0 for purse in new_purses]
    # Round down (which casts back to int as well, since we probably got some
    # floats from the split division).
    new_purses = [round_down_to_nearest_10(purse) for purse in new_purses]
    # And give all the remainders back to first place(s).
    remainder = total_purse - sum(new_purses)
    # Now we need to determine, with that extra amount added evenly to first
    # places, how to split and round down again. Recurse this function.
    how_many_tied_for_first = ranks.count(1)
    if (add_more := remainder / how_many_tied_for_first) >= round_down_threshold:
        for index in range(how_many_tied_for_first):
            new_purses[index] += add_more
        return adjust_purses_for_tied_ranks(
            ranks=ranks,
            purses=new_purses,
            min_purse=min_purse,
        )
    # Exit condition is if the remainder, divided among the people tied for
    # first, is less than our round-down amount anyway. Return the remainder
    # and add it to the party fund. If you've gotten past the recursion point,
    # we have even purses, and can cast back to int types.
    return [int(purse) for purse in new_purses], int(remainder)
