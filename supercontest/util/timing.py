"""Time, date, timezone, conversion - all timing related utilities."""
import datetime
import calendar
from dateutil import parser as dateutil_parser
import pytz


def convert_day_int_to_str(day_int: int) -> str:
    """Converts an integer to its English day-of-the-week equivalent.

    :param day_int: 0, 1, ..., 6
    :return: Monday, Tuesday, ... , Sunday
    """
    return calendar.day_name[day_int]


def convert_day_str_to_int(day_str: str) -> int:
    """Converts an English day-of-the-week string to its integer equivalent.

    :param day_str: Monday, Tuesday, ... , Sunday
    :return: 0, 1, ..., 6
    """
    return calendar.day_name[:].index(day_str)


def hours_since(start: datetime.datetime) -> float:
    """Calculates the number of hours since a certain time.

    :param start: Datetime, aware in UTC timezone.
    :return: Number of hours since the start time.
    """
    utc_now = get_utc_now()
    return (utc_now - start) / datetime.timedelta(hours=1)


def is_today(allowable_days: tuple[str, ...]) -> bool:
    """Checks if today is in a list of acceptable days.

    :param allowable_days: Eg ``['Monday', 'Thursday', ... ]``
    :return: If today is in the provided list of days.
    """
    today_int = convert_utc_to_pt(datetime.datetime.today()).weekday()
    today_name = convert_day_int_to_str(today_int)
    return today_name in allowable_days


def convert_westgate_datetime_to_utc(date_str: str) -> datetime.datetime:
    """Properly formats the strings for datetime in the database into python datetime objects,
    including tzinfo.

    Westgate returns a date string like this: ``"THURSDAY, NOVEMBER 9, 2017 5:25 PM"``. It is in
    the pacific timezone.

    :param date_str: The westgate datetime string (pacific).
    :return: The python standard datetime object (in UTC) for that westgate string.
    """
    naive_la_datetime = dateutil_parser.parse(date_str)
    # Vegas and LA are the same, both are Pacific Time and both respect DST.
    la_timezone = pytz.timezone("America/Los_Angeles")
    # Pass nothing for DST so pytz won't guess. It should infer from the timezone.
    la_datetime = la_timezone.localize(naive_la_datetime, is_dst=None)
    utc_datetime = la_datetime.astimezone(pytz.utc)
    return utc_datetime


def convert_utc_to_pt(datetime_obj: datetime.datetime) -> datetime.datetime:
    """I have timezone explicitly set True in sqlalchemy, ``db.session.query`` and ``psql`` queries
    both return UTC datetimes. This function converts them to pacific time.

    :param datetime_obj: A datetime object aware in the utc timezone.
    :return: A datetime object aware in the pacific timezone.
    """
    return datetime_obj.astimezone(pytz.timezone("America/Los_Angeles"))


def get_utc_now() -> datetime.datetime:
    """All the timestamps in postgres are stored as UTC. Therefore, to make proper comparison, you
    must always use ``utcnow()``. Remember ``datetime`` returns the object of the current UTC
    timestamp, but we want it fully aware with ``tzinfo``, so we pass to ``pytz``.

    :return: The datetime obj for NOW, aware in the UTC timezone.
    """
    return pytz.utc.localize(datetime.datetime.utcnow())


def get_dow(datetime_obj: datetime.datetime) -> str:
    """Takes a datetime (like the ``game.datetime`` col from the db) and returns the day of the
    week. Specifically ignores year, week, etc - this just categorizes along weekday. Times are
    cast to PT. This is obviously for statistical analysis (bucketing).

    :param datetime_obj: A datetime object.
    :return: Eg ``"Sun"``.
    """
    return convert_utc_to_pt(datetime_obj).strftime("%a")


def is_around_kickoff(datetimes: list[datetime.datetime], within: int = 30) -> bool:
    """Checks if you are in a window around a kickoff time (set by lines), before or after,
    to force the score fetch.

    :param datetimes: The ``datetime`` cols (just vals) of the ``games`` table.
    :param within: Number of minutes (before AND after) around kickoff to check within.
    :return: True if around kickoff (fetch scores), False if not around kickoff (don't fetch).
    """
    utc_now = get_utc_now()
    for kickoff in datetimes:
        window = datetime.timedelta(minutes=within)
        if kickoff - window < utc_now < kickoff + window:
            return True
    return False
