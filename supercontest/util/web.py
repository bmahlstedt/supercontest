"""Utilities for managing browser access within the app (webscrapes)."""

from functools import wraps
from collections.abc import Callable
from typing import Concatenate, ParamSpec, TypeVar
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

P = ParamSpec("P")
R = TypeVar("R")


def get_webdriver() -> webdriver.Chrome:
    """Instantiate and configure a selenium webdriver for chrome (headless).

    :return: The configured web driver.
    """
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    driver = webdriver.Chrome(options=chrome_options)
    driver.implicitly_wait(30)
    return driver


def with_webdriver(
    func: Callable[Concatenate[webdriver.Chrome, P], R],
) -> Callable[P, R]:
    """Provides a webdriver for the calling function. And quits the driver after.

    :param func: The decorated function.
    :return: The wrapper function.
    """

    @wraps(func)
    def wrapper(*args: P.args, **kwargs: P.kwargs) -> R:
        """The wrapper function.

        :param args: Generic positional args to pass to the wrapped function.
        :param kwargs: Generic keyword args to pass to the wrapped function.
        :return: The wrapped function with the driver argument inserted.
        """
        driver = get_webdriver()
        try:
            return func(driver, *args, **kwargs)
        finally:
            driver.quit()

    return wrapper
