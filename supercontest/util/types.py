"""Collection of custom types, classes, aliases, and generics used by other modules."""
from collections import defaultdict
from typing import TypedDict, NotRequired, Any, cast


def ddict2dict(ddict: defaultdict[Any, Any] | dict[Any, Any]) -> dict[Any, Any]:
    """Converts a defaultdict to a dict. Handles nested/recursion.

    :param ddict: Default dictionary.
    :return: Dictionary.
    """
    for key, val in ddict.items():
        if isinstance(val, dict):
            ddict[key] = ddict2dict(cast(dict[Any, Any], val))
    return dict(ddict)


class LinesFetch(TypedDict):
    """Keys and their types for the fetched lines dictionary."""

    datetime: str
    line: str
    favored_team: str
    underdog_team: str
    home_team: str | None


class ScoresFetch(TypedDict):
    """Keys and their types for the fetched scores dictionary."""

    team_scores: dict[str, int]
    status: str  # espn_abbv


# Keys are the different coverage names (``cover``, ``pushing``, etc) in the DB.
# Note that we append "ing" for in-progress games.
#: Pick counts.
PickCounts = defaultdict[str, int]
#: Pick counts, by user.
PickCountsByUser = defaultdict[int, PickCounts]
#: Pick counts, by user, by week.
PickCountsByUserByWeek = defaultdict[int, PickCountsByUser]
#: Pick counts, by user, by week, by season.
PickCountsByUserByWeekBySeason = defaultdict[int, PickCountsByUserByWeek]
#: Pick counts, by week.
PickCountsByWeek = dict[int, PickCounts]
#: Pick counts, by week, by user.
PickCountsByWeekByUser = defaultdict[int, PickCountsByWeek]
#: Pick counts, by season.
PickCountsBySeason = dict[int, dict[str, int]]
#: Pick counts, by season, by user.
PickCountsBySeasonByUser = defaultdict[int, PickCountsBySeason]

# Keys are ``finished`` / ``inprogress``.
#: Point rollups.
PointRollups = defaultdict[str, float]
#: Point rollups, by user.
PointRollupsByUser = defaultdict[int, PointRollups]
#: Point rollups, by user, by week.
PointRollupsByUserByWeek = defaultdict[int, PointRollupsByUser]
#: Point rollups, by user, by week, by season.
PointRollupsByUserByWeekBySeason = defaultdict[int, PointRollupsByUserByWeek]
#: Point rollups, by week.
PointRollupsByWeek = dict[int, PointRollups]
#: Point rollups, by week, by user.
PointRollupsByWeekByUser = defaultdict[int, PointRollupsByWeek]
#: Point rollups, by season.
PointRollupsBySeason = dict[int, dict[str, float]]
#: Point rollups, by season, by user.
PointRollupsBySeasonByUser = defaultdict[int, PointRollupsBySeason]

#: Picks, by user.
RawPicksByUser = defaultdict[int, list[Any]]
#: Picks, by user, by week.
RawPicksByUserByWeek = defaultdict[int, RawPicksByUser]
#: Picks, by user, by week, by season.
RawPicksByUserByWeekBySeason = defaultdict[int, RawPicksByUserByWeek]


class WeekResults(TypedDict):
    """Typing for single-row (user) results of a WEEK-scoped view."""

    # raw pick count info
    pick_counts: PickCounts
    # pre-sort rollups
    user_id: int
    points_finished: float
    points_inprogress: float
    num_picks: int
    sorted_pick_teams: list[str]
    # post-sort rollups
    rank: NotRequired[int]
    grade: NotRequired[float]


class SeasonResults(TypedDict):
    """Typing for single-row (user) results of a SEASON-scoped view."""

    # raw pick count info (cols of frontend view) - these include in-progress statuses
    pick_counts_by_week: PickCountsByWeek
    point_rollups_by_week: PointRollupsByWeek
    # pre-sort rollups (all of these only consider completed games, not in-progress)
    user_id: int
    cover: int
    push: int
    noncover: int
    points: int | float
    num_picks: int
    percentage: int | float
    # post-sort rollups
    rank: NotRequired[int]
    percentile: NotRequired[int | float]
    purse: NotRequired[int]
    bonus: NotRequired[int]
    payout: NotRequired[int]


class AlltimeResults(TypedDict):
    """Typing for single-row (user) results of an ALLTIME-scoped view."""

    # raw pick count info (cols of frontend view) - only completed statuses
    pick_counts_by_season: PickCountsBySeason
    point_rollups_by_season: PointRollupsBySeason
    # pre-sort rollups (all of these only consider completed games, not in-progress)
    user_id: int
    cover: int
    push: int
    noncover: int
    points: int | float
    num_picks: int
    percentage: int | float
    # post-sort rollups
    rank: NotRequired[int]
    avg_pick_margin: NotRequired[int | float]
