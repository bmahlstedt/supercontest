# pylint: disable=import-outside-toplevel,invalid-name
"""The South Bay Supercontest.

This is the app factory pattern.

All stdlib packages are imported at the toplevel. All first-party and third-party packages
are intentionally lazy-loaded. Some hoisted to toplevel purely for types.
"""
import os
from typing import Any, cast
import logging
import pkg_resources
from flask import Flask
from flask_sqlalchemy import SQLAlchemy


def get_package_info() -> tuple[str, str]:
    """Inspects the supercontest package version, to smartly set other extension configs.

    :return: SBSC package name and version.
    """
    pkg_name = __name__
    pkg_version = pkg_resources.get_distribution(pkg_name).version
    return pkg_name, pkg_version


def set_env_modes(app: Flask, test: bool) -> None:
    """Establishes the modes of the app creation. These are used to condition other instantiations
    in the app factory, which generally flow in order of criticality. Here's the order of app
    complexity, in terms of mode (and thus how quickly the app factory returns without extra
    configuration): Test -> CLI -> Dev -> Prod.

    Note that modes are independent. You can have multiple (or none) be true.

    We don't pass booleans around for modes. They're written to the app. This allows other
    extensions (as well as sbsc itself) to piggyback on them.

    * ``app.config["TESTING"]`` (test mode)
    * ``app.config["CLI"]`` (cli mode)
    * ``app.config["DEBUG"]`` (dev mode)

    Test mode just instantiates app-related things, like the routes/blueprints. It is passed
    from the factory itself.

    CLI mode is for dropping into the flask CLI. The factory is still called, but needs less
    configuration. This is primarily used for ``flask-migrate``, for commands like ``flask db <>``
    to run alembic migrations. It is set as an env var from the makefile.

    Dev vs prod is the primary distinction. Flask prefers debug mode be set from the command line,
    but SBSC is frontend by gunicorn, so there's no command line invocation (and no ``flask.run()``
    python invocation for the ``debug`` arg). So we set in the docker compose file as an env var.
    As for gunicorn itself being dev vs prod: that's set in the gunicorn config.

    :param app: The flask app.
    :param test: Passed through :func:`create_app`.
    """
    app.config["TESTING"] = test
    app.config["CLI"] = bool(int(os.environ.get("FLASK_CLI", 0)))
    app.config["DEBUG"] = bool(int(os.environ.get("SC_DEV", 0)))


def configure_logging(app: Flask) -> logging.Logger:
    """Configures the root logger to capture everything from all other loggers. This includes the
    logger from this exact module, all other sbsc modules, flask's logger (``app.logger``), and
    all other packages that have loggers without a custom handler and with ``propagate=True``.
    ``basicConfig()`` just creates a streamHandler with the specified formats for the root logger.

    :param app: The flask app.
    :return: The logger for this module.
    """
    from supercontest.util.logging import create_handler

    root_logger = logging.getLogger()
    root_logger.addHandler(create_handler())
    if app.config["CLI"]:
        log_level = logging.WARNING
    elif app.config["DEBUG"] or app.config["TESTING"]:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    root_logger.setLevel(log_level)
    # Gunicorn logging specifics. By default, gunicorn swallows all streams
    # and only prints its stuff. So we do a few things.
    # (1) Add capture_output=True to the server configs, stopping the swallow.
    # (2) Remove the stream handler for gunicorn (since we use a custom format
    #     above) and set the gunicorn loggers to just propagate up to our root.
    logging.getLogger("gunicorn.access").propagate = True
    logging.getLogger("gunicorn.error").propagate = True
    logging.getLogger("gunicorn.error").handlers = []
    # Instantiate a logger for this exact module, just like all the other modules in this package.
    logger = logging.getLogger(__name__)
    return logger


def get_config_dir() -> str:
    """Fetches the path of the configuration directory.

    :return: Absolute path of config dir.
    """
    curdir = os.path.dirname(os.path.abspath(__file__))
    cfgdir = os.path.join(curdir, "config")
    return cfgdir


def configure_app(app: Flask, cfgdir: str) -> None:
    """Mutates the flask app with some configuration.

    :param app: The flask app to configure.
    :param cfgdir: The path to configs.
    """
    app.config.from_pyfile(  # pyright: ignore[reportUnknownMemberType]
        os.path.join(cfgdir, "app", "public.py")
    )
    if app.config["TESTING"]:
        test_configs = {
            "TESTING": True,  # propagate exceptions
            "MAIL_SUPPRESS_SEND": True,  # don't send mail
            "WTF_CSRF_ENABLED": False,  # disable csrf form validation
        }
        app.config |= test_configs  # dict update
    else:  # we still configure private in dev
        app.config.from_pyfile(  # pyright: ignore[reportUnknownMemberType]
            os.path.join(cfgdir, "app", "private.py")
        )


def register_blueprints(app: Flask) -> None:
    """Adds the endpoints to the app.

    :param app: The flask object.
    """
    from supercontest.views import (
        main_blueprint,
        matchups_blueprint,
        picks_blueprint,
        leaderboard_blueprint,
        progression_blueprint,
        statistics_blueprint,
        schedules_blueprint,
    )

    app.register_blueprint(main_blueprint)
    app.register_blueprint(matchups_blueprint)
    app.register_blueprint(picks_blueprint)
    app.register_blueprint(leaderboard_blueprint)
    app.register_blueprint(progression_blueprint)
    app.register_blueprint(statistics_blueprint)
    app.register_blueprint(schedules_blueprint)


def configure_cache(app: Flask, cfgdir: str) -> None:
    """Initializes the cache.

    :param app: The flask app.
    :param cfgdir: Abs path to config dir.
    """
    from supercontest.models import cache

    # If in dev or testing mode, set up a NULL cache.
    # If in prod, for the actual app OR for flask-cli, use the redis elasticache.
    if app.config["DEBUG"] or app.config["TESTING"]:
        cache.init_app(app, config={"CACHE_TYPE": "NullCache"})
        return

    from supercontest.util.config import get_sectionless_config

    cache_public_conf = get_sectionless_config(os.path.join(cfgdir, "cache", "public.conf"))
    cache_private_conf = get_sectionless_config(os.path.join(cfgdir, "cache", "private.conf"))
    cache_conf = cache_public_conf | cache_private_conf
    app.config["CACHE_REDIS_CLUSTER"] = f"{cache_conf['CACHE_HOST']}:{cache_conf['CACHE_PORT']}"
    app.config["CACHE_REDIS_PASSWORD"] = cache_conf["REDISCLI_AUTH"]
    app.config["CACHE_OPTIONS"] = {"ssl": True}
    app.config["CACHE_TYPE"] = "RedisClusterCache"
    cache.init_app(app)


def configure_database(app: Flask, cfgdir: str) -> SQLAlchemy:
    """Grabs the db URI, auth, and initializes the ORM.

    :param app: The flask app.
    :param cfgdir: Abs path to config dir.
    :return: SQLAlchemy object.
    """
    from supercontest.util.config import get_sectionless_config

    if app.config["DEBUG"] or app.config["TESTING"]:
        db_public_conf_file = "public-dev.conf"
    else:
        db_public_conf_file = "public-prod.conf"
    db_public_conf = get_sectionless_config(os.path.join(cfgdir, "db", db_public_conf_file))
    db_private_conf = get_sectionless_config(os.path.join(cfgdir, "db", "private.conf"))
    db_conf = db_public_conf | db_private_conf
    app.config["SQLALCHEMY_DATABASE_URI"] = (
        "postgresql://"
        + f"{db_conf['POSTGRES_USER']}"
        + ":"
        + f"{db_conf['POSTGRES_PASSWORD']}"
        + "@"
        + f"{db_conf['POSTGRES_HOST']}"
        + ":"
        + f"{db_conf['POSTGRES_PORT']}"
        + "/"
        + f"{db_conf['POSTGRES_DB']}"
    )

    from supercontest.models import db

    db.init_app(app)
    return db


def configure_db_obj_serialization(app: Flask) -> None:
    """Initializes the app with serialization capability, using marshmallow to pass database
    rows from serverside sqla to frontend js.

    :param app: The flask app.
    """
    from supercontest.models import ma

    ma.init_app(app)


def configure_migrations(app: Flask, db: SQLAlchemy) -> None:
    """Initializes alembic.

    :param app: The flask app.
    :param db: The sqla object.
    """
    from flask_migrate import Migrate

    migrate: Any = Migrate()
    migrate.init_app(app, db)


def configure_user_manager(app: Flask, db: SQLAlchemy) -> None:
    """Attaches a context processor to the app which injects necessary user information into each
    request/session.

    :param app: The flask app.
    :param db: The sqla object.
    """
    from supercontest.models import User
    from flask_user import UserManager

    user_manager = UserManager(app, db, User)

    @app.context_processor
    def context_processor() -> dict[str, Any]:  # pyright: ignore[reportUnusedFunction]
        """Provides the context for flask-user's management of users in the app.

        :return: The user manager.
        """
        return {"user_manager": user_manager}


def inject_jinja_globals(app: Flask) -> None:
    """Makes a few capabilities available to the templates. This isn't dynamic info (like
    a new user registering, and thus appearing in the ``id_name_map``) because that could change
    on any request. These are constants that are initialized on the start of the application
    (ie changing them would require a redeploy of the app).

    :param app: The flask app.
    """
    from supercontest.core.results import get_percentage, convert_num_to_int_if_whole

    app_jinja_env: Any = app.jinja_env
    app_jinja_env.globals["get_percentage"] = get_percentage
    app_jinja_env.globals["convert_num_to_int_if_whole"] = convert_num_to_int_if_whole


def add_preprocessor_for_g_attrs(app: Flask) -> None:
    """Creates the root preprocessor for the flask application, hydrating all requests with some
    global attributes.

    :param app: The flask app.
    """
    from flask import g
    from flask_user import current_user
    from supercontest.dbsession import queries

    @app.url_value_preprocessor
    def app_url_value_preprocessor(  # pyright: ignore[reportUnusedFunction]
        endpoint: str | None, values: dict[str, Any] | None
    ):  # pylint: disable=unused-argument
        """Current week and season are always recalculated (once) on every request to make
        sure the link wasn't stale. It's very unlikely, but if someone had kept the tab open
        in a browser for over a week, the current_week/season variables would be obsolete
        and display old information. This protects against that.

        :param endpoint: The flask endpoint.
        :param values: The flask values.
        """
        g.current_season, g.current_week = queries.get_current_season_and_week()
        g.available_seasons = queries.get_sorted_seasons()
        g.is_regseason = queries.is_regseason()
        g.is_offseason = queries.is_offseason()
        g.is_preseason = queries.is_preseason()
        g.is_postseason = queries.is_postseason()
        g.id_name_map = queries.get_id_name_map()
        g.id_email_map = queries.get_id_email_map()
        g.params = {}
        if current_user.is_authenticated:
            g.is_paid_user = queries.is_paid_user(season=g.current_season, user_id=current_user.id)
        else:
            g.is_paid_user = False


def add_requester_printer(app: Flask, logger: logging.Logger) -> None:
    """On each request, print the user's email for easy log inspection.

    :param app: The flask app.
    :param logger: The logger to print with.
    """
    from flask import request
    from flask_user import current_user

    @app.before_request
    def print_user_email():  # pyright: ignore[reportUnusedFunction]
        """Outputs the user email on every request."""
        if current_user.is_authenticated:
            user_email = current_user.email
        else:
            user_email = "unauthenticated user"
        logger.info(f"{user_email} ({request.remote_addr}) requested {request.url}")


def configure_csrf(app: Flask) -> Any:
    """Initializes CSRF protection.

    :param app: The flask app.
    :return: The CSRF object.
    """
    from supercontest.util.csrf import csrf

    csrf.init_app(app)
    return csrf


def add_graphql(app: Flask, csrf: Any) -> None:
    """Adding graphql schemas and the interactive API. Intentionally allowing users in production
    to use graphiql to explore the db because it still requires auth.

    :param app: The flask object.
    :param csrf: The csrf object.
    """
    from supercontest.graphql import schema
    from flask_graphql import GraphQLView
    from flask_user import login_required

    view = GraphQLView.as_view("graphql", schema=schema, graphiql=True)
    csrf_exemption: Any = csrf.exempt(view)
    view_func: Any = login_required(csrf_exemption)
    app.add_url_rule("/graphql", view_func=view_func)


def add_admin_panel(app: Flask, db: SQLAlchemy) -> None:  # pylint: disable=too-many-locals
    """Adds the admin interface. Custom views for repetitive actions (like committing lines),
    as well as a model browser and much more.

    :param app: The flask object.
    :param db: The sqla object.
    """
    from flask_admin import Admin
    from supercontest.views.admin import (
        AdminHomeView,
        CommitLinesView,
        EmailAllUsersView,
        LatePicksView,
        ChangeStatusView,
        AdminModelView,
        BackToSupercontestView,
    )
    from supercontest.models import (
        Season,
        Week,
        Role,
        League,
        User,
        Team,
        Prediction,
        Location,
        Coverage,
        Status,
        Game,
        Contestant,
        Pick,
    )

    app.config["FLASK_ADMIN_SWATCH"] = "yeti"
    admin = Admin(app, name="SBSC Admin", template_mode="bootstrap3", index_view=AdminHomeView)
    admin.add_view(CommitLinesView(name="Commit Lines", endpoint="commit_lines"))
    admin.add_view(EmailAllUsersView(name="Email All Users", endpoint="email_all_users"))
    admin.add_view(LatePicksView(name="Late Picks", endpoint="late_picks"))
    admin.add_view(ChangeStatusView(name="Change Status", endpoint="change_status"))
    models = [
        Season,
        Week,
        Role,
        League,
        User,
        Team,
        Prediction,
        Location,
        Coverage,
        Status,
        Game,
        Contestant,
        Pick,
    ]
    for model in models:
        admin.add_view(
            AdminModelView(
                model, db.session, category="Models"  # pyright: ignore[reportUnknownMemberType]
            ),
        )
    admin.add_view(BackToSupercontestView(name="Back To SBSC", endpoint="back_to_supercontest"))


def configure_stripe(app: Flask) -> None:
    """Adds the API key to the app configuration.

    :param app: The flask object.
    """
    import stripe

    if app.config["DEBUG"] is True:
        stripe.api_key = app.config["STRIPE_SECRET_KEY_API_TEST"]
    else:
        stripe.api_key = app.config["STRIPE_SECRET_KEY_API"]


def minify_js(app: Flask) -> None:
    """Bundles and minifies the javascript.

    :param app: The flask app.
    """
    from flask_assets import Environment, Bundle

    assets = Environment()
    assets.init_app(app)
    sc_js = Bundle(
        "js/common.js",
        "js/feedback.js",
        "js/matchups.js",
        "js/picks.js",
        "js/graph.js",
        "js/leaderboard.js",
        "js/alltimelb.js",
        "js/stats.js",
        filters="jsmin",
        output="gen/packed.js",
    )
    assets.register("sc_js", sc_js)
    app.config["ASSETS_DEBUG"] = cast(bool, app.config["DEBUG"])


def minify_html(app: Flask) -> None:
    """Minifies the templates for the app.

    :param app: The flask app.
    """
    from flask_htmlmin import HTMLMIN

    app.config["MINIFY_PAGE"] = not app.config["DEBUG"]
    htmlmin = HTMLMIN()
    htmlmin.init_app(app)


def print_loggers() -> None:
    """Simply prints all the loggers in the application's python process."""
    from supercontest.util.logging import show_loggers

    show_loggers()


def configure_smtp(app: Flask) -> None:
    """Initializes mail.

    :param app: The flask object.
    """
    from supercontest.comms.email import mail

    mail.init_app(app)


def create_app(test: bool = False) -> Flask:
    """App factory. This toplevel package provides a single function which returns the configured
    flask app. All modules are lazy loaded, and protected by logic based on env vars for the mode
    that the app is running in.

    :param test: Whether or not to run the app in test mode.
    :return: Flask app.
    """
    pkg_name, _ = get_package_info()
    app = Flask(pkg_name)
    set_env_modes(app=app, test=test)
    logger = configure_logging(app=app)
    cfgdir = get_config_dir()
    configure_app(app=app, cfgdir=cfgdir)
    register_blueprints(app=app)
    # As of right now, you can't hit any routes in test mode, because request handling requires
    # flask to be aware of the user_manager, which requires the db. I'll rip that out during the
    # flask-user ripout.
    if app.config["TESTING"]:
        return app
    db = configure_database(app=app, cfgdir=cfgdir)
    configure_db_obj_serialization(app=app)
    configure_migrations(app=app, db=db)
    configure_cache(app=app, cfgdir=cfgdir)
    if app.config["CLI"]:
        return app
    configure_user_manager(app=app, db=db)
    inject_jinja_globals(app=app)
    add_preprocessor_for_g_attrs(app=app)
    add_requester_printer(app=app, logger=logger)
    csrf = configure_csrf(app=app)
    add_graphql(app=app, csrf=csrf)
    add_admin_panel(app=app, db=db)
    configure_stripe(app=app)
    minify_js(app=app)
    minify_html(app=app)
    if app.config["DEBUG"]:
        print_loggers()
        return app
    configure_smtp(app=app)
    return app
