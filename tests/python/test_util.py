"""Tests for modules in the supercontest.util package."""
from supercontest.util.math import log_rank_distribution


def test_log_rank_distribution():
    """Simulates a standard season."""
    num_players = 30
    buyin = 50
    total = num_players * buyin
    results, remainder = log_rank_distribution(total=total, floor=buyin)
    print(results)
    num_winners = len(results)
    first_prize = results[0]
    percent_of_pot = first_prize / total
    assert num_winners > 5
    assert num_winners < 10
    assert percent_of_pot > 0.40
    assert percent_of_pot < 0.60
    assert remainder < 10
